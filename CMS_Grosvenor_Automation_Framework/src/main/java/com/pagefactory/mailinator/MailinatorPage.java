package com.pagefactory.mailinator;

import org.openqa.selenium.By;

import com.generic.Pojo;
public class MailinatorPage {

	// Local variables
		private Pojo objPojo;

		
		private By inpInboxName = By.xpath("//input[@aria-label='Enter Inbox Name']");
		private By lnkLogin = By.xpath("//a[contains(text(),'LOGIN')]");
		private By loginCodeEmailIframe = By.id("msg_body");
		private By inputEmail = By.id("many_login_email");
		private By inputPassword= By.id("many_login_password");
		private By btnLogin = By.xpath("//a[contains(text(),'Log in')]");
		private By btnGO = By.xpath("//button[@aria-label='Go to public']");
		 	
		By ClickHereLnk = By.xpath("//table[@class='intro-copy']//tbody//tr[5]//td[1]//p//font//a[contains(.,'here')]");

		private By logoMailinator = By.xpath("//div[@class='nav-title' and normalize-space()='Mailinator']");
		
		private By hdrInbox = By.xpath("//div[@class='ng-binding' and contains(.,'inbox')]");
	  	 	
		public MailinatorPage(Pojo pojo){
			this.objPojo = pojo;
		}

		public void verifyMailinatorLogoDisplayed(){
			objPojo.getLogReporter().webLog("Verify Mailinator logo displayed.", 
					objPojo.getWebActions().checkElementDisplayed(logoMailinator));
		}
		
		public void verifyLoginlnkDisplayed()
		{
			objPojo.getLogReporter().webLog("Verify Login link displayed.", 
					objPojo.getWebActions().checkElementDisplayed(lnkLogin));
		}
		
		public void clickLoginlnk()
		{
			objPojo.getLogReporter().webLog("Click 'Login' link", 
					objPojo.getWebActions().click(lnkLogin));
		}
		
		public void setLoginEmail(String email)
		{
			objPojo.getLogReporter().webLog("Enter Email ID for Mailinator Inbox ", email,
					objPojo.getWebActions().setText(inputEmail, email));
			
		}
		
		public void setPassword(String pwd)
		{
			objPojo.getLogReporter().webLog("Enter Password for Mailinator Inbox ", pwd,
					objPojo.getWebActions().setText(inputPassword, pwd));
		}
		
		public void clickLoginBtn()
		{
			objPojo.getLogReporter().webLog("Click 'Login' button", 
					objPojo.getWebActions().click(btnLogin));
		}
		
		
		public void setInboxName(String emailAddress) {
	 		objPojo.getLogReporter().webLog("Enter Public Mailinator Inbox ", emailAddress,
					objPojo.getWebActions().setText(inpInboxName, emailAddress));
		}

		public void clickGO() {
	 		objPojo.getLogReporter().webLog("Click 'GO' button", 
					objPojo.getWebActions().click(btnGO));
		}
		
		public void verifyInboxDisplayed(){
			objPojo.getLogReporter().webLog("Verify Inbox displayed.", 
					objPojo.getWebActions().checkElementDisplayed(hdrInbox));
	 	}

		public void verifyForgotUserNameMailReceived() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Did you forget your Grosvenor Casinos username?']");
			objPojo.getLogReporter().webLog("Verify forgot username mail received.", 
					objPojo.getWebActions().checkElementDisplayed(locator));
		} 
		
		public void openForgotUsernameMail() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Did you forget your Grosvenor Casinos username?']");
			objPojo.getLogReporter().webLog("Open forgot username email.", 
					objPojo.getWebActions().click(locator));
		} 
		
		public void verifyYouRequestedPINResetMailReceived() {
			//pending
		}
		
		public void openPINResetMail() {
			//pending
		}
		
		public void clickChangeYourPINLinkFromMail() {
			//pending
		}
		
		
		public void validateUsernameinEmail(String username)
		{
			objPojo.getWebActions().switchToFrameUsingIframe_Element(loginCodeEmailIframe);
			objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
			By Username_Email = By.xpath("//p[contains(text(),'Your username is:')]");
			
			String Usernameinemail = objPojo.getWebActions().getText(Username_Email);
			
			if(Usernameinemail.contains(username))
			{
				objPojo.getLogReporter().webLog("UserName displayed correctly", true);
				
			}
			else
			{
				objPojo.getLogReporter().webLog("Wrong UserName displayed", true);
				
			}
			
		}
		public void verifyForgotPasswordMailReceived() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Did you forget your Grosvenor Casinos password?']");
			objPojo.getLogReporter().webLog("Verify forgot password mail received.", 
					objPojo.getWebActions().checkElementDisplayed(locator));
		} 
		
		public void openForgotPasswordMail() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Did you forget your Grosvenor Casinos password?']");
			objPojo.getLogReporter().webLog("Open forgot password email.", 
					objPojo.getWebActions().click(locator));
		} 
		
		public void clickResetPasswordButtonFromMail() {
			By locator = By.xpath("//a[text()='Reset Password']");
			objPojo.getWebActions().javascriptScrollIntoView(locator);
		 	objPojo.getLogReporter().webLog("Click 'Reset Password' button.", 
					objPojo.getWebActions().click(locator));
		  	
		}
		
		public String getResetPasswordLink()
		{
			By locator = By.xpath("//a[text()='Reset Password']");
			return objPojo.getWebActions().getAttribute(locator, "href");
			
		}
		
		public void verifyHereAreYourMembershipCardDetailsMailReceived() {
			
		}
		
		public void verifyForgotCardNumberMailReceived() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Here are your membership card details']");
			objPojo.getLogReporter().webLog("Verify forgot password mail received.", 
					objPojo.getWebActions().checkElementDisplayed(locator));
		} 
		
		public void openForgotCardNumberMail() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Here are your membership card details']");
			objPojo.getLogReporter().webLog("Open forgot password email.", 
					objPojo.getWebActions().click(locator));
		} 
		
		public void validateCardNumberinEmail(String cardnumber)
		{
			objPojo.getWebActions().switchToFrameUsingIframe_Element(loginCodeEmailIframe);
			objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
			By CardNumber_Email = By.xpath("//p[contains(text(),'Your membership card number is:')]");
			
			String CardNoinEmail = objPojo.getWebActions().getText(CardNumber_Email);
			
			if(CardNoinEmail.contains(cardnumber))
			{
				objPojo.getLogReporter().webLog("Card Number displayed correctly", true);
				
			}
			else
			{
				objPojo.getLogReporter().webLog("Wrong Card Number displayed", true);
				
			}
		}
		
		public void verifyForgotPINMailReceived() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='You requested a PIN reset for GrosvenorCasinos.com']");
			objPojo.getLogReporter().webLog("Verify Reset PIN email received.", 
					objPojo.getWebActions().checkElementDisplayed(locator));
		} 
		
		public void openForgotPINMail() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='You requested a PIN reset for GrosvenorCasinos.com']");
			objPojo.getLogReporter().webLog("Open PIN Reset email.", 
					objPojo.getWebActions().click(locator));
		} 
		
		
		
		public String GetResetPINLinkinEmail()
		{
			
			objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
			return objPojo.getWebActions().getAttribute(ClickHereLnk,"href");
		}
		
		// Self Exclusion email
		
		public void verifySelfExclusionMailReceived() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Important information from Grosvenor Casinos']");
			objPojo.getLogReporter().webLog("Verify SelfExclusion email received.", 
					objPojo.getWebActions().checkElementDisplayed(locator));
		} 
		
		public void openSelfExclusionMail() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Important information from Grosvenor Casinos']");
			objPojo.getLogReporter().webLog("Open SelfExclusion email.", 
					objPojo.getWebActions().click(locator));
		} 
		
		public void verifyExclusionONDigitalInfoDisplayed()
		{
			By locator = By.xpath("//table[@class='intro-copy']/tbody/tr[2]/td//p[contains(text(),'We have received your request to self-exclude from GrosvenorCasinos.com and you will now no unable to play with us online. This also applies to our sister site Mecca Bingo, and can only be reversed by written request after at least 6 months.')]");
			
			objPojo.getLogReporter().webLog("Verify 'We have received your request to self-exclude from GrosvenorCasinos.com ' msg displayed in email received.", 
					objPojo.getWebActions().checkElementDisplayed(locator));
			
		}
		
		
		public void verifyRequestedLoginCodeMailReceived() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[contains(text(),'requested a Login Code to access GrosvenorCasinos.com')]");
			objPojo.getLogReporter().webLog("Verify 'You've requested a Login Code to access GrosvenorCasinos.com' mail received.", 
					objPojo.getWebActions().checkElementDisplayed(locator));
		} 
		
		public void openRequestedLoginCodeMail() {
			By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[contains(text(),'requested a Login Code to access GrosvenorCasinos.com')]");
			objPojo.getLogReporter().webLog("Open requested a Login Code mail.", 
					objPojo.getWebActions().click(locator));
		} 
		
		public String getLoginCode() {
			By locator = By.xpath("//p[contains(.,'Login') and contains(.,'code')]");
			return objPojo.getWebActions().getText(locator).split(":")[1].trim();
		}
		 
	
}