package com.pagefactory.bedePAM;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class Bede_LoginPage {

	// Local variables
	private Pojo objPojo;
	
	By inpUserName = By.id("Username");
	By inpPassword = By.id("Password");
	By btnSignIn = By.id("loginSubmit");
	
	public Bede_LoginPage(Pojo pojo){
		this.objPojo = pojo;
	}
	
	public void setUserName(String userName){
		objPojo.getLogReporter().webLog("Set user name", 
				objPojo.getWebActions().setText(inpUserName, userName));
	}
	
	public void setPassword(String password){
		objPojo.getLogReporter().webLog("Set password", 
				objPojo.getWebActions().setText(inpPassword, password));
	}
	
	public void clickSignIn(){
			
		objPojo.getLogReporter().webLog("Click Sign In button", 
				objPojo.getWebActions().click(btnSignIn));
		
	}
}
