package com.pagefactory.bedePAM;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class Bede_UpdateBalance {
	
	// Local variables
		private Pojo objPojo;
		By ManageBalanceTab = By.xpath("//span[contains(text(),'Manage Balance')]");
		By adjustPlayerBalanceHdr =  By.xpath("//input[@class='hide js-manage-balance-navigation']  ");
		By amountField = By.xpath("//input[@class='form-control js-amount required js-input' and @id='adjustmentAmount']");
		By enterNoteLabel = By.xpath("//label[@class='control-label' and contains(text(),'Leave a note to explain this change.')]");
		
		By setNoteTXT = By.xpath("//textarea[@class='form-control js-input required js-player-note-input' and @placeholder='Please enter a note...']");	
		
		By applyBTN = By.xpath("//a[@class='btn btn-primary js-button js-submit-form' and contains(text(),'Apply')]");
		By cancelBTN = By.xpath("//a[@class='btn btn-cancel js-button js-close' and contains(text(),'Cancel')]");
		By closeXBtn = By.xpath("//button[@class='close js-close-modal' and contains(text(),'×')]");
		
		
		public Bede_UpdateBalance(Pojo pojo){
			this.objPojo = pojo;
		}
		
		public void verifyManageBalanceTABdisplayed()
		{
			objPojo.getWaitMethods().sleep(6);
			objPojo.getLogReporter().webLog("Verify Manage Balance TAB is displayed",
					objPojo.getWebActions().checkElementDisplayed(ManageBalanceTab));
		}
		
		public void clickManageBalanceTAB()
		{
			objPojo.getWaitMethods().sleep(6);
			objPojo.getLogReporter().webLog(" Click on Manage Balance TAB displayed",
					objPojo.getWebActions().click(ManageBalanceTab));
		}
		
		public void verifyLabels(String labels)
		{
			if(labels.contains(","))
			{
				String[] arr1 = labels.split(",");
				
				for (String labels2 : arr1) 
				{
					By label01 = By.xpath("//label[@class='col-sm-3 control-label' and contains(text(),'"+labels2+"')]");
					
					objPojo.getLogReporter().webLog(" Verify "+labels2+" is displayed on Manage Balance screen",  
							 objPojo.getWebActions().checkElementDisplayed(label01));
				}
			}
		}
		
		public void clickWalletType()
		{
			By type = By.xpath("//span[@class='select2-chosen' and contains(text(),'Please select a wallet')]");
			objPojo.getLogReporter().webLog(" Click on Please select a wallet displayed",
					objPojo.getWebActions().click(type));
		}

		public void selectWalletType(String wallet)
		{
		 By walletType =By.xpath("//div[@class='select2-result-label']/span[contains(text(),'"+wallet+"')]");
		
		 objPojo.getLogReporter().webLog(" Select "+wallet+" wallet displayed",
					objPojo.getWebActions().click(walletType));
		}
		
		public void setAmount(String amt)
		{
			objPojo.getLogReporter().webLog(" Enter amount as :"+amt,
					objPojo.getWebActions().setText(amountField, amt));
		}
		
		public void clickReasonType()
		{
			By type = By.xpath("//span[@class='select2-chosen' and contains(text(),'Please select the reason')]");
			
			objPojo.getLogReporter().webLog(" Click on Please select the reason displayed",
					objPojo.getWebActions().click(type));
			
		}
		public void selectReasonType(String reason)
		{
		 By reasonType =By.xpath("//div[@class='select2-result-label']/span[contains(text(),'"+reason+"')]");
			
		 objPojo.getLogReporter().webLog(" Select reason as : "+reason+" displayed",
					objPojo.getWebActions().click(reasonType));
		}
		
		public void verifyNoteLabelDisplayed()
		{
			objPojo.getLogReporter().webLog("verify 'Leave a note to explain this change.' displayed",
					objPojo.getWebActions().checkElementDisplayed(enterNoteLabel));
		}
		
		public void setNote(String note)
		{
			objPojo.getLogReporter().webLog(" Set Note as :"+note,
					objPojo.getWebActions().setText(setNoteTXT, note));
		}
		
		public void clickCancelBTN()
		{
			objPojo.getLogReporter().webLog(" Click on Cancel BTN displayed",
					objPojo.getWebActions().click(cancelBTN));
		}
		public void clickApplyBTN()
		{
			objPojo.getLogReporter().webLog(" Click on Apply BTN displayed",
					objPojo.getWebActions().click(applyBTN));
		}
		
		public void clickCloseXBTN()
		{
			objPojo.getLogReporter().webLog(" Click on close 'X' button displayed",
					objPojo.getWebActions().click(closeXBtn));
		}
		public void verifymanagePlayerBalanceHDR()
		{
			By managePlayerBalanceHDR = By.xpath("//h4[contains(text(),'Manage player balance')]");
			objPojo.getLogReporter().webLog(" Verify manage Player Balance HDR displayed on popup ",
					objPojo.getWebActions().checkElementDisplayed(managePlayerBalanceHDR));
		}
		
		public String getLabelValueOnPopup(String label) {
			By label01 = By.xpath("//table[@class='table wallet-update-details']/tbody/tr/th[contains(text(),'"+label+"')]/following-sibling::td");
			return objPojo.getWebActions().getText(label01);
		}
		
		public String validateBalanceDisplayed()
		{		
			String currBalance = (this.getLabelValueOnPopup("Current Wallet Balance")).substring(1);
			String adjAmt =(this.getLabelValueOnPopup("Adjustment Amount")).substring(1);
			String newBal = (this.getLabelValueOnPopup("New Wallet Balance")).substring(1);
			float totalBalance = Float.parseFloat(currBalance) +Float.parseFloat(adjAmt);
			float newBalance = Float.parseFloat(newBal);
			
			objPojo.getLogReporter().webLog("Validated New Wallet balance : "+newBalance ,
					String.valueOf(totalBalance).equals(String.valueOf(newBalance)) );
			return String.valueOf(newBalance);
				}
		public void verifyconfirmBTN()
		{
			By confirmBTN = By.xpath("//a[@class='btn btn-primary js-button js-confirm' and contains(text(),'Confirm')]");
			objPojo.getLogReporter().webLog(" Verify confirm BTN displayed on popup ",
					objPojo.getWebActions().checkElementDisplayed(confirmBTN));
		}
		public void backBTN()
		{
			By backBTN = By.xpath("//a[@class='btn btn-cancel js-button js-previous' and contains(text(),'Back')]");
			objPojo.getLogReporter().webLog(" Verify back BTN displayed on popup ",
					objPojo.getWebActions().checkElementDisplayed(backBTN));
		}
		public void clickconfirmBTN()
		{
			By confirmBTN = By.xpath("//a[@class='btn btn-primary js-button js-confirm' and contains(text(),'Confirm')]");
			objPojo.getLogReporter().webLog(" Click on confirm BTN displayed on popup ",
					objPojo.getWebActions().click(confirmBTN));
		}
				
}
