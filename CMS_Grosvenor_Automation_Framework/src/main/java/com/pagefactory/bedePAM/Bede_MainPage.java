package com.pagefactory.bedePAM;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class Bede_MainPage {

	// Local variables
	private Pojo objPojo;
 
	By lnkLogOut = By.xpath("//a[contains(text(),'Logout')]");
	By lnkPlayerManagement = By.xpath("//a[@class='menu-link' and @href='https://np03-playeraccountmanagement-bos.rank.bedegaming.net/PlayerManagement']"); 
 	
	public Bede_MainPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifyLogoutLinkDisplayed(){
		
		objPojo.getLogReporter().webLog("Verify 'logout' link displayed", 
				objPojo.getWebActions().checkElementDisplayed(lnkLogOut));
		
	}
	
	public void clickLogout(){
		objPojo.getLogReporter().webLog("Click 'logout' link.", 
				objPojo.getWebActions().click(lnkLogOut));
	}

	public void clickPlayerManagementLink(){
		objPojo.getLogReporter().webLog("Click 'Player Management' link", 
				objPojo.getWebActions().click(lnkPlayerManagement));
	}
}
