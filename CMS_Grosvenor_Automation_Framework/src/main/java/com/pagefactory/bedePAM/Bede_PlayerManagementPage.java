package com.pagefactory.bedePAM;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class Bede_PlayerManagementPage 
{
	// Local variables
	private Pojo objPojo;

	By inpPlayerSearch = By.xpath("//input[@id='unifiedSearch' and @placeholder='Player Search']");
	By inpMinDeposit = By.name("change-limit-input-minimumdeposit");
	By inpMaxDeposit = By.name("change-limit-input-maximumdeposit");
	By inpMinWithdrawal = By.name("change-limit-input-minimumwithdrawal");
	By inpMaxWithdrawal = By.name("change-limit-input-maximumwithdrawal");

	By btnSerach = By.id("unifiedSubmit");
	By btnApplyTransactionLimit = By.xpath("//a[contains(text(),'Apply')]");
	By btnConfirmTransactionLimit = By.xpath("//a[contains(text(),'Confirm')]");
	By btnNextIDStatusChange = By.xpath("//a[contains(text(),'Next')]");
	By btnConfirmIDStatusChange = By.xpath("//a[contains(text(),'Confirm')]");

	By hrdWelcomeToPlayerManagement = By.xpath("//h1[contains(text(),'Welcome to Player Management')]");
	By hdrYouAreChangingLimits = By.xpath("//p[contains(text(),'You are changing limits')]");
	By hdrYouAreUpdatingIDStatus = By.xpath("//p[contains(text(),'You are updating ID Status')]");
	By hdrMarketingStatusSubscribed = By.xpath("//dt[@class='title marketing']/following-sibling::dd/a[contains(text(),'Email, SMS, Post, Telephone, Push')]");
	By hdrMarketingStatusUnSubscribed = By.xpath("//dt[@class='title marketing']/following-sibling::dd/a[contains(text(),'Unsubscribed')]");

	By txtAreaPlayerNoteIDStatusChange = By.id("playerNote");

	public Bede_PlayerManagementPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifyPlayerManagementHeaderDisplayed(){
		objPojo.getLogReporter().webLog("Verify 'Welcome to Player Management' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hrdWelcomeToPlayerManagement));
	}

	public void setPlayerSearch(String player){
		objPojo.getLogReporter().webLog("Set player search", player,
				objPojo.getWebActions().setText(inpPlayerSearch, player));
	}

	public void clickSearch(){
		objPojo.getLogReporter().webLog("Click 'Search'", 
				objPojo.getWebActions().click(btnSerach));
	}

	public void verifySummaryTabDisplayedFromPalyerManagement(){
		By locator = By.xpath("//a[contains(text(),'Summary')]/parent::li[contains(@class,'selected')]");
		objPojo.getLogReporter().webLog("Verify summary tab displayed from palyer management", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	}

	public void verifyPlayerHeaderOnSummaryTabOfPlayerManagement(String playerName){
		By locator = By.xpath("//div[@class='account-header']/h2[@title='Username: " + playerName + "']");
		objPojo.getLogReporter().webLog("Verify player header displayed from player management search", playerName, 
				objPojo.getWebActions().checkElementDisplayed(locator));
	}

	public void verifyeEmailOnSummaryTabOfPlayerManagement(String email){
		By locator = By.xpath("//a[contains(text(),'" + email +"')]/parent::dd/parent::div/dt[contains(text(),'Email')]");
		objPojo.getLogReporter().webLog("Verify email displayed from player management search", email, 
				objPojo.getWebActions().checkElementDisplayed(locator));
	}

	public void verifyeDateOfBirthOnSummaryTabOfPlayerManagement(String dateOfBirth){
		By locator = By.xpath("//a[contains(text(),'" + dateOfBirth + "')]/parent::dd/parent::dl/dt[contains(text(),'DOB')]");
		objPojo.getLogReporter().webLog("Verify date of birth displayed from player management search", dateOfBirth, 
				objPojo.getWebActions().checkElementDisplayed(locator));
	}

	public void selectPayerManagementTab(String tab){
		By locator = By.xpath("//a[@class='local-menu-link' and contains(text(),'" + tab +"')]");
		objPojo.getLogReporter().webLog("Select tab" , tab, 
				objPojo.getWebActions().click(locator));
	}

	public void selectChangeLimitType(String limitType){
		By locator = By.xpath("//input[@name='limitsChange']/parent::label[contains(.,'" + limitType +"')]");
		objPojo.getLogReporter().webLog("Select change limit type" , limitType, 
				objPojo.getWebActions().click(locator));
	}

	public void setTransactionMiniumDeposit(String minDeposit){
		objPojo.getLogReporter().webLog("Set minimun deposite for transaction", minDeposit,
				objPojo.getWebActions().setTextWithClear(inpMinDeposit, minDeposit));
	}

	public void setTransactionMaximumDeposit(String maxDeposit){
		objPojo.getLogReporter().webLog("Set maximum deposite for transaction", maxDeposit,
				objPojo.getWebActions().setTextWithClear(inpMaxDeposit, maxDeposit));
	}

	public void setTransactionMiniumWithdrawal(String minWithdrawal){
		objPojo.getLogReporter().webLog("Set minimun Withdrawal for transaction", minWithdrawal,
				objPojo.getWebActions().setTextWithClear(inpMinWithdrawal, minWithdrawal));
	}

	public void setTransactionMaximumWithdrawal(String maxWithdrawal){
		objPojo.getLogReporter().webLog("Set maximum Withdrawal for transaction", maxWithdrawal,
				objPojo.getWebActions().setTextWithClear(inpMaxWithdrawal, maxWithdrawal));
	}

	public void clickApplyForTransactionLimit(){
		objPojo.getLogReporter().webLog("Click 'Apply' for Transaction limit", 
				objPojo.getWebActions().click(btnApplyTransactionLimit));
	}

	public void verifyYouAreChangingLimitsHeaderDisplayed(){
		objPojo.getLogReporter().webLog("Verify 'You are changing limits' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrYouAreChangingLimits));
	}

	public void clickConfirmForTransactionLimit(){
		objPojo.getLogReporter().webLog("Click 'Confirm' for Transaction limit", 
				objPojo.getWebActions().click(btnConfirmTransactionLimit));
	}

	public void verifyYouAreUpdatingIDStatusHeaderDisplayed(){
		objPojo.getLogReporter().webLog("Verify 'You are updating ID Status' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrYouAreUpdatingIDStatus));
	}

	public void verifyYouAreUpdatingIDStatusHeaderNotDisplayed(){
		objPojo.getLogReporter().webLog("Verify 'You are updating ID Status' header not displayed", 
				objPojo.getWebActions().checkElementNotDisplayed(hdrYouAreUpdatingIDStatus));
	}

	public void changeIDStatusOfPlayer(String status, String playerNote){
		By spnCurrentStatus = By.xpath("//dt[@class='title status-id']/following-sibling::dd[@data-name='idStatus']/span");
		String currentIDStatus = objPojo.getWebActions().getText(spnCurrentStatus);
		if(currentIDStatus.equalsIgnoreCase(status))
			objPojo.getLogReporter().webLog("Current ID status is - " + status , true);
		else{
			objPojo.getLogReporter().webLog("Click on current ID status ", 
					objPojo.getWebActions().click(spnCurrentStatus));
			this.verifyYouAreUpdatingIDStatusHeaderDisplayed();
			By drpIDStatus = By.xpath("//label[contains(text(),'ID Status')]//parent::div/div/div");
			objPojo.getLogReporter().webLog("Click on ID status dropdown", 
					objPojo.getWebActions().click(drpIDStatus));
			By ulLocator = By.xpath("//div[contains(@class,'confirm-idstatus') and contains(@style,'display: block')]/ul");
			objPojo.getLogReporter().webLog("Select ID status ", 
					objPojo.getWebActions().selectFromCustomDropDown(ulLocator, status));
			objPojo.getLogReporter().webLog("Set player note for id status change", playerNote,
					objPojo.getWebActions().setText(txtAreaPlayerNoteIDStatusChange, playerNote));
			objPojo.getLogReporter().webLog("Click 'Next'", 
					objPojo.getWebActions().click(btnNextIDStatusChange));
			objPojo.getLogReporter().webLog("Click 'Confirm'", 
					objPojo.getWebActions().click(btnConfirmIDStatusChange));
			this.verifyYouAreUpdatingIDStatusHeaderNotDisplayed();
			objPojo.getLogReporter().webLog("Verify ID status changed", 
					objPojo.getWebActions().getText(spnCurrentStatus).equalsIgnoreCase(status));	
		}
	}

	public void verifyLatestTransactionHistory(String transactionDate, String transactionType, 
			String transactionAmount, String transactionMethod)
	{
		System.out.println("------------" + transactionDate);
		System.out.println("------------" + transactionType);
		System.out.println("------------" + transactionAmount);
		System.out.println("------------" + transactionMethod);
		if(!transactionDate.equals("")){
			By tdLatestTransactionDate = By.xpath("//table[contains(@class,'transaction-history')]/tbody/tr[1]/td[1]");
			objPojo.getLogReporter().webLog("Verify latest transaction date", 
					objPojo.getWebActions().getText(tdLatestTransactionDate).contains(transactionDate));
		}

		if(!transactionType.equals("")){
			By tdLatestTransactionType = By.xpath("//table[contains(@class,'transaction-history')]/tbody/tr[1]/td[3]");
			objPojo.getLogReporter().webLog("Verify latest transaction type", 
					objPojo.getWebActions().getText(tdLatestTransactionType).contains(transactionType));
		}

		if(!transactionAmount.equals("")){
			By tdLatestTransactionAmount = By.xpath("//table[contains(@class,'transaction-history')]/tbody/tr[1]/td[4]");
			objPojo.getLogReporter().webLog("Verify latest transaction amount", 
					objPojo.getWebActions().getText(tdLatestTransactionAmount).contains(transactionAmount));
		}

		if(!transactionMethod.equals("")){
			By tdLatestTransactionMethod = By.xpath("//table[contains(@class,'transaction-history')]/tbody/tr[1]/td[5]");
			objPojo.getLogReporter().webLog("Verify latest transaction method", 
					objPojo.getWebActions().getText(tdLatestTransactionMethod).contains(transactionMethod));
		}
	}

	public void verifyMarketingStatusIsSubscribed(){
		objPojo.getLogReporter().webLog("Verify marketing status is subscribed", 
				objPojo.getWebActions().checkElementDisplayed(hdrMarketingStatusSubscribed));
	}

	public void verifyMarketingStatusIsUnSubscribed(){
		objPojo.getLogReporter().webLog("Verify marketing status is unsubscribed", 
				objPojo.getWebActions().checkElementDisplayed(hdrMarketingStatusUnSubscribed));
	}
	
	
	public void clickOnEndBreak()
	{
		By btnEndBreak = By.xpath("//label[contains(text(),'End Break')]");
		objPojo.getWebActions().checkElementDisplayed(btnEndBreak);
		objPojo.getLogReporter().webLog("click on EndBreak ", objPojo.getWebActions().click(btnEndBreak));
	}
	
	public void SetReason(String noteDetails)
	{
		By inputReason = By.xpath("//label[text()='Leave a note to explain this change.']//following-sibling::input");
		objPojo.getLogReporter().webLog("Enter notes ",  objPojo.getWebActions().setText(inputReason,noteDetails));
	}
	
	
	public void clickOnButton(String btn)
	{
		By button = By.xpath("//button[contains(.,'"+btn+"')]");
		objPojo.getLogReporter().webLog("Click on "+btn,objPojo.getWebActions().click(button));
		objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
	}
	
	
	
	public void ChangeAccountStatus(String accStatus)
	{
System.out.println("********** acc "+accStatus);

		switch(accStatus)
		{
		case "Account On A Break":
			clickOnAccountStatus(accStatus);
			clickOnEndBreak();
			SetReason("Test purpose only");
			clickOnButton("Apply");
			clickOnButton("Confirm");
			verifyAccountStatus("Account OK ");
			break;
		case "Account Operator Excluded":
			clickOnAccountStatus(accStatus);
			SetReason("Test purpose only");
			clickOnButton("Apply");
			clickOnButton("Confirm");
			verifyAccountStatus("Account OK ");
			break;
		}

	}

	public void verifyAccountStatus(String accStatus)
	{
		By accountStatus = By.xpath("//span[@class=\"input-text\"][contains(.,\""+accStatus+"\")]");
		
		objPojo.getLogReporter().webLog("Verify Account status displayed is "+accStatus+" .", 
				objPojo.getWebActions().checkElementDisplayed(accountStatus));
		
	}
	
	
	public void clickOnAccountStatus(String accStatus)
	{
		By accountStatus = By.xpath("//span[@class=\"input-text\"][contains(.,\""+accStatus+"\")]");
		objPojo.getLogReporter().webLog("Verify Account status displayed is "+accStatus+" .", 
				objPojo.getWebActions().click(accountStatus));
		objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
	}
	
}
