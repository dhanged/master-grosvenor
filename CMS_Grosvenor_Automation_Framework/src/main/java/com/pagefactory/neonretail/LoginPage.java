package com.pagefactory.neonretail;

import org.openqa.selenium.By;

import com.generic.Pojo;
public class LoginPage {

	// Local variables
	private Pojo objPojo;
 
	public LoginPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifyNeonLogoDisplayed(){
		By logoNeon = By.xpath("//img[contains(@src,'Neon-logo-lg')]");
		objPojo.getLogReporter().webLog("Verify Noen logo displayed.", 
				objPojo.getWebActions().checkElementDisplayed(logoNeon));
 	}
 
	public void setUserName(String userName) {
		By inpUserName = By.xpath("");
		objPojo.getLogReporter().webLog("Set username ", userName,
				objPojo.getWebActions().setText(inpUserName, userName));
	}
	
	public void setPassword(String password) {
		By inpPassword = By.xpath("//input[@name='Password']");
		objPojo.getLogReporter().webLog("Set password ", password,
				objPojo.getWebActions().setText(inpPassword, password));
	}
	
	public void clickLogin() {
		By inpLogin = By.xpath("//input[@id='buttonSubmit' and text()='Login']");
		objPojo.getLogReporter().webLog("Click 'Login' button", 
				objPojo.getWebActions().click(inpLogin));
	}
}