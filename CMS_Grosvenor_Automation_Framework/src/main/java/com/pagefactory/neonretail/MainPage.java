package com.pagefactory.neonretail;

import org.openqa.selenium.By;

import com.generic.Pojo;
public class MainPage {

	// Local variables
	private Pojo objPojo;
 
	public MainPage(Pojo pojo){
		this.objPojo = pojo;
	}
	
	public void clickOKButton() {
		By btnOk = By.xpath("//button[@data-ig-type='OK' and text()='OK']");
		do {
			objPojo.getWebActions().click(btnOk);	
		}while(objPojo.getWebActions().checkElementDisplayed(btnOk));
 	}
	
	public void verifyUSerLoggedInSuccessfully(String userName){
		By userLogin = By.xpath("//span[@class='readonlytext ig-header-text' and contains(text(),'" + userName + "')]");
		objPojo.getLogReporter().webLog("Verify user logged in successfully.", userName,
				objPojo.getWebActions().checkElementDisplayed(userLogin));
 	} 
	
	public void navigateThroughMenu(String mainMenu, String... subMenu) {
		By lnkMainMenu =By.xpath("//div[@class='ig-navbar-menu']/ul[@class='ig-navbar-menu-levelone nav']/li[@class='parent dropdown']/a[text()='" + mainMenu + "']");
		objPojo.getLogReporter().webLog("Select main menu ", mainMenu,
				objPojo.getWebActions().click(lnkMainMenu));
		
		By lnkMainMenuOpen =By.xpath("//div[@class='ig-navbar-menu']/ul[@class='ig-navbar-menu-levelone nav']/li[@class='parent dropdown open']/a[text()='" + mainMenu + "']");
		objPojo.getLogReporter().webLog("Verify main menu get opened.", 
				objPojo.getWebActions().checkElementDisplayed(lnkMainMenuOpen));
		
		if(subMenu != null && subMenu.length > 0 ) {
			By lnkSubMenu =By.xpath("//li[@class='parent dropdown open']/a[text()='" + mainMenu + "']/div[@class='container-accordion dropdown-menu']/div[@class='container-accordion-inner']/ul/li/a[text()='" + subMenu[0] + "']");
			objPojo.getLogReporter().webLog("Select sub menu ", subMenu[0],
					objPojo.getWebActions().click(lnkSubMenu));
		}
	}
}