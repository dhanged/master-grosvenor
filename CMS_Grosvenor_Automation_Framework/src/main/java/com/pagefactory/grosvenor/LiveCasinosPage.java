package com.pagefactory.grosvenor;
import org.openqa.selenium.By;

import com.generic.Pojo;

public class LiveCasinosPage {
	
	private Pojo objPojo;

	public LiveCasinosPage(Pojo pojo){
		this.objPojo = pojo;
	}


	public void navigateToCasinosTAB()
	{
		By LiveCasinosPrimaryTAB = By.xpath("//nav[@class='component top-navigation']/ul/li/a[@class='tux']");
		
		objPojo.getLogReporter().webLog(" Verify Live Casinos tab from Primary Navigation displayed",
				objPojo.getWebActions().checkElementDisplayed(LiveCasinosPrimaryTAB));
		
		objPojo.getLogReporter().webLog("Click on the Live Casinos tab from Primary Navigation",
				objPojo.getWebActions().click(LiveCasinosPrimaryTAB));
	}
	
	
	public void verifySecondaryTABdisplayed(String links)
	{
		
		if(links.contains(","))
		{
			String[] arr1 = links.split(",");
			
			for (String links2 : arr1  ) 
			{
				By locator = By.xpath("//a[contains(text(),'"+links2+"')]");
				
				objPojo.getLogReporter().webLog(" verify "+ links2+" link displayed from secondary navigation",
						objPojo.getWebActions().checkElementDisplayed(locator));
			}
		}
	}
	public void clickSecondaryTABdisplayed(String links)
	{
		if(links.contains(","))
		{
			String[] arr1 = links.split(",");
			
			for (String links2 : arr1  ) 
			{
				By locator = By.xpath("//a[contains(text(),'"+links2+"')]");
				
				objPojo.getLogReporter().webLog(" Click on "+ links2+" link displayed from secondary navigation",
						objPojo.getWebActions().click(locator));
			}
		}
		else
		{
			By locator2= By.xpath("//a[contains(text(),'"+links+"')]");
			objPojo.getLogReporter().webLog(" Click on "+ links+" link displayed from secondary navigation",
					objPojo.getWebActions().click(locator2));
		}
	}
	
	public void clickInfoIcon()
	{
		By gameImg = By.xpath("//a[@class='right-link']/img[@src='/assets/grosvenor/img/info.jpg']");
		
		objPojo.getLogReporter().webLog(" Click on info icon displayed on first game ",
				objPojo.getWebActions().click(gameImg));
	}
	
	
	
	
	public void verifyJoinBTNOnInfoPage()
	{
		By joinBTN = By.xpath("//button[@class='load-table column-12-exsmall column-12-small column-4-middle column-4-large']");
		
		objPojo.getLogReporter().webLog(" Verify on Join Button displayed on info page ",
				objPojo.getWebActions().checkElementDisplayed(joinBTN));
	}
	
	public void clickJoinBTNOnInfoPage()
	{
		By joinBTN = By.xpath("//button[@class='load-table column-12-exsmall column-12-small column-4-middle column-4-large']");
		
		objPojo.getLogReporter().webLog(" Click on Join Button displayed on info page ",
				objPojo.getWebActions().click(joinBTN));
	}
	
	public void verifyDescriptionBTNOnInfoPage()
	{
		By joinBTN = By.xpath("//a[contains(text(),'Description')]");
		
		objPojo.getLogReporter().webLog(" Verify on Join Button displayed on info page ",
				objPojo.getWebActions().checkElementDisplayed(joinBTN));
	}
	
	
	public void verifyNameOnInfoPage(String name)
	{
		By locator = By.xpath("//h1[contains(text(),'"+name+"')]");
		
		objPojo.getLogReporter().webLog(" Click on Join Button displayed on info page ",
				objPojo.getWebActions().checkElementDisplayed(locator));
	}

	public void verifyHomepageSectionHeadersdisplayed(String classHeader)
	{
		
		String[] arrSingleEntry = classHeader.split("~"); //
		for(String singleEntry : arrSingleEntry) 
		{
			String[] entry = singleEntry.split(":");
			By locator = By.xpath("//h2[@class='"+entry[0]+"'and contains(text(),'"+entry[1]+"')]");
							
			objPojo.getLogReporter().webLog(" Verify "+entry[1]+" section is displayed with "+entry[0]+" icon",
					objPojo.getWebActions().checkElementDisplayed(locator));
			
		}
		
			
	}
	
	public void verifySectionHeadersLiveCasinoPage(String hdr)
	{
		By locator = By.xpath("//h1[contains(text(),'EXPERIENCE LIVE CASINO GAMES')]");
		
		objPojo.getLogReporter().webLog(" Verify EXPERIENCE LIVE CASINO GAMES is displayed on landing page",
				objPojo.getWebActions().checkElementDisplayed(locator));
	
		if(hdr.contains("~"))
		{
			String[] arr1 = hdr.split("~");
			
			for (String hdr2 : arr1  ) 
			{
				By locator2 = By.xpath("//h2[contains(text(),'"+hdr2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify "+ hdr2+" displayed on landing page",
						objPojo.getWebActions().checkElementDisplayed(locator2));
			}
		}
	
	}
	
	public void verifyHeadersOnLiveRoulette()
	{
		By locator = By.xpath("//h1[contains(text(),'Play Live Roulette with Grosvenor Casinos')]");
		
			objPojo.getLogReporter().webLog(" Verify 'Play Live Roulette with Grosvenor Casinos' displayed Live Roulette page",
				objPojo.getWebActions().checkElementDisplayed(locator));
	}
	
	public void verifyHeadersOnLiveBlackJack(String hdrs)
	{
		By locator1 = By.xpath("//h1[contains(text(),'Play Live Blackjack with Grosvenor Casinos')]");
		
		objPojo.getLogReporter().webLog(" Verify 'Play Live Blackjack with Grosvenor Casinos' displayed Live Blackjack page",
				objPojo.getWebActions().checkElementDisplayed(locator1));
		
		if(hdrs.contains("~"))
		{
			String[] arr1 = hdrs.split("~");
			
			for (String hdrs2 : arr1  ) 
			{
				By locator = By.xpath("//h2[contains(text(),'"+hdrs2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify "+ hdrs2+"  header displayed on Live Blackjack page",
						objPojo.getWebActions().checkElementDisplayed(locator));
			}
		}
	}
	
	public void verifyHeadersOnLiveBaccarat(String hdrs)
	{
		By locator = By.xpath("//h1[contains(text(),'PLAY LIVE BACCARAT WITH GROSVENOR CASINOS')]");
		
			objPojo.getLogReporter().webLog(" Verify 'Play LIVE BACCARAT WITH GROSVENOR CASINOS' displayed Live BACCARAT page",
				objPojo.getWebActions().checkElementDisplayed(locator));
			
			if(hdrs.contains("~"))
			{
				String[] arr1 = hdrs.split("~");
				
				for (String hdrs2 : arr1  ) 
				{
					By locator2 = By.xpath("//h2[contains(text(),'"+hdrs2+"')]");
					
					objPojo.getLogReporter().webLog(" Verify "+ hdrs2+"  header displayed on Live BACCARAT page",
							objPojo.getWebActions().checkElementDisplayed(locator2));
				}
			}	
		}
	
	
	public void verifyHeadersOnHostOffers(String hdrs)
	{
		By locator = By.xpath("//p[contains(text(),'In-game dealers awards')]");
		
			objPojo.getLogReporter().webLog(" Verify 'In-game dealers awards' displayed Host Offers page",
				objPojo.getWebActions().checkElementDisplayed(locator));
			
			if(hdrs.contains("~"))
			{
				String[] arr1 = hdrs.split("~");
				
				for (String hdrs2 : arr1  ) 
				{
					By locator2 = By.xpath("//h3[contains(text(),'"+hdrs2+"')]");
					
					objPojo.getLogReporter().webLog(" Verify "+ hdrs2+" header displayed on Host Offers page",
							objPojo.getWebActions().checkElementDisplayed(locator2));
				}
			}	
		}
	
	
	
	
	
	
	public void verifyFirstTableDisplayed()
	{
		objPojo.getWebActions().switchToFrameUsingNameOrId("game-overlay-iframe");
		objPojo.getWebActions().switchToFrameUsingNameOrId("playerFrame"); 
		
		objPojo.getWebActions().switchToFrameUsingNameOrId("iframe"); 
		objPojo.getWaitMethods().sleep(10);
		By locator = By.xpath("//div[@class='lobby-table-game-specific-wrapper lobbyTableGameSpecificWrapper--1QUQK phonePortrait--7ijXQ']");
		
		objPojo.getLogReporter().webLog(" Verify First Game/Table displayed  ",
				objPojo.getWebActions().checkElementDisplayed(locator));
		
	}

	public void verifyFirstTableonliveBlackjackDisplayed()
	{
		objPojo.getWebActions().switchToFrameUsingNameOrId("game-overlay-iframe");
		objPojo.getWebActions().switchToFrameUsingNameOrId("playerFrame"); 
		
		objPojo.getWebActions().switchToFrameUsingNameOrId("iframe"); 
		objPojo.getWaitMethods().sleep(10);
		By locator = By.xpath("//div[@class='Wrapper--2s7uU lobby3 lobby3-standalone']");
		objPojo.getLogReporter().webLog(" Verify First Game/Table displayed  ",
				objPojo.getWebActions().checkElementDisplayed(locator));
		
	}

	

}

