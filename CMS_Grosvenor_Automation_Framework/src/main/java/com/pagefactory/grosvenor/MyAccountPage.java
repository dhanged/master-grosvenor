package com.pagefactory.grosvenor;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class MyAccountPage {

	// Local variables
	private Pojo objPojo;


	public MyAccountPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void selectMyAccountMenu(String menu, String... subMenu) {
		By locator = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'" + menu + "')]");
		objPojo.getLogReporter().webLog("Select main menu from My Account - ", menu,
				objPojo.getWebActions().click(locator));

		locator = By.xpath("//h2//span[contains(text(),'" + menu + "')]");
		objPojo.getLogReporter().webLog("Verify main menu get displayed", menu,
				objPojo.getWebActions().checkElementDisplayed(locator));

		if(subMenu != null && subMenu.length > 0) {
			locator = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'" + subMenu[0] + "')]");
			objPojo.getLogReporter().webLog("Verify sub menu get displayed", subMenu[0],
					objPojo.getWebActions().click(locator));

			locator = By.xpath("//h2//span[contains(text(),'" + subMenu[0] + "')]");
			objPojo.getLogReporter().webLog("Verify sub menu get displayed", subMenu[0],
					objPojo.getWebActions().checkElementDisplayed(locator));
		}
	}

	public int getAvailableAmountToWithdraw() {
		By locator = By.xpath("//div[@class='amount-text']/span[@class='preview-amount']/strong");
		String availableAmount = objPojo.getWebActions().getText(locator);
		availableAmount = availableAmount.substring(1);
		availableAmount = availableAmount.substring(0, availableAmount.indexOf("."));
		int amountToWithdraw = Integer.parseInt(availableAmount);
		return amountToWithdraw;
	}

	public void firstDepositForUserSuccessAgree() {
		By chkDepositAgree = By.xpath("//input[@id='deposit-agree']");
		if(objPojo.getWebActions().checkElementElementDisplayedWithMinWait(chkDepositAgree)) {
			objPojo.getLogReporter().webLog("Select Deposit Agree checkbox",  
					objPojo.getWebActions().click(chkDepositAgree));

			By btnNext = By.xpath("//button[contains(text(),'Next')]");
			objPojo.getLogReporter().webLog("Click 'Next' button ",  
					objPojo.getWebActions().click(btnNext));
		}
	}

	public void deposit_UsingCard(String cardNumber, String expDate, String securityCodeCVV, String amount) {
		this.firstDepositForUserSuccessAgree();
		objPojo.getWebActions().switchToFrameUsingNameOrId("payment-process");
		this.clickShowMore();
		// card deposit
		By inpCardNumber = By.id("cc_card_number");
		objPojo.getLogReporter().webLog("Set Card number ", cardNumber, 
				objPojo.getWebActions().setText(inpCardNumber, cardNumber));

		By inpExpDate = By.id("cc-exp-date");
		objPojo.getLogReporter().webLog("Set expiry date ", expDate, 
				objPojo.getWebActions().setText(inpExpDate, expDate));


		By inpSecurityCodeCVV = By.id("cc_cvv2");
		objPojo.getLogReporter().webLog("Set security date (CVV) ", securityCodeCVV, 
				objPojo.getWebActions().setText(inpSecurityCodeCVV, securityCodeCVV));

		this.setDepositAmount(amount);
		this.clickDeposit();

	}
	
	public void verifysetDepLimitsLnkDisplayed() {
		//pending
	}
	
	public void verifyliveHelpLnkDisplayed() {
		//pending
	}
	
	public void verifywelcomeBonusHDRDisplayed() {
		//pending
	}

	public void setDepositAmount(String amount) {
		By inpAmount = By.id("item_amount_1");
		objPojo.getLogReporter().webLog("Set amount to deposit ", amount, 
				objPojo.getWebActions().setText(inpAmount, amount));
	}
	
	public void verifyDepositNowHDRDisplayed() {
		//pending
	}
	
	public void verifyminMaxDepAmtTXT() {
		//pending
	}
	
	

	public void clickDeposit() {
		By btnDeposit = By.xpath("//input[@id='continueButton' and @value='Deposit']");
		objPojo.getLogReporter().webLog("Select 'Deposit' button ",  
				objPojo.getWebActions().click(btnDeposit));
	}


	public void selectDepositMethod() {

	}

	public void clickShowMore() {
		By locator = By.xpath("//span[contains(text(),'Show More')]");
		objPojo.getLogReporter().webLog("Click 'Show More' button ",  
				objPojo.getWebActions().clickByJavaScript(locator));
	}

	public void verifyDepositSuccessPopUpDisplayed() {
		By locator = By.xpath("//h2[contains(text(),'Deposit successful!')]");
		objPojo.getLogReporter().webLog("Verify 'Deposit' successful message displayed",  
				objPojo.getWebActions().clickByJavaScript(locator));
	}
	
	public void firstDepositPoPFAgree() {
		//pending
	}

	//////////////////////////////// Withdraw //////////////////////////////////
	public void setWithdrawAmount(String amount) {
		By inpWithdrawAmount = By.id("input-number");
		objPojo.getLogReporter().webLog("Set amount to Withdraw ", amount, 
				objPojo.getWebActions().setText(inpWithdrawAmount, amount));
	}

	public void setPasswordToWithdraw(String password) {
		By inpWithdrawPassword = By.xpath("//input[@placeholder='Enter password']");
		objPojo.getLogReporter().webLog("Set password to withdraw ", password, 
				objPojo.getWebActions().setText(inpWithdrawPassword, password));
	}

	public void clickNext() {
		By btnNext = By.xpath("//button[@type='submit' and contains(text(),'Next')]");
		objPojo.getLogReporter().webLog("Click 'Next' button ",  
				objPojo.getWebActions().click(btnNext));
	}

	public void clickContinue() {
		By btnContinue = By.xpath("//button[@type='submit' and contains(text(),'Continue')]");
		objPojo.getLogReporter().webLog("Click 'Continue' button ",  
				objPojo.getWebActions().click(btnContinue));
	}

	public void verifyAreYouSureToWithdrawHeadeDisplayed() {
		By locator = By.xpath("//div[contains(text(),'Are you sure')]");
		objPojo.getLogReporter().webLog("Verify 'Are you sure' to withdraw header displayed ",  
				objPojo.getWebActions().click(locator));
	}

	public void selectWithdrawMethod(String withdrawMethod) {
		By locator = null;
		switch(withdrawMethod.toLowerCase()) {
		case "card":
			locator = By.xpath("//table[@class='table_pms pm-list']//div[@class='upm-logo visa']");
			objPojo.getLogReporter().webLog("Select withdraw method as using card",  
					objPojo.getWebActions().click(locator));
			break;
	 	}
	}

	public void clickWithdraw() {
		By btnWithdraw = By.xpath("//button[@type='button' and contains(text(),'Withdraw')]");
		objPojo.getLogReporter().webLog("Click 'Withdraw' button ",  
				objPojo.getWebActions().click(btnWithdraw));
	}
 	
	public void verifyWithdrawalRequestReceived() {
 		By hdrWithdrawalRequestReceived = By.xpath("//h2[contains(text(),'Withdrawal request received')]");
		objPojo.getLogReporter().webLog("Verify 'Withdrawal request received' header displayed",  
				objPojo.getWebActions().click(hdrWithdrawalRequestReceived));
	}

	public void withdraw_UsingCard(String amount, String password) {
		this.setWithdrawAmount(amount);
		this.setPasswordToWithdraw(password);
		this.clickNext();
		this.verifyAreYouSureToWithdrawHeadeDisplayed();
		this.clickContinue();
		objPojo.getWebActions().switchToFrameUsingNameOrId("withdrawal-process");
		this.selectWithdrawMethod("card");
		this.clickWithdraw();
		this.verifyWithdrawalRequestReceived();
 	}
	
	
	public void setDepositAmt(String inpDepAmt)
	{
		By inputDepAmount= By.xpath("//input[@id='deposit-deposit-amount']");

		objPojo.getLogReporter().webLog(" Set/Enter the Deposit amount ",  
				objPojo.getWebActions().setTextWithClear(inputDepAmount, inpDepAmt));
	}
	
	public void validateBalanceAfterDeposit(String initialBal , String depAmt)
	{
		By BalanceOnHomepage = By.xpath("//li[@class='text-data']/a/div/span/strong"); 

		String HomePageBalAfterDep =objPojo.getWebActions().getText(BalanceOnHomepage).substring(1);

		float SumOfinitialBalDepAmt =Float.parseFloat(initialBal) + Float.parseFloat(depAmt) ;
		System.out.println("SumOfinitialBalDepAmt : "+SumOfinitialBalDepAmt);

		objPojo.getLogReporter().webLog(" Balance after deposit is  : "+HomePageBalAfterDep ,
				String.valueOf(Float.parseFloat(HomePageBalAfterDep)).equals(String.valueOf(SumOfinitialBalDepAmt)) );
	}
	public void setCardNumber(String cardNumber)
	{
		By inpCardNumber = By.id("cc_card_number");
		objPojo.getLogReporter().webLog("Set Card number ", cardNumber, 
				objPojo.getWebActions().setText(inpCardNumber, cardNumber));
	}
	public void setExpMonth(String expMon)
	{
		By inpExpMonth = By.xpath("//div[@data-id='cc_exp_month']");

		objPojo.getLogReporter().webLog("click expiry Month ", 
				objPojo.getWebActions().click(inpExpMonth));

		By monthFrmList = By.xpath("//select[@id='cc_exp_month']/option[@value='"+expMon+"']");

		objPojo.getLogReporter().webLog("Set expiry Month ", expMon, 
				objPojo.getWebActions().click(monthFrmList));
	}
	public void setExpYear(String expYr)
	{
		By inpExpYear = By.xpath("//div[@data-id='cc_exp_year']");

		objPojo.getLogReporter().webLog("click expiry year ", 
				objPojo.getWebActions().click(inpExpYear));

		By yearFrmList = By.xpath("//select[@id='cc_exp_year']/option[@value='"+expYr+"']");

		objPojo.getLogReporter().webLog("Set expiry Year ", expYr, 
				objPojo.getWebActions().click(yearFrmList));
	}
	public void setCVVcode(String securityCodeCVV) {

		By inpSecurityCodeCVV = By.id("cc_cvv2");
		objPojo.getLogReporter().webLog("Set security date (CVV) ", securityCodeCVV, 
				objPojo.getWebActions().setText(inpSecurityCodeCVV, securityCodeCVV));
	}
	public void validateSuccessMessage()
	{
		By confirmationHDR = By.xpath("//h4[contains(text(),'Confirmation')]");
		By successDepositedTXT = By.xpath("//p[contains(text(),'Successfully deposited')]");
		By closeBTN = By.xpath("//button[@id='close' and contains(text(),'Close')]");


		objPojo.getLogReporter().webLog(" Verify Confirmation HDR displayed ",  
				objPojo.getWebActions().checkElementDisplayed(confirmationHDR));
		objPojo.getLogReporter().webLog(" Verify Successfully Deposited TXT displayed ",  
				objPojo.getWebActions().checkElementDisplayed(successDepositedTXT));

		objPojo.getLogReporter().webLog(" Verify Close BTN displayed ",  
				objPojo.getWebActions().checkElementDisplayed(closeBTN));

		objPojo.getLogReporter().webLog(" Verify Close BTN displayed ",  
				objPojo.getWebActions().click(closeBTN));

	}

	public void Closedafterdeposit()
	{
		By closeBTN = By.xpath("//button[@class='modal_close btn on_confirm' and contains(text(),'Close')]");
		objPojo.getLogReporter().webLog(" Verify Close BTN displayed ",  
				objPojo.getWebActions().checkElementDisplayed(closeBTN));

		objPojo.getLogReporter().webLog(" Verify Close BTN displayed ",  
				objPojo.getWebActions().clickByJavaScript(closeBTN));

	}
	
}