package com.pagefactory.grosvenor;

import org.openqa.selenium.By;

import com.generic.Pojo;
import com.generic.utils.GenericUtils;

public class ResponsibleGamblingPage {

	private Pojo objPojo;
	
		By HowLongWouldMsgTakeBreak = By.xpath("//fieldset[@class='form-checkbox-buttons']/p[contains(text(),'How long would you like to take a break?')]");
		By inpPwd = By.xpath("//input[@id='input-password']");
		
		By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'Are you sure?')]");
		By InfoOnPopup = By.xpath("//p[contains(text(),'you are choosing to start your break. Your account will be locked until')]");
		By TakeBreakAndLogoutBtn = By.xpath("//button[contains(text(),'Take a break & LogOut')]");
		By PopupCancelBtn = By.xpath("//button[contains(text(),'Cancel')]");
		By PopupXBtn = By.xpath("//div[@id='modalTakeBreak']/div/div/div/button[@class='close']");
		By PopupConfirmBtn = By.xpath("//button[contains(text(),'Confirm')]");
		
		//Self Exclusion
    
    	By DoUFeelProblemWithGamblingTXT = By.xpath("//p[contains(text(),'you have a problem with gambling?')]");
		By btnYesSelfExclude = By.xpath("//div[@class='myaccount-menu']/ul/li/a[@class='reality-check' and contains(text(),'Yes')]");
		By btnNoSelfExclude = By.xpath("//div[@class='myaccount-menu']/ul/li/a[@class='reality-check' and contains(text(),'No')]");
		
		By YouCanBlockURSelfTXT = By.xpath("//div[@id='u37050']/p[contains(text(),'You can block yourself from playing with Grosvenor for a chosen period of time.')]");
		By WhySelfExcludeTXT = By.xpath("//div[@class='tell-more__text' and contains(text(),'Why Self Exclude?')]");
		By expandWhySelfExcludebtn = By.xpath("//span[@class='collapse-button tell-more__icon closed enabled']");
		By HowLongDoUWantToLockACTxt = By.xpath("//fieldset[@class='form-checkbox-buttons']/p[contains(text(),'How long do you want to lock your account for?')]");
		By WantToSelfExcludeBtn = By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Yes, I want to Self Exclude')]");
		
		//Self Exclusion-SAW	
		
		By radioBtnDigitalChannel = By.xpath("//input[@id='radio-digital']");
	 	By radioBtnRetailChannel = By.xpath("//input[@id='radio-retail']");
	 	By radioBtnBothChannel = By.xpath("//input[@id='radio-cross']");
	 	By LabelOnlineChannel = By.xpath("//label[@class='radio channel' and contains(text(),'Online')]");
	 	By LabelInClubChannel = By.xpath("//label[@class='radio channel' and contains(text(),'In club')]");
	 	By LabelBothChannel = By.xpath("//label[@class='radio channel' and contains(text(),'Both')]");
	
		//Reality Checks
		
		By RealityCheckInfo1 = By.xpath("//p[contains(text(),'Reality Checks are a helpful way of keeping track of the time you have spent playing our games.')]");
		By RealityCheckInfo2 = By.xpath("//p[contains(text(),'You can choose how often you would like to receive a Reality Check below. You will see a reminder each time you reach your chosen Reality Check time interval')]");
		By RealityCheckInfo3 = By.xpath("//p[contains(text(),'Your gaming session will begin when you place your first real money bet and will end when you log out.')]");
		By TellMeMorelnk = By.xpath("//div[contains(text(),'Tell me more')]");
		By TellMeMoreExpandBtn = By.xpath("//span[@class='collapse-button tell-more__icon closed enabled']");
		By setReminderTXT = By.xpath("//p[contains(text(),'Set your reminder')]");
		By selectReminderprd = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'30 mins')]");
		By saveChangesBTN = By.xpath("//button[contains(text(),'Save changes')]");
		By TellMeMoreCollapseBtn = By.xpath("//span[@class='collapse-button tell-more__icon opened enabled']");
		
		public ResponsibleGamblingPage(Pojo pojo){
			this.objPojo = pojo;
		}
		
		  public void verifyRealityChecksInfoMessages(String msg)
		     {
			  By RealityCheckInfo1 = By.xpath("//p[contains(text(),'Reality Checks are a helpful way of keeping track of the time you have spent playing our games.')]");
			  
			  objPojo.getLogReporter().webLog("Verify"+msg+" msg displayed",
		 				objPojo.getWebActions().checkElementDisplayed(RealityCheckInfo1));
		     }
		    
		public void verifyTellMeMoreExpandBtnDisplayed()
		{
			objPojo.getLogReporter().webLog("Verify Tell Me More expand button displayed",
					objPojo.getWebActions().checkElementDisplayed(TellMeMoreExpandBtn));
		}
		public void verifyTellMeMorelinkDisplayed()
		{
			objPojo.getLogReporter().webLog("Verify Tell Me More link displayed",
					objPojo.getWebActions().checkElementDisplayed(TellMeMorelnk));
		}
		
		//
		public void clickTellMeMoreExpandBtn()
		{
			objPojo.getLogReporter().webLog(" click on Tell Me More expand button displayed",
					objPojo.getWebActions().click(TellMeMoreExpandBtn));
		}
		
		public void clickTellMeMoreCollapseBtn()
		{
			objPojo.getLogReporter().webLog(" click on Tell Me More collapse button displayed",
					objPojo.getWebActions().click(TellMeMoreCollapseBtn));
		}
		
		
		public void verifyReminderOptionsDispalyed(String remprd)
		{
			if(remprd.contains("~"))
			{
				String[] arr1 = remprd.split("~");
				for (String remprd2 : arr1) 
				{
					By Reminderprd = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+remprd2+"')]");
			
					objPojo.getLogReporter().webLog(" Verify " +remprd2+" Reminder Period option displayed",
							objPojo.getWebActions().checkElementDisplayed(Reminderprd));
				}
			}
			
		}

		
		public void verifySetReminderTXTDisplayed()
		{
			objPojo.getLogReporter().webLog("Verify set Reminder TXT displayed",
					objPojo.getWebActions().checkElementDisplayed(setReminderTXT));
		}
		
		
		public void SelectReminderperiod(String remPeriod)
		{
			By selectReminderprd = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+remPeriod+"')]");
			
			objPojo.getLogReporter().webLog("Select Reminder Period Button displayed",
					objPojo.getWebActions().click(selectReminderprd));
		}
		
		public void verifySaveChangesBTNDisplayed()
		{
			objPojo.getLogReporter().webLog("Verify SaveChanges Button displayed",
					objPojo.getWebActions().checkElementDisplayed(saveChangesBTN));
		}
		public void clickSaveChangesBTN()
		{
			objPojo.getLogReporter().webLog("Verify SaveChanges Button displayed",
					objPojo.getWebActions().click(saveChangesBTN));
		}
		
		
		public void verifySavedBtnDisplayed()
		{
			By savedBtn =By.xpath("//fieldset[@class='form-buttons padded']/div/button[@class='saved']");
			objPojo.getLogReporter().webLog("Verify SaveChanges Button displayed",
					objPojo.getWebActions().checkElementDisplayed(savedBtn));
		}
		
		//Take A Break
	    
	    public void verifyInfoMsgTakeBreakDisplayed(){
	    	
	    	By infoMsgTakeBreak = By.xpath("//div[@class='component take-a-break']/div/p/p[contains(text(),'If you would like to take a temporary break from gambling')]");
			objPojo.getLogReporter().webLog("Verify If you would like to take a temporary break from gambling, you can choose a break period from 1 day up to 6 weeks. msg displayed",
					objPojo.getWebActions().checkElementDisplayed(infoMsgTakeBreak));
	    }
	    
	    
	    public void verifyInfoMessages(String msg)
	     {
	    	 By infoTxt01 = By.xpath("//div[@class='card card-block']/p[contains(text(),'"+msg+"')]");
	    	
	    	 objPojo.getLogReporter().webLog("Verify"+msg+" msg displayed",
	 				objPojo.getWebActions().checkElementDisplayed(infoTxt01));
	     }
	    
	    
	    public void verifyWhyTakeABreaklnkDisplayed(){
	    	By WhyTakeABreak = By.xpath("//div[@class='collapse-block__main']/div[contains(text(),'Why take a break?')]");
	    	By expandLnk = By.xpath("//div[@class='collapse-block__main']/span[@class='collapse-button tell-more__icon closed enabled']");
	    	
			objPojo.getLogReporter().webLog("Verify 	Why take a break? msg displayed",
					objPojo.getWebActions().checkElementDisplayed(WhyTakeABreak));
			
			objPojo.getLogReporter().webLog("Verify Expand link displayed next to Why take a break? msg",
					objPojo.getWebActions().checkElementDisplayed(expandLnk));
	    }
	  
	    public void verifyHowLongWouldTakeBreakMsgDisplayed(){
			objPojo.getLogReporter().webLog("Verify How long would you like to take a break? msg displayed",
					objPojo.getWebActions().checkElementDisplayed(HowLongWouldMsgTakeBreak));
	    }
	    
	     public void selectTakeABreakPeriod(String brkPeriod)
	    {
	    	By BreakPeriod = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+brkPeriod+"')]");
	     	objPojo.getLogReporter().webLog("Select Take a Break Period",
					objPojo.getWebActions().click(BreakPeriod));
	   }
	     
	     public void verifyPasswordFieldDisplayed()
	     {
	    	 objPojo.getLogReporter().webLog("Verify Password field displayed",
	 				objPojo.getWebActions().checkElementDisplayed(inpPwd));
	     }
	     
	     public void inputPassword(String pwd)
	     {
	    			 
	    	 objPojo.getWaitMethods().sleep(8);	 
	  		//objPojo.getWebActions().scrollUsingTouchActions_ByElements(objPojo.getWebActions().
	  		//processMobileElement(HowLongWouldMsgTakeBreak), objPojo.getWebActions().processMobileElement(lnkLiveHelp)));*/
	     
	    		objPojo.getLogReporter().webLog("Enter Password to take a break / Self Exclude",
	    				objPojo.getWebActions().setText(inpPwd,pwd));
	      }
	     public void ClickTakeBreakBtnDisplayed()
	     {
	    	 By btnTakeABreak = By.xpath("//button[@type='submit' and contains(text(),'Take a Break')]");
	    	
	     	 objPojo.getLogReporter().webLog("Verify Take a break button displayed",
	 				objPojo.getWebActions().checkElementDisplayed(btnTakeABreak));
	    
	    	 objPojo.getLogReporter().webLog("Click on the Take a Break Period",
	 				objPojo.getWebActions().click(btnTakeABreak));
	     }
	     
	    public void verifyConfirmationPopupDisplayed()
	 	{
	 		By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'Are you sure?')]");
	 		
	 		objPojo.getLogReporter().webLog("Are you sure? Header is displayed on Popup",
	 				objPojo.getWebActions().checkElementDisplayed(AreYouSurePopupHDR));
	 		
	 		By InfoOnPopup = By.xpath("//p[contains(text(),'you are choosing to start your break. Your account will be locked until')]");
	 		objPojo.getLogReporter().webLog("Popup Msg containing text -you are choosing to start your break. Your account will be locked until is displayed",
	 				objPojo.getWebActions().checkElementDisplayed(InfoOnPopup));
	 	}
	 	
	 	public void verifyTakeBreakAndLogoutBtnOnPopup()
	 	{ 		
	 		objPojo.getLogReporter().webLog("Verify Take a Break And Logout Btn is displayed on popup",
	 				objPojo.getWebActions().checkElementDisplayed(TakeBreakAndLogoutBtn));
	 	}
	 	
	 	public void clickTakeBreakAndLogoutBtn(){
	 		
	 		objPojo.getLogReporter().webLog("Click on Take a Break And Logout  Button",
	 				objPojo.getWebActions().click(TakeBreakAndLogoutBtn));
	 		}
	 	
	 	public void verifyCancelBtnOnPopupDisplayed()
	 	{
	 		objPojo.getLogReporter().webLog("Verify cancel Btn is displayed on popup",
	 				objPojo.getWebActions().checkElementDisplayed(PopupCancelBtn ));
	 	}
	 	public void clickTakeBreakPopupCancelBtn(){
	 		objPojo.getLogReporter().webLog("Click on Cancel  Button on popup",
	 				objPojo.getWebActions().click(PopupCancelBtn ));
	 		}
	     
	     public void verifyErrorMessageDisplayedOnLogin(String msg)
	     {
	    	 By errorUserOnBreak = By.xpath("//div[@class='error-msg']/ul/li[contains(text(),'"+msg+"')]");
	    	 
	    	 objPojo.getLogReporter().webLog("Verify "+msg+" error message displayed on login screen ",
	  				objPojo.getWebActions().checkElementDisplayed(errorUserOnBreak));
	     }
	     
	    //Self Exclusion
	     
	     	public void verifyDoUFeelProblemWithGamblingTXT()
		 	{
		 		objPojo.getLogReporter().webLog("Do U Feel Problem With Gambling text is displayed",
		 				objPojo.getWebActions().checkElementDisplayed(DoUFeelProblemWithGamblingTXT));
		 	}
		 	
		 	public void verifybtnYesSelfExcludeDisplayed(){
		 		objPojo.getLogReporter().webLog("Yes button displayed",
		 				objPojo.getWebActions().checkElementDisplayed(btnYesSelfExclude));
		     }
		 	
		 	public void verifybtnNoSelfExcludeDisplayed(){
		 		objPojo.getLogReporter().webLog("No button displayed",
		 				objPojo.getWebActions().checkElementDisplayed(btnNoSelfExclude));
		     }
		 	public void clickYesbtn(){
		 		objPojo.getLogReporter().webLog("Click on Yes Button",
		 				objPojo.getWebActions().click(btnYesSelfExclude));
		 	}
		 	
		 	public void verifyYouCanBlockURSelfTXT()
		 	{
		 		objPojo.getLogReporter().webLog("You Can Block URSelf from playing... text is displayed",
		 				objPojo.getWebActions().checkElementDisplayed(YouCanBlockURSelfTXT));
		 	}
		 	
		 	public void verifyHowLongDoUWantToLockACTxt()
		 	{
		 		objPojo.getLogReporter().webLog("How Long Do U Want To Lock Account'text is displayed",
		 				objPojo.getWebActions().checkElementDisplayed(HowLongDoUWantToLockACTxt));
		 	}
		 	
		 	public void verifyWhySelfExcludeTXT()
		 	{
		 		objPojo.getLogReporter().webLog("'Why Self Exclude? text is displayed",
		 				objPojo.getWebActions().checkElementDisplayed(WhySelfExcludeTXT));
		 		objPojo.getLogReporter().webLog("Expand button next to 'Why Self Exclude?  is displayed",
		 				objPojo.getWebActions().checkElementDisplayed(expandWhySelfExcludebtn));
		 			
		 		objPojo.getLogReporter().webLog(" click on Expand button next to 'Why Self Exclude?  is displayed",
		 				objPojo.getWebActions().click(expandWhySelfExcludebtn));
		 	}
		     public void selectSelfExcludePeriod(String exclPeriod)
		     {
		     	By ExcludePeriod = By.xpath("//div[@class='panel panel-default']/button[contains(text(),'"+exclPeriod+"')]");
		      	objPojo.getLogReporter().webLog("Select Self Exclusion Period",
		 				objPojo.getWebActions().click(ExcludePeriod));
		    }
		     public void verifyWantToSelfExcludeBtnDisplayed()
		     {
		    	 objPojo.getLogReporter().webLog("Verify Yes, I want to self exclude button displayed",
		  				objPojo.getWebActions().checkElementDisplayed(WantToSelfExcludeBtn));
		     }
		     public void clickWantToSelfExcludeBtn()
		     {
		    	 objPojo.getLogReporter().webLog(" Click on Yes, I want to self exclude button",
		  				objPojo.getWebActions().click(WantToSelfExcludeBtn));
		     }
		
		     public void verifySelfExclInfoMessages(String msg)
		     {
		    	 By infoTxt01 = By.xpath("//div[@class='card card-block']/ul/li[contains(text(),'"+msg+"')]");
		    	
		    	 objPojo.getLogReporter().webLog("Verify"+msg+" msg displayed",
		 				objPojo.getWebActions().checkElementDisplayed(infoTxt01));
		     }
	     
	    
		     public void verifyExclusionConfirmationPopupDisplayed()
			 	{
			 		By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'ARE YOU SURE?')]");
			 		
			 		objPojo.getLogReporter().webLog("Are you sure? Header is displayed on Popup",
			 				objPojo.getWebActions().checkElementDisplayed(AreYouSurePopupHDR));
			 		
			 		By InfoOnPopup = By.xpath("//p[contains(text(),'You are about to self-exclude from gambling online with Grosvenor until :')]");
			 		objPojo.getLogReporter().webLog("Popup Msg containing text -You are about to self-exclude from gambling online with Grosvenor until : is displayed",
			 				objPojo.getWebActions().checkElementDisplayed(InfoOnPopup));
			 	}
		       public void verifyConfirmBtnOnPopup()
			 	{ 	
		    	 	objPojo.getLogReporter().webLog("Verify Confirm Button is displayed on popup",
			 				objPojo.getWebActions().checkElementDisplayed(PopupConfirmBtn));
			 	}
			 	
			 	public void clickConfirmBtn(){
			 		
			 		objPojo.getLogReporter().webLog("Click on Confirm Button Button",
			 				objPojo.getWebActions().click(PopupConfirmBtn));
			 	}
			 	
			 	public String calculateExclusionDate(String brktime)
			 	{
					String[]	brkYear= brktime.split(" ");
					String brkdate= GenericUtils.getRequiredDateWithCustomYear(brkYear[0], "dd/MM/YYYY", "");
					return brkdate;
			 		
			 	}
			 	
			 	 public void verifySelfExclusionCompletePopup()
				 	{ 		
			 		By SelfExclusionCompleteHDR = By.xpath("//h4[contains(text(),'SELF-EXCLUSION COMPLETE')]");
			 		By InfoOnPopup = By.xpath("//p[contains(text(),'Your account is locked until:')]");
			    	 	
			 		objPojo.getLogReporter().webLog("Verify SELF-EXCLUSION COMPLETE header is displayed on popup",
				 				objPojo.getWebActions().checkElementDisplayed(SelfExclusionCompleteHDR));
			 		
			 		objPojo.getLogReporter().webLog("Verify To extend your break from online gambling,.... text is displayed on popup",
			 				objPojo.getWebActions().checkElementDisplayed(InfoOnPopup));
			 	 	}
			 	 
			 	 public void clickOKonPopup()
			 	 {
			 		 By OkPopup = By.xpath("//button[@id='modal-opt-out' and contains(text(),'OK')]");
			 		 
			 		objPojo.getLogReporter().webLog("Click on OK button displayed on popup",
			 				objPojo.getWebActions().click(OkPopup));
			  	 }
			 	public void verifySelfExcludeNLogOutBtnOnPopup()
			 	{ 		
		    	 By locator = By.xpath("//button[@id='modal-opt-out' and contains(text(),'Self exclude and log out')]");
		    	 	objPojo.getLogReporter().webLog(" Verify Self exclude and log out Button is displayed on popup",
			 				objPojo.getWebActions().checkElementDisplayed(locator));
			 	}
			 	
				public void verifySelfExcludeBtnOnPopup()
			 	{ 		
		    	 By locator = By.xpath("//button[@id='modal-opt-out' and contains(text(),'Self exclude')]");
		    	 	objPojo.getLogReporter().webLog(" Verify Self exclude Button is displayed on popup",
			 				objPojo.getWebActions().checkElementDisplayed(locator));
			 	}
			 	
			 	public void clickSelfExcludeNLogOutBtn(){
			 		By locator = By.xpath("//button[@id='modal-opt-out' and contains(text(),'Self exclude and log out')]");
			 		objPojo.getLogReporter().webLog("Click on Self exclude and log out Button Button",
			 				objPojo.getWebActions().click(locator));
			 	}
			 	public void clickSelfExcludeBtn(){
			 		By locator = By.xpath("//button[@id='modal-opt-out' and contains(text(),'Self exclude')]");
			 		objPojo.getLogReporter().webLog("Click on Self exclude Button Button",
			 				objPojo.getWebActions().click(locator));
			 	}
		        
			 	 public void validateExclusionPeriod(String exclPeriod)
			 	 {
			 		 By lockUntilDate = By.xpath("//div[@class='lock-date']/p[contains(text(),'Lock until:')]");
			 		 
			 		String[] lockuntildate= (objPojo.getWebActions().getText(lockUntilDate)).split(": ");
			 		
			 		System.out.println("lock untill date displayed on site : "+lockuntildate[1]);
			 		
			 		String exclusionPeriod= this.calculateExclusionDate(exclPeriod);
			 		System.out.println("exclusionPeriod selected : "+exclusionPeriod);
			 		
			 		objPojo.getLogReporter().webLog("Self exclusion period selected matches with the date displayed on screen :",
			 				exclusionPeriod.matches(lockuntildate[1]));
			 	
			 	 }
			 	 
			 	 //Self Exclusion-SAW
			 	 
			 	public void verifyWhichChannelToSelectTXTDisplayed()
			 	{
			 		By WhichChannelToSelectTXT = By.xpath("//p[contains(text(),'Which channels do you want to be excluded from?')]");
			 		
			 		objPojo.getLogReporter().webLog(" Verify Which channels do you want to be excluded from? text is displayed",
			 				objPojo.getWebActions().checkElementDisplayed(WhichChannelToSelectTXT));
			 	}
			 	
			 	public void verifyradioBtnOnlineChannelDisplayed()
			 	{
			 		objPojo.getLogReporter().webLog("Verify Radio Button - Online Channel displayed ",
			 				objPojo.getWebActions().checkElementDisplayed(radioBtnDigitalChannel));
			 	}
			 	
			 	public void verifyLabelOnlineChannelDisplayed()
			 	{
			 		objPojo.getLogReporter().webLog("Verify Label - Online Channel displayed ",
			 				objPojo.getWebActions().checkElementDisplayed(LabelOnlineChannel));
			 	}
			 	
			 	
				public void verifyTXTdisplayedOnlineSelectedDisplayed()
			 	{
					By TXTdisplayedOnlineSelected = By.xpath("//p[contains(text(),'You will no longer be able to log in, access the mobile app or deposit funds electronically. You will also be excluded from our online marketing communications and any accounts you may have on other sites owned by The Rank Group. If you have any cleared online-only funds, these will be returned to you.')]");
					
			 		objPojo.getLogReporter().webLog(" Verify 'You will no longer be able to log in, access the mobile app or deposit funds electronically....' Text displayed after selecting Online radio btn",
			 				objPojo.getWebActions().checkElementDisplayed(TXTdisplayedOnlineSelected));
			 	}
			 	
			 	
			 	public void SelectradioBtnDigitalChannel()
			 	{
			 		objPojo.getLogReporter().webLog("Verify Radio Button - Digital Channel displayed ",
			 				objPojo.getWebActions().click(radioBtnDigitalChannel));
			 	}
				public void verifyradioBtnRetailChannelDisplayed()
			 	{
					objPojo.getLogReporter().webLog("Verify Radio Button - Retail Channel displayed ",
			 				objPojo.getWebActions().checkElementDisplayed(radioBtnRetailChannel));
			 	}
				
				
				
				public void verifyLabelInClubChannelDisplayed()
			 	{
			 		objPojo.getLogReporter().webLog("Verify Label - InClub Channel displayed ",
			 				objPojo.getWebActions().checkElementDisplayed(LabelInClubChannel));
			 	}
				
				
				public void SelectradioBtnRetailChannel()
			 	{
			 		objPojo.getLogReporter().webLog("Verify Radio Button - Retail Channel displayed ",
			 				objPojo.getWebActions().click(radioBtnRetailChannel));
			 	}
				public void verifyTXTdisplayedRetailSelectedDisplayed()
			 	{
					By TXTdisplayedRetailSelected = By.xpath("//p[contains(text(),'You will no longer be able to enter any of our clubs and will be excluded from any club marketing communications. If you have any in-club-only funds, these will be returned to you.')]");
					 
			 		objPojo.getLogReporter().webLog(" Verify 'You will no longer be able to enter any of our clubs and will be excluded....' Text displayed after selecting Retail radio btn",
			 				objPojo.getWebActions().checkElementDisplayed(TXTdisplayedRetailSelected));
			 	}
				
				public void verifyradioBtnBothChannelDisplayed()
			 	{
					objPojo.getLogReporter().webLog("Verify Radio Button - Both Channel displayed ",
			 				objPojo.getWebActions().checkElementDisplayed(radioBtnBothChannel));
			 	}
				
				public void verifyLabelBothChannelDisplayed()
			 	{
			 		objPojo.getLogReporter().webLog("Verify Label - Both Channel displayed ",
			 				objPojo.getWebActions().checkElementDisplayed(LabelBothChannel));
			 	}
				
				public void SelectradioBtnBothChannel()
			 	{
			 		objPojo.getLogReporter().webLog("Verify Radio Button - Both Channel displayed ",
			 				objPojo.getWebActions().click(radioBtnBothChannel));
			 	}
				public void verifyTXTdisplayedBOTHSelectedDisplayed()
			 	{
					By TXTdisplayedBothSelected = By.xpath("//p[contains(text(),'You will no longer be able to log in, access the mobile app or deposit any funds. Neither will you be able to enter any of our clubs, receive any marketing communications, or access any accounts you may have on other websites owned by The Rank Group.')]");
 
			 		objPojo.getLogReporter().webLog(" Verify 'You will no longer be able to log in, access the mobile app or deposit any funds. Neither will you be able to enter any of our clubs....' Text displayed after selecting BOTH radio btn",
			 				objPojo.getWebActions().checkElementDisplayed(TXTdisplayedBothSelected));
			 	}
				
				public void verifyAreUSurePopupDisplayed()
			 	{
			 		By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'ARE YOU SURE?')]");
			 		
			 		objPojo.getLogReporter().webLog("Are you sure? Header is displayed on Popup",
			 				objPojo.getWebActions().checkElementDisplayed(AreYouSurePopupHDR));
			 		
			 		By InfoOnPopup = By.xpath("//p[contains(text(),'You are about to self-exclude from gambling with Grosvenor both online and in club until :')]");
			 		objPojo.getLogReporter().webLog("Popup Msg containing text -,'You are about to self-exclude from gambling with Grosvenor both online and in club until :",
			 				objPojo.getWebActions().checkElementDisplayed(InfoOnPopup));
			 	}
		
				public void verifyExcludeRetail_AreUSurePopupDisplayed()
			 	{
			 		By AreYouSurePopupHDR = By.xpath("//h4[contains(text(),'ARE YOU SURE?')]");
			 		
			 		objPojo.getLogReporter().webLog("Are you sure? Header is displayed on Popup",
			 				objPojo.getWebActions().checkElementDisplayed(AreYouSurePopupHDR));
			 		
			 		By InfoOnPopup = By.xpath("//p[contains(text(),'You are about to self-exclude from gambling in Grosvenor clubs until:')]");
			 		objPojo.getLogReporter().webLog("Popup Msg containing text -'You are about to self-exclude from gambling in Grosvenor clubs until:",
			 				objPojo.getWebActions().checkElementDisplayed(InfoOnPopup));
			 	}
		 

}
