package com.pagefactory.grosvenor;
import org.openqa.selenium.By;

import com.generic.Pojo;

public class Footer {



	// Local variables
	private Pojo objPojo;

		//Useful Links
	private By linkSlotsandGames = By.xpath("//a[text()='Slots & Games'][@href='/slots-and-games']");
    private By linkOnlinePoker = By.xpath("//a[text()='Online Poker'][@href='/poker']");
    private By linkLiveCasino = By.xpath("//a[text()='Live Casino'][@href='/live-casino']");
    private By linkRouletteGames = By.xpath("//a[text()='Roulette Games'][@href='/table-games/roulette']");
	private By linkJackpots = By.xpath("//a[text()='Jackpots'][@href='/jackpots']");
	private By linkBlackjack = By.xpath("//a[text()='Blackjack'][@href='/live-casino/live-blackjack']");
	private By linkSportsBetting = By.xpath("//a[text()='Sports Betting'][@href='/sport']");
	private By linkGrosvenorBlog = By.xpath("//a[text()='Grosvenor Blog'][@href='https://blog.grosvenorcasinos.com/']");
	private By linkThismonthspromotions = By.xpath("//a[text()='This month's promotions'][@href='/promotions']");
	private By linkGrosvenorOne = By.xpath("//a[text()='Grosvenor One'][@href='/grosvenor-one']");

	//Help and Advice
	private By linkLiveHelp = By.xpath("//a[text()='Live Help'][@href='https://rank.secure.force.com/chat?cid=9e3887e44789c460c5ea2de80f94cc7b#/liveSupport']");
	private By linkComplaintsProcess = By.xpath("//a[text()='Complaints Process'][@href='/static/contactus']");
	private By linkResponsibleGambling = By.xpath("//a[text()='Responsible Gambling'][@href='https://keepitfun.rank.com/']");
	private By linkDepositLimits = By.xpath("//a[text()='Deposit Limits'][@href='/#menu/ResponsibleGambling/deposit_limits']");
	private By linkWorkforGrosvenor = By.xpath("//a[text()='Work for Grosvenor'][@href='https://careers.smartrecruiters.com/TheRankGroup/grosvenor-casino']");
	private By linkTermsandConditions = By.xpath("//a[text()='Terms & Conditions'][@href='/static/termsandconditions']");
	private By linkPrivacyPolicy = By.xpath("//a[text()='Privacy Policy'][@href='/static/privacy-policy']");
	private By linkAffiliates = By.xpath("//a[text()='Affiliates'][@href='http://www.grosvenoraffiliates.com/']");
	private By linkCarersTrust = By.xpath("//a[text()='Carers Trust'][@href='/grosvenor-and-carers-trust']");
	
	
	//Constructor
	public Footer(Pojo pojo){
		this.objPojo = pojo;
	}



	public void verifyUsefulLinksDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'Slots & Games link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkSlotsandGames));
		objPojo.getLogReporter().webLog("Verify 'Online Poker link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkOnlinePoker));
		objPojo.getLogReporter().webLog("Verify 'Live Casino link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkLiveCasino));
		objPojo.getLogReporter().webLog("Verify 'Roulette Games link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkRouletteGames));
		objPojo.getLogReporter().webLog("Verify 'Jackpots link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkJackpots));
		
		objPojo.getLogReporter().webLog("Verify 'Blackjack link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkBlackjack));
		objPojo.getLogReporter().webLog("Verify 'Sports Betting' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkSportsBetting));
		objPojo.getLogReporter().webLog("Verify 'Grosvenor Blog link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkGrosvenorBlog));
		objPojo.getLogReporter().webLog("Verify 'Grosvenor One link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkGrosvenorOne));

	
	
	
	}

	public void verifyHelpLinksDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'Live Help link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkLiveHelp));
		objPojo.getLogReporter().webLog("Verify 'Complaints Process link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkComplaintsProcess));
		objPojo.getLogReporter().webLog("Verify 'Responsible Gambling link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkResponsibleGambling));
		objPojo.getLogReporter().webLog("Verify 'Deposit Limits' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkDepositLimits));
		objPojo.getLogReporter().webLog("Verify 'Work for Grosvenor link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkWorkforGrosvenor));
	
		objPojo.getLogReporter().webLog("Verify 'Terms & Conditions link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkTermsandConditions));
		
		objPojo.getLogReporter().webLog("Verify 'Privacy Policy link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkPrivacyPolicy));
	
		objPojo.getLogReporter().webLog("Verify 'Affiliates link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkAffiliates));
		objPojo.getLogReporter().webLog("Verify 'Carers Trust link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(linkCarersTrust));
	
	}

	public void verifyURLRedirection(String expectedurl) {
		String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
		System.out.println("currentURL "+currentURL);
		System.out.println("currentURL "+currentURL+" exp "+expectedurl);
		objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(expectedurl));
	}

	public void clickOnFooterLinks(String sectionName,String lnk)
	{
		if(lnk.contains("~"))
		{
			String[] arr1 = lnk.split("~");

			for (String labels2 : arr1) 
			{
				String[] lnks = labels2.split(",");
				By locator = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnks[0]+"']");

				objPojo.getLogReporter().webLog(" click on  '"+lnks[0]+"'  link is displayed under "+sectionName+" footer section",  
						objPojo.getWebActions().click(locator));
				verifyURLRedirection(lnks[1]);
			}
		}
		else
		{
			By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnk+"']");
			objPojo.getLogReporter().webLog("click on  " +lnk+ "link is displayed under "+sectionName+"section ", 
					objPojo.getWebActions().clickByJavaScript(lnks));
		}
		
	}

	public void verifyFooterLinks(String sectionName,String lnk)
	{

		if(lnk.contains("~"))
		{
			String[] arr1 = lnk.split("~");

			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+labels2+"']");

				objPojo.getLogReporter().webLog(" Verify link : '"+labels2+"' is displayed under "+sectionName+" footer section",  
						objPojo.getWebActions().checkElementDisplayed(locator));
			}
		}
		else
		{
			By lnks = By.xpath("//span[text()='"+sectionName+"']//following-sibling::ul//li//a[text()='"+lnk+"']");
			objPojo.getLogReporter().webLog("verify  " +lnk+ "under "+sectionName+" section ", 
					objPojo.getWebActions().checkElementDisplayed(lnks));
		}
	}
	public void clickResponsibleGamblingLink() {
		String url = "https://keepitfun.rank.com/";
		objPojo.getLogReporter().webLog("Click 'Responsible Gambling Link'", 
				objPojo.getWebActions().click(linkResponsibleGambling));
		objPojo.getWebActions().switchToChildWindow();
		String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
		objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
	}


}
