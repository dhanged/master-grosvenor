
package com.pagefactory.grosvenor;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class DepositLimitPage {
	private Pojo objPojo;

	public DepositLimitPage(Pojo pojo){
		this.objPojo = pojo;
	}

	

	By inputDepositLimit = By.xpath("//input[@id='joinnow-depositLimit']");
	By btnSetLimit = By.xpath("//button[contains(.,'Set limit')]");


	public void clickOnSetLimit()
	{
		objPojo.getLogReporter().webLog("click on Set limit ",
				objPojo.getWebActions().click(btnSetLimit));	
	}

	public void setDepositLimit(String amt)
	{
		objPojo.getLogReporter().webLog("Set deposit limit as ", amt,
				objPojo.getWebActions().setText(inputDepositLimit, amt));
	}

	public void verifyWerecommendthatyousetdepositlimitstohelpyoumanageyourspendingInfoText()
	{
		By infoTxt = By.xpath("//div[@class='dialog-box limit-dialogue']//p[contains(.,'We recommend that you set deposit limits to help you manage your spending.')]");
		objPojo.getLogReporter().webLog("Verify  'We recommend that you set deposit limits to help you manage your spending.' ", 
				objPojo.getWebActions().checkElementDisplayed(infoTxt));
	}

	public String getDepositLimit(String lmt)
	{
		By txtDepositLimit = By.xpath("//p[contains(text(),'Current limit')]//following-sibling::table//tr//td[contains(.,'"+lmt+"')]//following-sibling::td");
		return objPojo.getWebActions().getText(txtDepositLimit);
	}
	
	public void verifyDepositLimitIsSetSuccessfullyOrNot(String lmt,String value)
	{
		By txtDepositLimit = By.xpath("//p[contains(text(),'Current limit')]//following-sibling::table//tr//td[contains(.,'"+lmt+"')]//following-sibling::td[contains(.,'"+value+"')]");
		objPojo.getLogReporter().webLog(lmt+ " Limit is displayed correctly ",
				objPojo.getWebActions().checkElementDisplayed(txtDepositLimit));
	}
	
	public void selectDepositLimit(String limitLbl)
	{
		By btnDepositLimit = By.xpath("//fieldset[@class='form-checkbox-buttons']//div//button[contains(text(),'"+limitLbl+"')]");
		objPojo.getLogReporter().webLog("select limit as", limitLbl,
				objPojo.getWebActions().click(btnDepositLimit));
	}
	
	public void verifyInfoText(String txt)
	{
		By infoText = By.xpath("//p[contains(.,'"+txt+"')]");
		objPojo.getLogReporter().webLog("Verify ",txt, 
				objPojo.getWebActions().checkElementDisplayed(infoText));
	}
	public void  verifyDepositLimitInfoText(String txt)
	{
		if(txt.contains("~")) 
		{
			String[] arrText = txt.split("~");
			for(String infoTxt : arrText) {
			By locator = By.xpath("//div[@class='info-list']//ul//li[contains(.,'"+infoTxt+"')]");
			objPojo.getLogReporter().webLog("verify", infoTxt,objPojo.getWebActions().checkElementDisplayed(locator));
			}
		}
		else {
			By locator = By.xpath("//div[@class='info-list']//ul//li[contains(.,'"+txt+"')]");
			objPojo.getLogReporter().webLog("verify", txt,objPojo.getWebActions().checkElementDisplayed(locator));
		}	
	}
	
	


	public void verifyDepositLimitErrorMessage(String msg)
	{
		By errMsg = By.xpath("//ul[@class='errors']//li[contains(.,'"+msg+"')]");
		objPojo.getLogReporter().webLog("Verify", msg,
				objPojo.getWebActions().checkElementDisplayed(errMsg));
	}
	
	public void verifyDepositLimitConfirmationMessage()
	{
		By  txtConfirmation = By.xpath("//p[@class='heading'][contains(.,'Confirmation')]//following::i[@class='confirmation-tick']");
		objPojo.getLogReporter().webLog("Verify 'Deposit limit confirmation 'message ",
				objPojo.getWebActions().checkElementDisplayed(txtConfirmation));	
		this.verifyInfoText("Your limits have been updated successfully");
	}
	
	
	public void verifyDepositLimitPendingRequestReceivedMessage()
	{
		By  txtConfirmation = By.xpath("//p[@class='heading'][contains(.,'Request received')]//following::i[@class='request-tick']");
		objPojo.getLogReporter().webLog("Verify 'Request received 'message ",
				objPojo.getWebActions().checkElementDisplayed(txtConfirmation));
		
		this.verifyInfoText("After 24 hours you will be able to activate your new limit.");
	}
	
	public void clickOnClose()
	{
		By btnClose = By.xpath("//div[@class='conf-button']//button[contains(.,'Close')]");
		objPojo.getLogReporter().webLog("Click on 'Close'",
				objPojo.getWebActions().click(btnClose));
	}
	public void verifyActivateLimitButton()
	{
		By  btnActivatelimit = By.xpath("//button[contains(.,'Activate limit')]");
		objPojo.getLogReporter().webLog("Verify 'Activate limit' cta ",
				objPojo.getWebActions().checkElementDisplayed(btnActivatelimit));
				
	}
	
	public void clickOnResetLimit()
	{
		By lnkResetLimit = By.xpath("//a[@class='input-inner-link'][contains(.,'Reset Limit')]");
		objPojo.getLogReporter().webLog("Click on 'Reset Limit''",
				objPojo.getWebActions().click(lnkResetLimit));
	}
	public void verifyCancelLink()
	{
		By lnkCancel = By.xpath("//a[contains(.,'Cancel')]");
		objPojo.getLogReporter().webLog("Verify 'Cancel' link ",
				objPojo.getWebActions().checkElementDisplayed(lnkCancel));
		
	}
//	
	public void clickOnCancelLink()
	{
		By lnkCancel = By.xpath("//a[contains(.,'Cancel')]");
		objPojo.getLogReporter().webLog(" 'Cancel' link ",
				objPojo.getWebActions().checkElementDisplayed(lnkCancel));
		
	}
//
	
	public String getPendingDepositLimit(String lmt)
	{
		By txtDepositLimit = By.xpath("//p[contains(text(),'Pending limit')]//following::table//tr//td[contains(.,'"+lmt+"')]//following-sibling::td");
		return objPojo.getWebActions().getText(txtDepositLimit);
	}
	
	public void verifyPendingLimitIsSetSuccessfully(String lmt,String value)
	{
		
		By txtDepositLimit = By.xpath("//p[contains(text(),'Pending limit')]//following::table//tr//td[contains(.,'"+lmt+"')]//following-sibling::td[contains(.,'"+value+"')]");
		objPojo.getLogReporter().webLog("Pending limit for "+lmt+ " is displayed correctly",
				objPojo.getWebActions().checkElementDisplayed(txtDepositLimit));
	}
	

	public void verifyDepositLimitErrorMesageOnDepositPage(String msg)
	{
		By errMsg = By.xpath("//div[@class='error-msg']//li[text()='"+msg+"']");
		objPojo.getLogReporter().webLog(msg+ " is displayed correctly ",
				objPojo.getWebActions().checkElementDisplayed(errMsg));
	}
}



