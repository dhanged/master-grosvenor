package com.pagefactory.grosvenor;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class MainPage {

	// Local variables
	private Pojo objPojo;

	// Input
	private By inpUserName = By.xpath("//input[@id='input-username']");
	private By inpPassword = By.xpath("//input[@id='input-password']");
	private By inpUsernameReminderEmailAddress = By.xpath("//input[@id='fu-email']");
	private By inpResetPasswordUserName = By.xpath("//input[@placeholder='Username' and @autocomplete='new-password']");
	private By inpResetPINEmailAddress = By.xpath("//input[@id='login-membership-number']");	
	private By inpNewPasswordForPasswordReset = By.xpath("//input[@id='password' and @placeholder='New Password']");
	private By inpConfirmPasswordForPasswordReset = By.xpath("//input[@id='confirmPassword' and @placeholder='Confirm Password']");

	// Button 
	private By btnContinue = By.xpath("//div[@class='cookie-message-text']/following-sibling::button[text()='Continue']");
	private By btnOpenLogin = By.xpath("//button[@class='open-login' and text()='Login']");
	private By btnLogin = By.xpath("//button[contains(@class,'loginBtn') and text()='Login']");
	private By btnVerify = By.xpath("//button[@type='submit' and text()='Verify']");
	private By btnJoinNow = By.xpath("//li[@class='join-now-btn']/a[@class='btn' and text()='Sign Up']");
	private By btnSendUsernameReminder = By.xpath("//button[text()='Send username reminder']");
	private By btnSubmitForResetPassword = By.xpath("//button[@type='submit' and text()='Submit']");
	private By btnSubmitForMembershipNumberReminder = By.xpath("//button[@type='submit' and text()='Submit']");
	private By btnResetPasswordForPasswordReset = By.xpath("//button[@type='submit' and text()='Reset Password']");
	private By btnRequestANewPIN = By.xpath("//button[@type='submit' and text()='Request a new PIN']");
	// Link
	private By lnkForgottenUserName = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='Username']");
	private By lnkForgottenPassword = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='Password']");
	private By lnkForgottenPIN = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='PIN']");
	private By lnkForgottenMembershipNumber = By.xpath("//p[@class='useful-links' and contains(text(),'Forgotten')]/a[text()='Membership Number']");
	private By lnkLogout = By.xpath("//a[@class='logout-link' and text()='Logout']");

	// Checkbox

	// Logo
	private By logoGC = By.xpath("//a[@class='logo']/img[contains(@src,'grosvenor-casinos/g_logo.png')]");

	// Link 
	private By lnkMyAccount = By.xpath("//a[@class='open-myaccount']");
 
	// header
	private By hdrLogin = By.xpath("//span[text()='Login']");
	private By hdrUsernameReminder = By.xpath("//a[text()='Username']");
	private By hdrMembershipNumberReminder = By.xpath("//a[text()='Membership Card Number']");
	private By hdrResetPassword = By.xpath("//a[text()='Password']");
	private By hdrResetPIN = By.xpath("//a[text()='PIN']");
	private By hdrYouHaveMail = By.xpath("//h4[text()='You have mail']");
	
	
	//Header
	private By hdrlivecasinoTab = By.xpath("//*[@id=\"header\"]/header/nav/ul/li[2]/a");
	private By hdrSlotsandGamesTab = By.xpath("//*[@id=\"header\"]/header/nav/ul/li[3]/a");
	private By hdrJackpotsTab = By.xpath("//a[@class='money'][@href='/jackpots']");
	private By hdrTableGamesTab = By.xpath("//a[@class='chip-solid'][@href='/table-games']");
	private By hdrSportTab = By.xpath("//a[@class='football'][@href='/sport']");
	private By hdrPokerTab = By.xpath("//a[@class='hearts'][@href='/poker']");
	private By hdrPromosTab = By.xpath("//a[@class='star'][@href='/promotions']");
	private By hdrGrosvenorOneTab = By.xpath("//a[@class='grosvenor-one'][@href='/grosvenor-one']");
	private By hdrLocalCasinosTab = By.xpath("//a[@class='house active'][@href='/local-casinos']");
	
	
	
	
	
	public MainPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifyGrosvenorCasinosLogoDisplayed(){
		objPojo.getLogReporter().webLog("Verify Grosvenor Casinos logo displayed.", 
				objPojo.getWebActions().checkElementDisplayed(logoGC));
		if(objPojo.getWebActions().checkElementElementDisplayedWithMinWait(btnContinue))
			this.clickContinueCookie();
	}

	public void clickContinueCookie() {
		objPojo.getLogReporter().webLog("Click 'Continue' Cookie button", 
				objPojo.getWebActions().click(btnContinue));
	}

	public void clickLogin() {
		objPojo.getLogReporter().webLog("Click 'Login' button", 
				objPojo.getWebActions().click(btnOpenLogin));
	}

	public void verifyLoginPopupDisplayed() { 
		objPojo.getLogReporter().webLog("Verify 'Login' popup displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrLogin));
	}

	public void setUserName(String userName) {
		objPojo.getLogReporter().webLog("Set username ", userName,
				objPojo.getWebActions().setText(inpUserName, userName));
	}

	public void setPassword(String password) {
		objPojo.getLogReporter().webLog("Set password ", password,
				objPojo.getWebActions().setText(inpPassword, password));
	}
	
	public void setPIN(String PIN) {
		By locator = By.xpath("//input[@type='password' and @placeholder='PIN']");
		objPojo.getLogReporter().webLog("Activate PIN text box", 
				objPojo.getWebActions().click(locator));
	 	
		locator = By.xpath("//input[@type='password' and @placeholder='*' and @class='activated']");		
		objPojo.getLogReporter().webLog("Set PIN by single digit", PIN.substring(0,1),
				objPojo.getWebActions().setText(locator, PIN.substring(0,1)));
		
		locator = By.xpath("//input[@type='password' and @placeholder='*'][2]");		
		objPojo.getLogReporter().webLog("Set PIN by single digit", PIN.substring(1,2),
				objPojo.getWebActions().setText(locator, PIN.substring(1,2)));
		
		locator = By.xpath("//input[@type='password' and @placeholder='*'][3]");		
		objPojo.getLogReporter().webLog("Set PIN by single digit", PIN.substring(2,3),
				objPojo.getWebActions().setText(locator, PIN.substring(2,3)));
		
		locator = By.xpath("//input[@type='password' and @placeholder='*'][4]");		
		objPojo.getLogReporter().webLog("Set PIN by single digit", PIN.substring(3),
				objPojo.getWebActions().setText(locator, PIN.substring(3)));
 	}

	
	public void setCodeForMembershipCardLogin(String code) {
		By locator = By.xpath("//input[@placeholder='Enter code' and@type='password']");
		objPojo.getLogReporter().webLog("Set code", code,
				objPojo.getWebActions().setText(locator,code));
		objPojo.getWebActions().pressKeybordKeys(locator, "tab");
	}
	
	public void clickLoginOnLoginPopup() {
		objPojo.getLogReporter().webLog("Click 'Login' button", 
				objPojo.getWebActions().click(btnLogin));
	}
	
	public void clickVerifyOnLoginPopup() {
		objPojo.getLogReporter().webLog("Click 'Verify' button", 
				objPojo.getWebActions().click(btnVerify));
	}
	
	public void verifyCodeHasBeenSentToYouHeaderDisplayed() {
		By locator = By.xpath("//h3[contains(text(),'A code has been sent to you')]");
		objPojo.getLogReporter().webLog("Verify 'A code has been sent to you' header displayed. ", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	}

	public void clickJoinNow() {
		objPojo.getLogReporter().webLog("Click 'Join Now' button", 
				objPojo.getWebActions().click(btnJoinNow));
	}

	public void clickMyAccount() {
		objPojo.getLogReporter().webLog("Click 'My Account' link", 
				objPojo.getWebActions().clickByJavaScript(lnkMyAccount));
	}
	public void verifyUserLoggedIn() {
		
		objPojo.getLogReporter().mobileLog("User still Logged In  - Logout button Displayed", 
				objPojo.getMobileActions().checkElementDisplayed(lnkLogout));
		}

	public void verifyUserLoggedInSuccessfully(String userName) {
		
		By locator =  By.xpath("//div[@class='header']/div[@class='user']");
		objPojo.getLogReporter().webLog("Verify user logged in successfully.", userName, 
				objPojo.getWebActions().getText(locator).equalsIgnoreCase(userName));
	}

	public void clickCloseX()
	{
		By locator = By.xpath("//a[@class='close']");
		objPojo.getLogReporter().webLog("Close the my account ", 
				objPojo.getWebActions().click(locator));
	}
	
	public void verifyUserLoggedOutSuccessfuly()
	{
		objPojo.getLogReporter().webLog("User Logged Out Successfully - Join Now displayed", 
				objPojo.getWebActions().checkElementDisplayed(btnJoinNow));
		objPojo.getLogReporter().webLog("User Logged Out Successfully - Login button Displayed", 
				objPojo.getWebActions().checkElementDisplayed(btnOpenLogin));
	}
	
	public void clickForgottenUserNameLink() {
		objPojo.getLogReporter().webLog("Click 'Forgotten UserName' link", 
				objPojo.getWebActions().click(lnkForgottenUserName));
	}

	public void verifyUsernameReminderHeaderDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'Username Reminder' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrUsernameReminder));
	}

	public void setUsernameReminderEmailAddress(String emailAddress) {
		objPojo.getLogReporter().webLog("Set username remider email address ", emailAddress,
				objPojo.getWebActions().setText(inpUsernameReminderEmailAddress, emailAddress));
	}

	public void clickSendUsernameReminder() {
		objPojo.getLogReporter().webLog("Click 'Send username reminder' button", 
				objPojo.getWebActions().click(btnSendUsernameReminder));
	}

	public void verifyYouHaveMailHeaderDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'You have mail' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrYouHaveMail));
	}

	public void clickForgottenPasswordLink() {
		objPojo.getLogReporter().webLog("Click 'Forgotten Password' link", 
				objPojo.getWebActions().click(lnkForgottenPassword));
	}

	public void verifyResetPasswordHeaderDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'Reset Password' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrResetPassword));
	}

	public void setResetPasswordUserName(String userName) {
		objPojo.getLogReporter().webLog("Set reset password user name", userName,
				objPojo.getWebActions().setText(inpResetPasswordUserName, userName));
	}

	public void clickSubmitForResetPassword() {
		objPojo.getLogReporter().webLog("Click 'Submit' button for reset password", 
				objPojo.getWebActions().click(btnSubmitForResetPassword));
	} 

	public void clickSubmitForMembershipNumberReminder() {
		objPojo.getLogReporter().webLog("Click 'Submit' button for reset password", 
				objPojo.getWebActions().click(btnSubmitForMembershipNumberReminder));
	}

	public void verifyPasswordResetMailSend() {
		By locator = By.xpath("//p[contains(text(),'Your Password Reset instructions are waiting')]");
		objPojo.getLogReporter().webLog("Verify 'Your Password Reset instructions are waiting in your inbox' message displayed", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	}

	
	public void setNewPasswordForPasswordReset(String password) {
		objPojo.getLogReporter().webLog("Set new password for password reset process", password,
				objPojo.getWebActions().setText(inpNewPasswordForPasswordReset, password));
	}

	
	public void setConfirmPasswordForPasswordReset(String password) {
		objPojo.getLogReporter().webLog("Set confirm password for password reset process", password,
				objPojo.getWebActions().setText(inpConfirmPasswordForPasswordReset, password));
	}

	
	public void clickResetPasswordForPasswordReset() {
		objPojo.getLogReporter().webLog("Click 'Reset Password' button for password reset process", 
				objPojo.getWebActions().click(btnResetPasswordForPasswordReset));
	}

	
	public void verifyPasswordResetSuccessMessageDisplayed() {
		By locatorSuccess = By.xpath("//h1[text()='Success']");
		By locatorYourPasswordReset = By.xpath("//p[contains(text(),'Your password has been reset')]");
		objPojo.getLogReporter().webLog("Verify password reset success message displayed", 
				(objPojo.getWebActions().checkElementDisplayed(locatorSuccess) &&
						objPojo.getWebActions().checkElementDisplayed(locatorYourPasswordReset)));
	}

	public void clickForgottenMembershipNumberLink() {
		objPojo.getLogReporter().webLog("Click 'Forgotten Membership Number' link", 
				objPojo.getWebActions().click(lnkForgottenMembershipNumber));
	}

	public void verifyMembershipNumberReminderHeaderDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'Membership number reminder' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrMembershipNumberReminder));
	}

	public void clickForgottenPINLink() {
		objPojo.getLogReporter().webLog("Click 'Forgotten PIN' link", 
				objPojo.getWebActions().click(lnkForgottenPIN));
	}

	public void verifyResetPINHeaderDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'Reset PIN' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrResetPIN));
	}

	public void setResetPINEmailAddress(String emailAddress) {
		objPojo.getLogReporter().webLog("Set reset PIN email address", emailAddress,
				objPojo.getWebActions().setText(inpResetPINEmailAddress, emailAddress));
	}

	public void clickRequestANewPIN() {
		objPojo.getLogReporter().webLog("Click 'Request a new PIN' button", 
				objPojo.getWebActions().click(btnRequestANewPIN));
	}
	
	
	public void verifyResetPINForUseInGrosvenorCasinosHeaderDisplayed() {
		By locator = By.xpath("//h1[contains(text(),'Reset PIN for use in Grosvenor Casinos')]");
		objPojo.getLogReporter().webLog("Verify 'Reset PIN for use in Grosvenor Casinos' header displayed.", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	}
	
	
	public void setNewPINForPasswordPIN(String pin) {
		By locator = By.xpath("//input[@type='password' and @id='pin']");
		objPojo.getLogReporter().webLog("Set new PIN for PIN reset process", pin,
				objPojo.getWebActions().setText(locator, pin));
	}
	
	
	public void setConfirmPINForPasswordPIN(String pin) {
		By locator = By.xpath("//input[@type='password' and @id='confirmPin']");
		objPojo.getLogReporter().webLog("Set confirm PIN for PIN reset process", pin,
				objPojo.getWebActions().setText(locator, pin));
	}
	
	
	public void clickSubmitForPINReset() {
		By locator = By.xpath("//button[@type='submit' and text()='Submit']");
		objPojo.getLogReporter().webLog("Click 'Submit' button for PIN reset process", 
				objPojo.getWebActions().click(locator));
	}
	
	
	public void verifyPINResetSuccessMessageDisplayed() {
		By locatorSuccess = By.xpath("//h1[text()='Success']");
		By locatorYourPasswordReset = By.xpath("//p[contains(text(),'Your in-club PIN is now ready to be used')]");
		objPojo.getLogReporter().webLog("Verify 'Your in-club PIN is now ready to be used' message displayed", 
				(objPojo.getWebActions().checkElementDisplayed(locatorSuccess) &&
						objPojo.getWebActions().checkElementDisplayed(locatorYourPasswordReset)));
	}
	
	   // Header Navigation Tabs
	
		public void verifylivecasinoTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify live casino Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrlivecasinoTab));
			
		}
		public void verifySlotsandGamesTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Slots and Games Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrSlotsandGamesTab));
			
		}
		
		public void verifyJackpotsTabTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Jackpots Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrJackpotsTab));
			
		}
		public void verifyTableGamesTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Table Games Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrTableGamesTab));
			
		}
		public void verifySportTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Sport Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrSportTab));
			
		}
		public void verifyPokerTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Poker Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrPokerTab));
			
		}
		public void verifyPromosTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Promos Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrPromosTab));
			
		}
		public void verifyGrosvenorOneTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Grosvenor One Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrGrosvenorOneTab));
			
		}
		public void verifyLocalCasinosTabDisplayed() {
			objPojo.getLogReporter().webLog("Verify Local Casinos Tab displayed", 
					objPojo.getWebActions().checkElementDisplayed(hdrLocalCasinosTab));
			
		}
		
		// Header Navigation Tabs
		
		
		public void clicklivecasinoTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/live-casino";
			objPojo.getLogReporter().webLog("Click Live Casino Tab from header button", 
					objPojo.getWebActions().click(hdrlivecasinoTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
		
		public void clickSlotsandGamesTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/slots-and-games";
			objPojo.getLogReporter().webLog("Click Slots and Games Tab from header button", 
					objPojo.getWebActions().click(hdrSlotsandGamesTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
			
		public void clickJackpotsTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/jackpots";
			objPojo.getLogReporter().webLog("Click Jackpot Tab from header button", 
					objPojo.getWebActions().click(hdrJackpotsTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
				
		public void clickTableGamesTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/table-games";
			objPojo.getLogReporter().webLog("Click Table Games Tab from header button", 
					objPojo.getWebActions().click(hdrTableGamesTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
		
		public void clickSportTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/sport#home";
			objPojo.getLogReporter().webLog("Click Sport Tab from header button", 
					objPojo.getWebActions().click(hdrSportTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
		
		public void clickPokerTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/poker";
			objPojo.getLogReporter().webLog("Click Poker Tab from header button", 
					objPojo.getWebActions().click(hdrPokerTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
		public void clickPromosTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/promotions";
			objPojo.getLogReporter().webLog("Click Promos Tab from header button", 
					objPojo.getWebActions().click(hdrPromosTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
		
		public void clickGrosvenorOneTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/grosvenor-one";
			objPojo.getLogReporter().webLog("Click Grosvenor One Tab from header button", 
					objPojo.getWebActions().click(hdrGrosvenorOneTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
		
		
		public void clickLocalCasinosTab() {
			String url = "https://np03-gros-cms.rankgrouptech.net/local-casinos";
			objPojo.getLogReporter().webLog("Click Local Casinos Tab from header button", 
					objPojo.getWebActions().click(hdrLocalCasinosTab));
			String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
			objPojo.getLogReporter().webLog("Redirect to the correct url ", currentURL.equalsIgnoreCase(url));
		}
		
		
		
		
}