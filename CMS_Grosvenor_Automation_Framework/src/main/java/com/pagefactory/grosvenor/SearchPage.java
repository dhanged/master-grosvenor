package com.pagefactory.grosvenor;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class SearchPage {

	// Local variables
	private Pojo objPojo;

	private String validSearchCharacters = "jac";

	private By playbutton = By.xpath("//button[@class='open-login load-game column-4-middle column-4-large'][contains(text(),'Play')]");
	private By hdrLogin = By.xpath("//button[@class='secondary join-now' and text()='Sign Up']");
	
	
	private By btnSearch = By.xpath("//button[@class='search__open-btn']");

	private By inpSearchBox = By.xpath("//input[@type='search']");
	private By searchResults = By.xpath("//li[1][@class='search-results__category']");
	private By searchResultsGameName = By.xpath("//li[1]/ol/li[1]/div[@class='search-results__title heading']/span/strong[contains(text(),'"+validSearchCharacters+"')]");



	private By searchResultsGameImage = By.xpath("//li[1]/ol/li[1]/span[@class='search-results__thumbnail']");
	private By searchResultsGamePlaybtn = By.xpath("//li[1]/ol/li[1]/button[@class='search-results__play-btn btn']");
	private By searchResultsErrorMessage = By.xpath("//p[@class='search-no-results__message dialog-box']");
	private By searchResultsInfoIcon = By.xpath("//li[1]/ol/li[1]/a/i[@class='search-results__info-icon']");

	private By btnBrowseSections = By.xpath("//button[@class='search-no-results__browse-btn']");
	private By searchSectionOptionAllSlots = By.xpath("//a[@class='sitemap__sub-section-title'][contains(text(),'All Slots')]");
	private By searchSectionOptionJackpots = By.xpath("//a[@class='sitemap__sub-section-title'][contains(text(),'Jackpots')]");
	private By searchSectionOptionTopSlots = By.xpath("//a[@class='sitemap__sub-section-title'][contains(text(),'Top Slots')]");
	private By searchSectionOptionNewSlots = By.xpath("//a[@class='sitemap__sub-section-title'][contains(text(),'New Slots')]");


	//Constructor
	public SearchPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifySearchButtonDisplayed() {
		objPojo.getLogReporter().webLog("Verify Search button displayed", 
				objPojo.getWebActions().checkElementDisplayed(btnSearch));
	}	

	public void clickSearchButton() {
		objPojo.getLogReporter().webLog("Click on Serach button", 
				objPojo.getWebActions().click(btnSearch));

	}

	public void searchGame(String gamename) {
		objPojo.getLogReporter().webLog("Set valid search characters ", gamename,
				objPojo.getWebActions().setText(inpSearchBox, gamename));
	}

	public void setTextForInvalidSearchResults() {
		objPojo.getLogReporter().webLog("Set invalid search characters ", "fgh",
				objPojo.getWebActions().setText(inpSearchBox, "fgh"));
	}

	public void verifySearchResultsDisplayed() {
		objPojo.getLogReporter().webLog("Verify Search button displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchResults));
	}

	public void verifySearchResultsNotDisplayed() {
		objPojo.getLogReporter().webLog("Verify Search Results not displaying", 
				objPojo.getWebActions().checkElementNotDisplayed(searchResults));
	}

	public void verifyInfoIconInSearchResult()
	{
		objPojo.getLogReporter().webLog("Verify in Search Results info icon displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchResultsInfoIcon));
	}

	public void clickOnInfoIconInSearchResult()
	{
		objPojo.getLogReporter().webLog(" click on info icon ", 
				objPojo.getWebActions().click(searchResultsInfoIcon));
	}
	public void verifySearchResultsOptionsDisplayed() {
		objPojo.getLogReporter().webLog("Verify in Search Results Game Name displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchResultsGameName));
		objPojo.getLogReporter().webLog("Verify in Search Results Game Image displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchResultsGameImage));
		verifyInfoIconInSearchResult();
		objPojo.getLogReporter().webLog("Verify in Search Results Play button displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchResultsGamePlaybtn));
	}

	public void verifyErrorMessageDisplayed() {
		objPojo.getLogReporter().webLog("Verify Error message displaying ", 
				objPojo.getWebActions().checkElementDisplayed(searchResultsErrorMessage));
	}

	public void verifyBrowseSectionButtonDisplayed() {
		objPojo.getLogReporter().webLog("Verify Browse sections button displayed", 
				objPojo.getWebActions().checkElementDisplayed(btnBrowseSections));
	}

	public void clickBrowseSectionButton() {
		objPojo.getLogReporter().webLog("Click on browse sections button", 
				objPojo.getWebActions().click(btnBrowseSections));

	}

	public void verifySearchSectionOptionsDisplayed() {
		objPojo.getLogReporter().webLog("Verify All Slots option displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchSectionOptionAllSlots));
		objPojo.getLogReporter().webLog("Verify Jackpots option displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchSectionOptionJackpots));
		objPojo.getLogReporter().webLog("Verify Top slots option displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchSectionOptionTopSlots));
		objPojo.getLogReporter().webLog("Verify New Slots option displayed", 
				objPojo.getWebActions().checkElementDisplayed(searchSectionOptionNewSlots));
	}

	public void clickSearchSectionOptions() {
		objPojo.getLogReporter().webLog("Click on All Slots option", 
				objPojo.getWebActions().click(searchSectionOptionAllSlots));

	}

	public void clickOnButton(String btnName) 
	{
		By btn = By.xpath("//button[text()='"+btnName+"']");
		objPojo.getLogReporter().webLog("Click on "+btnName, 
				objPojo.getWebActions().click(btn));
	}
	public void verifyButton(String btnName) 
	{
		By btn = By.xpath("//button[text()='"+btnName+"']");
		objPojo.getLogReporter().webLog("verify  "+btnName, 
				objPojo.getWebActions().checkElementDisplayed(btn));
	}
	
	
	public void clickPlayButton() {
		objPojo.getLogReporter().webLog("Click on Play button", 
				objPojo.getWebActions().click(playbutton));

	}

	public void verifyPlayButtonDisplayed() {
		objPojo.getLogReporter().webLog("Verify Play button displayed", 
				objPojo.getWebActions().checkElementDisplayed(playbutton));

	}
	
	
	public void verifyLoginPopupDisplayed() {
		objPojo.getLogReporter().webLog("Verify 'Login' popup displayed", 
				objPojo.getWebActions().checkElementDisplayed(hdrLogin));

	}


	
	
}
