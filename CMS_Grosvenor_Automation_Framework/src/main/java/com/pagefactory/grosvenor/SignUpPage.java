package com.pagefactory.grosvenor;

import org.openqa.selenium.By;

import com.generic.Pojo;
public class SignUpPage {

	// Local variables
	private Pojo objPojo;

	private By inpEmail = By.xpath("//input[@type='email' and @placeholder='Email address']");
	private By inpUserName = By.id("joinnow-username");
	private By inpPassword = By.id("joinnow-password");
	private By inpFirstName = By.id("joinnow-firstname");
	private By inpSurName = By.id("joinnow-surname");
	private By inpMobileNumebr = By.id("joinnow-mobile");
	
	private By btnNext = By.xpath("//button[@type='submit' and text()='Next']");
	private By btnYesNewUser = By.xpath("//p[text()='New to Grosvenor casinos?']/following-sibling::p/button[text()='Yes']");
	private By btnDepositLimitYes = By.xpath("//legend[text()='Would you like to set a deposit limit now?']/parent::div/following-sibling::div/button[contains(text(),'Yes')]"); 
	private By btnDepositLimitNo = By.xpath("//legend[text()='Would you like to set a deposit limit now?']/parent::div/following-sibling::div/button[contains(text(),'No')]");
	private By btnRegister = By.xpath("//button[@type='submit' and contains(text(),'Register')]");
	private By btnSignUpClose = By.xpath("//div[@class='header']/header/a[@class='close' and contains(text(),'Close')]");
	
	private By chkJoinnowAgree = By.xpath("//input[@type='checkbox' and @name='joinnow-agree']");
	private By hdrSignUp = By.xpath("//h1[text()='SIGN UP']");
	private By drpCountry = By.xpath("//select[@id='joinnow-country']");
	
	public SignUpPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void selectNewUserYes() {
		objPojo.getLogReporter().webLog("Click 'Yes' new user", 
				objPojo.getWebActions().click(btnYesNewUser));
	}

	public void verifySignUpHeaderDisplayed(){
		objPojo.getLogReporter().webLog("Verify 'Sign Up' header displayed.", 
				objPojo.getWebActions().checkElementDisplayed(hdrSignUp));
	}

	public void setEmailAddress(String email){
		objPojo.getLogReporter().webLog("Set Email Address", email, 
				objPojo.getWebActions().setText(inpEmail, email));
		objPojo.getWebActions().pressKeybordKeys(inpEmail, "tab");
	}

	public void selectJoinnowAgreeCheckbox() {
		objPojo.getLogReporter().webLog("Select Joinnow Agree Checkbox",  
				objPojo.getWebActions().selectCheckbox(chkJoinnowAgree, true));
	}

	public void clickNext() {
		objPojo.getLogReporter().webLog("Click 'Next' button", 
				objPojo.getWebActions().click(btnNext));
	}	

	public void setUserName(String userName){
		objPojo.getLogReporter().webLog("Set user name", userName, 
				objPojo.getWebActions().setText(inpUserName, userName));
	}

	public void setPassword(String password){
		objPojo.getLogReporter().webLog("Set password", password, 
				objPojo.getWebActions().setText(inpPassword, password));
	}

	public void selectTitle(String title) {
		By locator = null;
		switch(title.toLowerCase()){
		case "mr":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Mr']");
			break;
		case "miss":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Miss']");
			break;
		case "mrs":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Mrs']");
			break;
		case "ms":
			locator = By.xpath("//fieldset[@class='form-checkbox-buttons']//button[text()='Ms']");
			break;
		}
		objPojo.getLogReporter().webLog("Select title", title, 
				objPojo.getWebActions().click(locator));
	}

	public void setFirstName(String firstName){
		objPojo.getLogReporter().webLog("Set first name", firstName, 
				objPojo.getWebActions().setText(inpFirstName, firstName));
	}

	public void setSurName(String surName){
		objPojo.getLogReporter().webLog("Set sur name", surName, 
				objPojo.getWebActions().setText(inpSurName, surName));
	}

	public void selectDateOfBirth(String dateOfBirth){
		String[] arrDateOfBirth = dateOfBirth.split("_");
		String dobDay = arrDateOfBirth[0];
		String dobMonth = arrDateOfBirth[1];
		String dobYear = arrDateOfBirth[2];
		By locator = By.xpath("//input[@placeholder='Date of birth']");

		objPojo.getLogReporter().webLog("Activate DOB fileds",   
				objPojo.getWebActions().click(locator));

		locator = By.id("dob-day");
		objPojo.getLogReporter().webLog("Set dob day", dobDay, 
				objPojo.getWebActions().setText(locator, dobDay));

		locator = By.id("dob-month");
		objPojo.getLogReporter().webLog("Set dob month", dobMonth, 
				objPojo.getWebActions().setText(locator, dobMonth));

		locator = By.id("dob-year");
		objPojo.getLogReporter().webLog("Set dob year", dobYear, 
				objPojo.getWebActions().setText(locator, dobYear));

	}

	public void selectCountry(String country) {
		objPojo.getLogReporter().webLog("Select country", country, 
				objPojo.getWebActions().selectFromDropDown(drpCountry, country));
	}

	public void setmanualAddress(String addressLine1, String addressLine2, 
			String townCity, String country, String postCode) 
	{
		By lnkForManualAddress = By.xpath("//a[contains(text(),'Enter Address manually')]");

		objPojo.getLogReporter().webLog("Select Enter Address manually",   
				objPojo.getWebActions().click(lnkForManualAddress));


		By locator = By.id("joinnow-addressLine1");
		if(!objPojo.getWebActions().checkElementElementDisplayedWithMidWait(locator))
			objPojo.getWebActions().click(lnkForManualAddress);

		objPojo.getLogReporter().webLog("Set address line1", addressLine1, 
				objPojo.getWebActions().setText(locator, addressLine1));

		locator = By.id("joinnow-addressLine2");
		objPojo.getLogReporter().webLog("Set address line2", addressLine2, 
				objPojo.getWebActions().setText(locator, addressLine2));

		locator = By.id("joinnow-townCity");
		objPojo.getLogReporter().webLog("Set Town/City", townCity, 
				objPojo.getWebActions().setText(locator, townCity));

		locator = By.id("joinnow-county");
		objPojo.getLogReporter().webLog("Set country", country, 
				objPojo.getWebActions().setText(locator, country));

		locator = By.id("joinnow-postcode");
		objPojo.getLogReporter().webLog("Set post code", postCode, 
				objPojo.getWebActions().setText(locator, postCode));
	}

	public void setMobileNumber(String mobileNumber) {
		objPojo.getLogReporter().webLog("Set mobile number", mobileNumber, 
				objPojo.getWebActions().setText(inpMobileNumebr, mobileNumber));
	}

	
	public void setMarketingPreferences(String marketingPreferences) {
		if(marketingPreferences.contains("~")) 
		{
			String[] arrMarketingPreferences = marketingPreferences.split("~");
			for(String marketingPreference : arrMarketingPreferences) {
				By locator = By.xpath("//p[contains(text(),'Marketing preferences')]/parent::fieldset/following-sibling::fieldset[1]//input[@type='checkbox' and @id='" + marketingPreference + "']");
				objPojo.getLogReporter().webLog("Select marketing preference", marketingPreference,  
						objPojo.getWebActions().click(locator));
			}
		}
		else {
			By locator = By.xpath("//p[contains(text(),'Marketing preferences')]/parent::fieldset/following-sibling::fieldset[1]//input[@type='checkbox' and @id='" + marketingPreferences + "']");
			objPojo.getLogReporter().webLog("Select marketing preference", marketingPreferences,  
					objPojo.getWebActions().click(locator));
		}
	}

	public void selectDepositLimitYes() {
		objPojo.getLogReporter().webLog("Select deposit limit now 'Yes'",   
				objPojo.getWebActions().clickByJavaScript(btnDepositLimitYes));
	}

	public void selectDepositLimitNo() {
		objPojo.getLogReporter().webLog("Select deposit limit now 'No'",   
				objPojo.getWebActions().click(btnDepositLimitNo));
	}

	public void clickRegister() {
		objPojo.getLogReporter().webLog("Click register",   
				objPojo.getWebActions().clickByJavaScript(btnRegister));
	}

	public void clickCloseSignUp() {
		if(objPojo.getWebActions().checkElementDisplayed(btnSignUpClose)) {
			objPojo.getLogReporter().webLog("Close Sign Up Popup",   
					objPojo.getWebActions().click(btnSignUpClose));
		}
	}
	
	public void setDepositLimit(String lmt,String value)
	{
		
			By lblDepositLmt = By.xpath("//fieldset[@id='joinnow-depositLimit']//following-sibling::div//fieldset//div//button[text()='"+lmt+"']");

		objPojo.getLogReporter().webLog("Clik on "+lmt,   
				objPojo.getWebActions().click(lblDepositLmt));

		objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
		By inputDepositLimit = By.xpath("//input[@id='joinnow-depositLimit']");
		objPojo.getLogReporter().webLog("Set" +lmt + " as " , value,
				objPojo.getWebActions().setTextWithClear(inputDepositLimit, value));
	}

	public void setDepositLimitafterreset(String lmt,String value)
	{
		
		By lnkResetLimit = By.xpath("//a[@class='input-inner-link'][contains(.,'Reset Limit')]");
		objPojo.getLogReporter().webLog("Click on 'Reset Limit''",
				objPojo.getWebActions().click(lnkResetLimit));
	
		
		By lblDepositLmt = By.xpath("//fieldset[@id='joinnow-depositLimit']//following-sibling::div//fieldset//div//button[text()='"+lmt+"']");

		objPojo.getLogReporter().webLog("Clik on "+lmt,   
				objPojo.getWebActions().click(lblDepositLmt));

		objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
		By inputDepositLimit = By.xpath("//input[@id='joinnow-depositLimit']");
		objPojo.getLogReporter().webLog("Set" +lmt + " as " , value,
				objPojo.getWebActions().setTextWithClear(inputDepositLimit, value));
	}

	
	
	
	
	public void verifyErrorMessage(String err)
	{
		if(err.contains("~")){
			String[] arr1 = err.split("~");
			for (String links : arr1  ) {
				By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+links+"']");
				objPojo.getLogReporter().webLog(links+" displayed  ",
						objPojo.getWebActions().checkElementDisplayed(errMsg));}}
		else{
			By errMsg = By.xpath("//ul[@class='errors']//li[text()='"+err+"']");
			objPojo.getLogReporter().webLog(err+" displayed  ",
					objPojo.getWebActions().checkElementDisplayed(errMsg));}

	}
	public void verifyDepositNowHeader()
	{
		objPojo.getWaitMethods().sleep(objPojo.getConfiguration().getConfigIntegerValue("midwait"));
		By lblDepositNow = By.xpath("//p[@class='heading'][text()='Deposit now']");
		objPojo.getLogReporter().webLog(" verify 'Deposit now'  " ,
				objPojo.getWebActions().checkElementDisplayed(lblDepositNow));
	}

	
}