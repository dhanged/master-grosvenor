package com.pagefactory.grosvenor;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class PageValidation {
	private Pojo objPojo;


	public PageValidation(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifyPrimaryTabs(String tabName)
	{	if(tabName.contains(",")){
		String[] arr1 = tabName.split(",");
		for (String links : arr1  ) {
			By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+links+"')]");
			objPojo.getLogReporter().webLog("verify '"+links+"' link is displayed in Primary navigation bar ",
					objPojo.getWebActions().checkElementDisplayed(lnkPrimaryTAB));}}
	else{
		By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+tabName+"')]");
		objPojo.getLogReporter().webLog("verify '"+tabName+"' link is displayed in Primary navigation bar ",
				objPojo.getWebActions().checkElementDisplayed(lnkPrimaryTAB));
	}
	}
	public void verifyCarouselDisplayedOnPage()
	{
		By  carouselblock = By.xpath("//div[@class='component promo-carousel']//div[@class='block carouselblock ']");
		objPojo.getLogReporter().webLog("Verify promo-carousel ",
				objPojo.getWebActions().checkElementDisplayed(carouselblock));
	}
	public void navigateToPrimaryTabs(String tabName)
	{
		if(tabName.contains(",")){
			String[] arr1 = tabName.split(",");
			for (String links : arr1  ) {
				By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+links+"')]");
				objPojo.getLogReporter().webLog(links+" displayed in Primary navigation bar ",
						objPojo.getWebActions().click(lnkPrimaryTAB));}}
		else{
			By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(.,'"+tabName+"')]");
			objPojo.getLogReporter().webLog(tabName+" displayed in Primary navigation bar ",
					objPojo.getWebActions().click(lnkPrimaryTAB));}
	}
	

	public void verifyPrimayTabDisplayedAsSelectedOrNot(String tabName)
	{
		By lnkPrimaryTAB = By.xpath("//nav[@class='component top-navigation']//ul//li//a[contains(@class,'active')][contains(.,'"+tabName+"')]");
		objPojo.getLogReporter().webLog(tabName+" displayed as seleced in Primary navigation bar ",
				objPojo.getWebActions().checkElementDisplayed(lnkPrimaryTAB));
	}
	
	public void verifySecondaryTabDisplayedAsSelectedOrNot(String tabName)
	{
		By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(@class,'active')][contains(text(),'"+tabName+"')]");
		objPojo.getLogReporter().webLog(tabName+" displayed as seleced in secondary navigation bar ",
				objPojo.getWebActions().checkElementDisplayed(locator));
	}
	
	public void verifySecondaryTABdisplayed(String links){
		if(links.contains(","))
		{
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				objPojo.getLogReporter().webLog(" verify "+ links2+" link displayed from secondary navigation",
						objPojo.getWebActions().checkElementDisplayed(locator));}
		}
		else
		{
			By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			objPojo.getLogReporter().webLog(" verify "+ links+" link displayed from secondary navigation",
					objPojo.getWebActions().checkElementDisplayed(locator));
		}
	}

	public void clickSecondaryTABdisplayed(String links)
	{
		if(links.contains(",")){
			String[] arr1 = links.split(",");
			for (String links2 : arr1  ) {
				By locator = By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links2+"')]");
				objPojo.getLogReporter().webLog(" Click on "+ links2+" link displayed from secondary navigation",
						objPojo.getWebActions().click(locator));}
		}
		else
		{
			By locator2= By.xpath("//nav[@class='component sub-navigation']//div[@class='sub-navigation-wrapper']//p//a[contains(text(),'"+links+"')]");
			objPojo.getLogReporter().webLog(" Click on "+ links+" link displayed from secondary navigation",
					objPojo.getWebActions().click(locator2));
		}
	}
	public void verifySectionLinks(String lnks)
	{
		String[] arrSingleEntry = lnks.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(":");
			By locator = By.xpath("//h2[contains(text(),'"+entry[0]+"')]//following::a[contains(.,'"+entry[1]+"')]");
			objPojo.getLogReporter().webLog(" Verify '"+entry[1]+"' lnk is displayed againest '"+entry[0]+"' section ",
					objPojo.getWebActions().checkElementDisplayed(locator));	}
	}

	public void verifyArticleBlockHeaders(String hdrs)
	{
		String[] arrSingleEntry = hdrs.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(";");
			By locator = By.xpath("//"+entry[0]+"[contains(.,'"+entry[1]+"')]");	
			objPojo.getLogReporter().webLog(" Verify '"+entry[1]+"' ",
					objPojo.getWebActions().checkElementDisplayed(locator));	}
	}

	public void verifyExpandLinks(String lnk)
	{
		By locator = By.xpath("//h3//a[text()='"+lnk+"']");	
		objPojo.getLogReporter().webLog(" Verify '"+lnk,
				objPojo.getWebActions().checkElementDisplayed(locator));			
	}
	public void clickExpandLinks(String lnk)
	{
		By locator = By.xpath("//h3//a[text()='"+lnk+"']");	
		objPojo.getLogReporter().webLog(" Verify '"+lnk,
				objPojo.getWebActions().click(locator));			
	}
	public void verifyGamesSection(String classHeader)
	{
		String[] arrSingleEntry = classHeader.split("~"); //
		for(String singleEntry : arrSingleEntry) {
			String[] entry = singleEntry.split(":");
			By locator = By.xpath("//h2[@class='"+entry[0]+"'and contains(text(),'"+entry[1]+"')]//following::div[@class='mobile-game-panels']");
			objPojo.getLogReporter().webLog(" Verify 'Games' is displayed under '"+entry[1]+"' section ",
					objPojo.getWebActions().checkElementDisplayed(locator));}
	}
	public void verifyHomepageSectionHeadersdisplayed(String classHeader)
	{

		String[] arrSingleEntry = classHeader.split("~"); //
		for(String singleEntry : arrSingleEntry) 
		{
			String[] entry = singleEntry.split(":");
			By locator = By.xpath("//h2[@class='"+entry[0]+"'and contains(text(),'"+entry[1]+"')]");

			objPojo.getLogReporter().webLog(" Verify "+entry[1]+" section is displayed with "+entry[0]+" icon",
					objPojo.getWebActions().checkElementDisplayed(locator));

		}	
	}
	public void verifySortByText()
	{
		By lblSortBy = By.xpath("//div[@class='show-game-panels']//div[@class='ordering-bar']//span[contains(.,'Sort By')]");
		objPojo.getLogReporter().webLog(" Verify 'Sort By' text ",
				objPojo.getWebActions().checkElementDisplayed(lblSortBy));	}

	public void verifyGamesOnSlotsPage()
	{
		By lblgamesPanel = By.xpath("//div[@class='game-panels container-fluid']//div[@class='row']//div[contains(@class,'game-panel')]");
		objPojo.getLogReporter().webLog(" Verify games panel ",
				objPojo.getWebActions().checkElementDisplayed(lblgamesPanel));	
	}

	
	public void verifyGamesOnGameshowsPage()
	{
		By lblgamesPanelgamesshows = By.xpath("//div[@class='game-panels game-panels-container']");
		objPojo.getLogReporter().webLog(" Verify games panel ",
				objPojo.getWebActions().checkElementDisplayed(lblgamesPanelgamesshows));	
	}

	
	public void verifySectionsOnG1(String sectNm)
	{
		if(sectNm.contains(",")){
			String[] arr1 = sectNm.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+links+"')]");
				objPojo.getLogReporter().webLog(" verify  "+ links+" displayed on G1 page",
						objPojo.getWebActions().checkElementDisplayed(locator));}
		}
		else{
			By locator2 = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+sectNm+"')]");
			objPojo.getLogReporter().webLog(" verify  "+ sectNm+" displayed on G1 page",
					objPojo.getWebActions().checkElementDisplayed(locator2));}
	}



	public void clickOnSectionsOnG1(String sectNm)
	{
		if(sectNm.contains(",")){
			String[] arr1 = sectNm.split(",");
			for (String links : arr1  ) {
				By locator = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+links+"')]");
				objPojo.getLogReporter().webLog(" Click  on "+ links+" ",objPojo.getWebActions().click(locator));}
		}
		else
		{
			By locator2 = By.xpath("//div[@class='article-block__detailed-info-block']//div//a[contains(.,'"+sectNm+"')]");
			objPojo.getLogReporter().webLog(" Click  on "+ sectNm+" ",
					objPojo.getWebActions().click(locator2));
		}

	}

	public void verifyGrosvenorOneCasinoscardiMG()
	{
		By imgGrosvenorOneCasinoscard = By.xpath("//img[@alt='Grosvenor One Casinos card']");
		objPojo.getLogReporter().webLog(" Verify 'Grosvenor One Casinos card' image on G1 page ",
				objPojo.getWebActions().checkElementDisplayed(imgGrosvenorOneCasinoscard));	
	}

	public void verifySerachFinderInHeader()
	{
		By btnSearch = By.xpath("//div[@class='component search']//button[@class='search__open-btn']");
		objPojo.getLogReporter().webLog(" verify game Serach Finder in Header ",
				objPojo.getWebActions().checkElementDisplayed(btnSearch));	
	}
	public void verifyEnterBonusCodeFieldOnPromotionPage()
	{
		By inpEnterBonusCode = By.xpath("//div[@class='bonus-link']//a[contains(.,'Enter bonus code')]");
		objPojo.getLogReporter().webLog(" Verify 'Enter bonus code' field on Promotion Page ",
				objPojo.getWebActions().checkElementDisplayed(inpEnterBonusCode));	
	}

	public void verifyBonusHistoryLinkOnPromotionPage()
	{
		By lnkBonusHistory = By.xpath("//div[@class='bonus-link']//a[contains(.,'Enter bonus code')]//following-sibling::a[text()='Bonus History']");
		objPojo.getLogReporter().webLog(" Verify 'Bonus History' link on Promotion Page ",
				objPojo.getWebActions().checkElementDisplayed(lnkBonusHistory));	
	}

	public void verifyPromoetionsONPromosPage()
	{
		By lstPromos = By.xpath("//div[@class='component latest-promo promo-opt']//div[@class='row']//div[@class='promotions']");
		objPojo.getLogReporter().webLog(" Verify 'promotions'  on Promos Page ",
				objPojo.getWebActions().checkElementDisplayed(lstPromos));	
	}

	public void verifyGamesOnCasinoPage()
	{
		By lstGames = By.xpath("//div[@class='block gameslistblock ']//div[@class='row']//div[contains(@class,'game-panel')]");
		objPojo.getLogReporter().webLog(" Verify 'games'  on Casino Page ",
				objPojo.getWebActions().checkElementDisplayed(lstGames));	

	}

	public void verifyEntertownorpostcodefield()
	{
		By inpEntertownpostcode = By.xpath("//input[@placeholder='Enter town or postcode']");
		objPojo.getLogReporter().webLog(" Verify 'Enter town or postcode' inputbox on casino Page ",
				objPojo.getWebActions().checkElementDisplayed(inpEntertownpostcode));	
	}
	
	public void verifyViewAllCasionsLink()
	{
		By lnkViewallcasinos = By.xpath("//div[@id='component_search-field']//..//h2[@class='location'][contains(text(),'Find')]//following-sibling::a[text()='View all casinos']");
		objPojo.getLogReporter().webLog(" Verify 'View all casinos' link on casino Page ",
				objPojo.getWebActions().checkElementDisplayed(lnkViewallcasinos));	
	}
	public void verifycasinoFinderClearButton()
	{
		By btnClear = By.xpath("//input[@placeholder='Enter town or postcode']//following-sibling::button[@class='clear']");
		objPojo.getLogReporter().webLog(" Verify 'clear' button ",
				objPojo.getWebActions().checkElementDisplayed(btnClear));	
	}
	public void verifyfindnearestButton()
	{
		By btnfindnearest = By.xpath("//input[@placeholder=\"Enter town or postcode\"]//following-sibling::button[text()='Or find nearest ']");
		objPojo.getLogReporter().webLog(" Verify 'find nearest' button ",
				objPojo.getWebActions().checkElementDisplayed(btnfindnearest));	
	}
	public void verifyMapIcon()
	{
		By lnkMap = By.xpath("//div[@class='retail-list__block']//div[@class='retail-list__contact-wrapper']//a[contains(@href,'http://maps.google.com/maps')]");
		objPojo.getLogReporter().webLog(" Verify 'MapIcon'  ",
				objPojo.getWebActions().checkElementDisplayed(lnkMap));	
	}
	public void verifyTelephoneIocn()
	{
		By lnkTelephoneNumber = By.xpath("//div[@class='retail-list__block']//div[@class='retail-list__contact-wrapper']//a[@class='telephone retail-list__contact']");
		objPojo.getLogReporter().webLog(" Verify 'TelephoneIocn; ",
				objPojo.getWebActions().checkElementDisplayed(lnkTelephoneNumber));	
	}
	
	public void verifyMoreInfoIocn()
	{
		By imgInfo = By.xpath("//div[@class='retail-list__block']//div[@class='retail-list__contact-wrapper']//a[@class='telephone retail-list__contact']//following-sibling::span//a//img[@alt='More Info']");
		objPojo.getLogReporter().webLog(" Verify 'info Iocn' ",
				objPojo.getWebActions().checkElementDisplayed(imgInfo));	
	}
		
}
