package com.pagefactory.grosvenor;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.joda.time.DateTime;
import org.openqa.selenium.By;

import com.generic.Pojo;
import com.generic.utils.GenericUtils;

public class MyAccount_BonusPage {
	
	private Pojo objPojo;
	
	public MyAccount_BonusPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifyEnterBonusCodeTXTDisplayed()
	{
		By EnterBonusCodeTXT= By.xpath("//p[contains(text(),'Enter bonus code')]");
		objPojo.getLogReporter().webLog("Verify 'Enter bonus Code' Text/label displayed", 
				objPojo.getWebActions().checkElementDisplayed(EnterBonusCodeTXT));
	}
	
	public void verifyEnterPromotionalCodeHDrDisplayed()
	{
		By EnterPromotionalCodeHDr= By.xpath("//p[contains(text(),'Enter promotional code')]");
		objPojo.getLogReporter().webLog("Verify 'Enter Promotional Code' header displayed", 
				objPojo.getWebActions().checkElementDisplayed(EnterPromotionalCodeHDr));
	}
	
	public void setPromoCode(String promocode)
	{
		By inputPromoCode = By.xpath("//input[@id='enter-code']");
		
		objPojo.getLogReporter().webLog(" Set Promocode/Bonus code ", promocode,
				objPojo.getWebActions().setText(inputPromoCode, promocode));
		
	}
	
	public void verifyClearBTN()
	{
		By ClearBtn = By.xpath("//button[contains(text(),'Clear')]");
		
		objPojo.getLogReporter().webLog("Verify 'Clear' button displayed", 
				objPojo.getWebActions().checkElementDisplayed(ClearBtn));
	}
	
	public void clickSubmitcode()
	{
		By submitCodeBtn= By.xpath("//button[contains(text(),'Submit')]");
		
		objPojo.getLogReporter().webLog(" Click on Submit ", 
				objPojo.getWebActions().click(submitCodeBtn));
		
		By errorMsg = By.xpath("//li[contains(text(),'Incorrect Bonus Code.')]");
		
		if (objPojo.getWebActions().checkElementDisplayed(errorMsg))
		{
			objPojo.getLogReporter().webLog(" 'Incorrect Bonus Code.' error message is displayed", 
					objPojo.getWebActions().checkElementDisplayed(errorMsg));
		}
	}
		
	public void verifyIncorrectCodeMSG()
	{
		By errorMsg = By.xpath("//li[contains(text(),'Incorrect Bonus Code.')]");
		objPojo.getLogReporter().webLog("Verify 'Incorrect Bonus Code.' error message is displayed", 
				objPojo.getWebActions().checkElementDisplayed(errorMsg));
	}
	
	public String getBonusName()
	{
		By bonusname = By.xpath("//p[@class='heading' and contains(text(),'Enter promotional code')]/following::p[@class='heading']");
		
		return objPojo.getWebActions().getText(bonusname);
	}
	public void verifyTermsnConditionsLinkDisplayed()
	{
		By TermsnConditionsLink = By.xpath("//a[@class='help-link' and contains(text(),'View Terms and Conditions')]");
		
		objPojo.getLogReporter().webLog("Verify 'Terms n Conditions' Link displayed", 
				objPojo.getWebActions().checkElementDisplayed(TermsnConditionsLink));
	}
	
	public void verifyAcceptCheckboxDisplayed()
	{
		By AcceptCheckbox=By.xpath("//input[@id='joinnow-agree']");
		
		objPojo.getLogReporter().webLog("Verify 'Accept Checkbox' displayed", 
				objPojo.getWebActions().checkElementDisplayed(AcceptCheckbox));
		}

	public void verifyAcceptTnCsLabelDisplayed()
	{
		By AcceptTnCsLabel = By.xpath("//label[contains(text(),'I accept the Terms and Conditions for this promotion')]");
		
		objPojo.getLogReporter().webLog("Verify 'I accept the Terms and Conditions for this promotion' label displayed", 
				objPojo.getWebActions().checkElementDisplayed(AcceptTnCsLabel));
	}
	
	public void SelectAcceptCheckboxDisplayed()
	{
		By AcceptCheckbox=By.xpath("//input[@id='joinnow-agree']");
		
		objPojo.getLogReporter().webLog(" Select 'Accept Checkbox' displayed", 
				objPojo.getWebActions().click(AcceptCheckbox));
	}
	
	public void verifyClaimPromotionBTN()
	{
		By ClaimPromotionBTN = By.xpath("//button[contains(text(),'Claim promotion')]");
		
		objPojo.getLogReporter().webLog(" Verify 'Claim Promotion BTN' displayed", 
				objPojo.getWebActions().checkElementDisplayed(ClaimPromotionBTN));
	}
	public void clickClaimPromotionBTN()
	{
		By ClaimPromotionBTN = By.xpath("//button[contains(text(),'Claim promotion')]");
		
		objPojo.getLogReporter().webLog(" Click on 'Claim Promotion BTN' displayed", 
				objPojo.getWebActions().click(ClaimPromotionBTN));
	}
	public void verifyCancelLinkDisplayed()
	{
		By cancelLink = By.xpath("//a[contains(text(),'Cancel')]");
		objPojo.getLogReporter().webLog("Verify 'Cancel Link' displayed", 
				objPojo.getWebActions().checkElementDisplayed(cancelLink));
	}
	public void verifySuccessHeaderDisplayed()
	{
		By SuccessHeader = By.xpath("//h4[contains(text(),'Success')]");
		
		objPojo.getLogReporter().webLog("Verify 'Success Header' displayed", 
				objPojo.getWebActions().checkElementDisplayed(SuccessHeader));
	}
	
	public void verifyClaimedBonusNameDisplayed(String Bonusname)
	{
		By bonusname= By.xpath("//p[@class='link-only']");
		
		if(Bonusname.equals(objPojo.getWebActions().getText(bonusname)))
		{
			System.out.println("Claimed bonus name is displayed correctly");
		}
		
		else 
			System.out.println("there is mismatch in Claimed bonus name displayed ");
	}
	
	public void verifyCloseBTNDisplayed()
	{
		By CloseBTN= By.xpath("//button[contains(text(),'Close')]");
		objPojo.getLogReporter().webLog("Verify 'Close BTN' displayed", 
				objPojo.getWebActions().checkElementDisplayed(CloseBTN));
		
	}
	public void ClickCloseBTNDisplayed()
	{
		By CloseBTN= By.xpath("//button[contains(text(),'Close')]");
		objPojo.getLogReporter().webLog(" Click on 'Close BTN' displayed", 
				objPojo.getWebActions().click(CloseBTN));
		
	}
	public void verifyActivePromotionsBTNDisplayed()
	{
		By ActivePromotionsBTN= By.xpath("//button[contains(text(),'Active promotions')]");
		objPojo.getLogReporter().webLog("Verify 'Active Promotions BTN' displayed", 
				objPojo.getWebActions().checkElementDisplayed(ActivePromotionsBTN));
		
	}
	public void clickActivePromotionsBTNDisplayed()
	{
		By ActivePromotionsBTN= By.xpath("//button[contains(text(),'Active promotions')]");
		objPojo.getLogReporter().webLog(" Click on 'Active Promotions BTN' displayed", 
				objPojo.getWebActions().click(ActivePromotionsBTN));
	}
	
	public void verifyExpandButtonDisplayed()
	{
		By expandBTN = By.xpath("//button[@class='toggle-module-btn ']");
		
		if(objPojo.getWebActions().checkElementDisplayed(expandBTN))
				{
			objPojo.getLogReporter().webLog(" click 'expand + BTN' displayed", 
					objPojo.getWebActions().click(expandBTN));
				}
		else
		{
			By noTransactionFoundMSG = By.xpath("//p[contains(text(),'No items found')]");
			objPojo.getLogReporter().webLog(" 'No items found' message is displayed on Bonuses screen ", 
					objPojo.getWebActions().checkElementDisplayed(noTransactionFoundMSG));
		}
	}
	public void clickExpandButton()
	{
		By expandBTN = By.xpath("//button[@class='toggle-module-btn ']");
		
		objPojo.getLogReporter().webLog(" click 'expand + BTN' displayed", 
				objPojo.getWebActions().click(expandBTN));
	}
	
	public void clickCollapseButton()
	{
		By collapseBTN = By.xpath("//button[@class='toggle-module-btn active']");
		objPojo.getLogReporter().webLog(" click 'collapse - BTN' displayed", 
				objPojo.getWebActions().click(collapseBTN));
	}
	public void verifyActiveBonusesLabelsDisplayed(String labels)
	{
		if(labels.contains(","))
		{
			String[] arr1 = labels.split(",");
			
			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//span[@class='list-detail' and contains(text(),'"+labels2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify "+labels2+" is displayed on Active Bonuses screen ",  
						 objPojo.getWebActions().checkElementDisplayed(locator));
			}
		}
		else 
		{
			By noTransactionFoundMSG = By.xpath("//p[contains(text(),'No items found')]");
			
			objPojo.getLogReporter().webLog(" 'No items found' message is displayed on Bonuses screen ", 
					objPojo.getWebActions().checkElementDisplayed(noTransactionFoundMSG));
		}
	}
	public void verifyViewTCsLinkDisplayed()
	{
		By ViewTCsLink = By.xpath("//a[contains(text(),'View T&Cs')]");
		objPojo.getLogReporter().webLog("Verify 'View T&Cs Link' displayed on Active Bonuses screen ", 
				objPojo.getWebActions().checkElementDisplayed(ViewTCsLink));
	}
	public void verifyOptOutLinkDisplayed()
	{
		By OptOutLink = By.xpath("//a[contains(text(),'Opt out')]");
		objPojo.getLogReporter().webLog("Verify 'OptOut Link' displayed on Bonuses screen ", 
				objPojo.getWebActions().checkElementDisplayed(OptOutLink));
	}
	
	public void clickOptOutLink()
	{
		By OptOutLink = By.xpath("//a[contains(text(),'Opt out')]");
		objPojo.getLogReporter().webLog(" click on 'OptOut Link' displayed on Bonuses screen ", 
				objPojo.getWebActions().click(OptOutLink));
	}
	
	public void verifyAreUSureTXTonPopupDisplayed()
	{
		By AreUSureTXT = By.xpath("//h4[contains(text(),'Are you sure?')]");
		objPojo.getLogReporter().webLog("Verify 'Are You Sure TXT' displayed on confirmation popup ", 
				objPojo.getWebActions().checkElementDisplayed(AreUSureTXT));
	}
	public void verifyOptOutTXTonPopupDisplayed()
	{
		By OptOutTXT = By.xpath("//p[contains(text(),'Opt out')]");
		objPojo.getLogReporter().webLog("Verify 'Opt Out TXT' displayed on confirmation popup ", 
				objPojo.getWebActions().checkElementDisplayed(OptOutTXT));
	}
	
	public void verifyYesBTNonPopupDisplayed()
	{
		By YesBTN = By.xpath("//button[@class='yellow btn btn-default' and contains(text(),'Yes')]");
		objPojo.getLogReporter().webLog("Verify 'Yes Button' displayed on confirmation popup ", 
				objPojo.getWebActions().checkElementDisplayed(YesBTN));
	}
	
	public void clickYesBTNonPopup()
	{
		By YesBTN = By.xpath("//button[@class='yellow btn btn-default' and contains(text(),'Yes')]");
		objPojo.getLogReporter().webLog(" Click 'Yes Button' displayed on confirmation popup ", 
				objPojo.getWebActions().click(YesBTN));
	}
	public void verifyNoBTNonPopupDisplayed()
	{
		By NoBTN = By.xpath("//button[@class='blue btn btn-default' and contains(text(),'No')]");
		objPojo.getLogReporter().webLog("Verify 'No Button' displayed on confirmation popup ", 
				objPojo.getWebActions().checkElementDisplayed(NoBTN));
	}
	
	public void closeConfirmationPopupDisplayed()
	{
		By closeXBTN = By.xpath("//button[@class='close']");
		objPojo.getLogReporter().webLog("Verify 'Close X Button' displayed on confirmation popup ", 
				objPojo.getWebActions().checkElementDisplayed(closeXBTN));
		
		objPojo.getLogReporter().webLog(" click 'Close X Button' displayed on confirmation popup ", 
				objPojo.getWebActions().click(closeXBTN));
		
	}
	
	public void validateBonusStatus(String status)
	{
		By locator = By.xpath("//span[@class='list-detail' and contains(text(),'Bonus status')]/following::span[contains(text(),'"+status+"')]");
		
		objPojo.getLogReporter().webLog(" Verify Bonus Status displayed is : "+status,
				(status.equals(objPojo.getWebActions().getText(locator))));
	}
	
	
	// Bonus History page
	
	public void verifyFilterByBonusTypeLabelDisplayed()
	{
		By FilterByBonusTypeLabel = By.xpath("//p[@class='dropdown-label' and contains( text(),'Filter by bonus type:')]");
		objPojo.getLogReporter().webLog("Verify 'Filter By Bonus Type' Label displayed on Active Bonuses screen ", 
				objPojo.getWebActions().checkElementDisplayed(FilterByBonusTypeLabel));
	}
	
	public void verifyBonusTypesDisplayed(String type)
	{
		if(type.contains(","))
		{
			String[] arr1 = type.split(",");
			
			for (String labels2 : arr1) 
			{
				By locator = By.xpath("//ul[@class='dropdown-menu' ]/li/a[contains( text(),'"+labels2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify bonus type : "+labels2+" is displayed under filter dropdown list ",  
						 objPojo.getWebActions().checkElementDisplayed(locator));
			}
		}
	}
	
	public void selectBonusType(String bonus)
	{
		By bonustype = By.xpath("//ul[@class='dropdown-menu' ]/li/a[contains( text(),'"+bonus+"')]");
		
		objPojo.getLogReporter().webLog(" Select Bonus type as : " +bonus +" from list ", 
				objPojo.getWebActions().click(bonustype));
		objPojo.getWaitMethods().sleep(8);
	}
	
	public void clickBonusType(String bonus)
	{
		By bonustype = By.xpath("//button[@class='btn dropdown-toggle bonus-toggle' and contains( text(),'"+bonus+"')]");
		
		objPojo.getLogReporter().webLog(" click on selected Bonus type as : " +bonus +" on Bonus history ", 
				objPojo.getWebActions().click(bonustype));
		objPojo.getWaitMethods().sleep(8);
	}
	
	public void verifyTypeHDRDisplayed(String type)
	{
		By typeValue= By.xpath("//span[@class='list-info']");
		
		if(type.equals(objPojo.getWebActions().getText(typeValue))){
						
			objPojo.getLogReporter().webLog(" Filtered transaction Bonus type is : " +type +" on Bonus history ", 
					objPojo.getWebActions().checkElementDisplayed(typeValue));
		}
		else 
		{
			By noTransactionFoundMSG = By.xpath("//p[contains(text(),'No items found')]");
			
			objPojo.getLogReporter().webLog(" 'No items found' message is displayed on Bonuses screen ", 
					objPojo.getWebActions().checkElementDisplayed(noTransactionFoundMSG));
		}
	}
	public void verifyFilterByDateRangeLabelDisplayed()
	{
		By FilterByDateRangeLabel = By.xpath("//div[@class='myaccount-details']/p[contains(text(),'Filter by date range')]");
		
		objPojo.getLogReporter().webLog("Verify 'Filter By Date Range Label' displayed on Active Bonuses screen ", 
				objPojo.getWebActions().checkElementDisplayed(FilterByDateRangeLabel));
		
	}
	public void selectCurrentMonth(String currentMonth , String CurrentDay)
	{
		By Month = By.xpath("//div[@class='react-datepicker__current-month']");
		By Day = By.xpath("//div[@class='react-datepicker__week']/div[contains(text(),'"+CurrentDay+"')]");
		
	//	System.out.println("Month on the calender "+ objPojo.getWebActions().getText(Month));
		
		while(true)
		{
			if(currentMonth.equals(objPojo.getWebActions().getText(Month)))
		{
			break;
		}
		else 
		{
			By NextMonthBTN = By.xpath("//button[@class='react-datepicker__navigation react-datepicker__navigation--next' and contains(text(),'Next month')]");
			
			objPojo.getLogReporter().webLog(" Click on the Next month button on datePicker", 
					objPojo.getWebActions().click(NextMonthBTN));
		}
		}
		objPojo.getLogReporter().webLog(" Selected the Day as: "+ objPojo.getWebActions().getText(Day)+"on datePicker ", 
				objPojo.getWebActions().click(Day));
		}
	
	public void setFromDate(String fromdate )
	{
		By inpFromDate= By.xpath("//div[@class='react-datepicker__input-container']/input[@class='react-date-input']");
		
		objPojo.getLogReporter().webLog(" Click 'from date' field to select the date ", 
				objPojo.getWebActions().click(inpFromDate));
		
		this.selectCurrentMonth(GenericUtils.getRequiredDay("-1", "MMMM yyyy", ""),GenericUtils.getRequiredDay("-1","dd",""));
	}
	
	public void verifyToDate(String currDate )
	{
		By inpToDate = By.xpath("//div[@class='react-datepicker__input-container']/input[@value='"+currDate+"']");
		
		if (currDate.equals(objPojo.getWebActions().getAttribute(inpToDate,"value")))
		{
			objPojo.getLogReporter().webLog(" Verify Todate displayed is "+currDate, 
					objPojo.getWebActions().checkElementDisplayed(inpToDate));
		}
	}
	public void verifyResetLinkdisplayed()
	{
		By ResetLink = By.xpath("//div[@class='myaccount-details']/a[contains(text(),'Reset')]");
		objPojo.getLogReporter().webLog(" Verify 'Reset Link' displayed on Bonus History screen ", 
				objPojo.getWebActions().checkElementDisplayed(ResetLink));
	}
	public void verifyFilterButtonDisplayed()
	{
		By FilterBTN = By.xpath("//button[contains(text(),'Filter')]");
		objPojo.getLogReporter().webLog("Verify 'Filter Button'displayed on Active Bonuses screen ", 
				objPojo.getWebActions().checkElementDisplayed(FilterBTN));
	}
	
	public void clickFilterButton()
	{
		By FilterBTN = By.xpath("//button[contains(text(),'Filter')]");
		objPojo.getLogReporter().webLog(" click on 'Filter Button'displayed on Active Bonuses screen ", 
				objPojo.getWebActions().click(FilterBTN));
		objPojo.getWaitMethods().sleep(5);
	}
	
	public void verifyBonusNameOnBonusHistoryDisplayed(String Bonusname)
	{
		By bonusname= By.xpath("//div[@class='toggle-module-content']/p[contains(text(),'"+Bonusname+"')]");
		
		if(Bonusname.equals(objPojo.getWebActions().getText(bonusname)))
		{
			objPojo.getLogReporter().webLog("Claimed bonus name"+ Bonusname +"is displayed correctly under Bonus history ", 
					objPojo.getWebActions().checkElementDisplayed(bonusname));
		}
		else {
			
		objPojo.getLogReporter().webLog("Claimed bonus name"+ Bonusname +"is not displayed correctly under Bonus history ", 
				objPojo.getWebActions().checkElementDisplayed(bonusname));
		}
	}
	public void clickXbtn()
	{
		By closeXbtn = By.xpath("//a[@class='close']");
		
		objPojo.getLogReporter().webLog(" click on ' X' Button'displayed ", 
				objPojo.getWebActions().click(closeXbtn));
	}
	public void clickLogoutLink()
	{
		By logoutLink = By.xpath("//a[@class='logout-link']");
		
		objPojo.getLogReporter().webLog(" click on logout-link displayed  ", 
				objPojo.getWebActions().click(logoutLink));
	}
	
	
}
