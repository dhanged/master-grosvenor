package com.pagefactory.grosvenor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;

import com.generic.Pojo;
import com.generic.utils.GenericUtils;

public class AccountDetailsPage {
	private Pojo objPojo;


	public AccountDetailsPage(Pojo pojo){
		this.objPojo = pojo;
	}

	By logoutLink = By.xpath("//a[@class='logout-link']");
	By lastLoginLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]");
	By NameLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Name')]");
	By EmailLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Email')]");
	By DOBLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Date of birth')]");
	By PhoneLabel =By.xpath("//span[@class='list-detail' and contains(text(),'Phone')]");
	By PostcodeLabel = By.xpath("//span[@class='list-detail' and contains(text(),'Postcode')]");
	By EditLink = By.xpath("//a[@class='edit-details']");
	By NonEditableTXT = By.xpath("//span[@class='non-editable']");
	By editEmail = By.xpath("//input[@id='edit-email']");
	By editPhone = By.xpath("//input[@id='edit-phone']");

	public void clickAccountDetail()
	{
		By locator = By.xpath("//div[@class='myaccount-menu']/ul/li/a[contains(text(),'Account Details')]");
		objPojo.getWaitMethods().sleep(5);

		objPojo.getLogReporter().webLog(" Click on Account details ",  
				objPojo.getWebActions().click(locator));
	}

	public void clickChangePassword()
	{
		By locator = By.xpath("//div[@class='myaccount-link']/a[contains(text(),'Change Password')]");
		objPojo.getWaitMethods().sleep(5);

		objPojo.getLogReporter().webLog(" Click on Change Password  ",  
				objPojo.getWebActions().click(locator));
	}

	public void clickMarketingPreferences()
	{
		By locator = By.xpath("//div[@class='myaccount-link']/a[contains(text(),'Marketing Preferences')]");
		objPojo.getWaitMethods().sleep(5);

		objPojo.getLogReporter().webLog(" Click on Marketing Preferences",  
				objPojo.getWebActions().click(locator));

		By header = By.xpath("//span[@class='h2 '][contains(text(),'Marketing Preferences')]");
		objPojo.getLogReporter().webLog(" Verify Marketing Preferences Header displayed ",  
				objPojo.getWebActions().checkElementDisplayed(header));
	}

	//update Marketing Preferences

	public void verifyMarketingInfoText()
	{
		By infotxt = By.xpath("//fieldset[@class='change_password_marketing_preferences']//p[contains(text(),'Please use the following methods to send me the latest offers and communications from Grosvenor Casinos')]");

		objPojo.getLogReporter().webLog(" Verify 'Please use the following methods to send me the latest offers and communications from Grosvenor Casinos (tick all that apply) ...' Info message displayed ",  
				objPojo.getWebActions().checkElementDisplayed(infotxt));
	}


	public void updateMarketingPreferences(String pref)
	{
		if(pref.contains("~"))
		{
			String[] arr1 = pref.split("~");
			for (String pref1 : arr1) 
			{
				By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref1+"')]");
				objPojo.getLogReporter().webLog(" Click on Marketing Preferences: " +pref1,  
						objPojo.getWebActions().click(locator));
			}
		}
		else
		{
			By locator = By.xpath("//input[@class=' valid']/following-sibling::label[contains(text(),'"+pref+"')]");
			objPojo.getLogReporter().webLog(" Click on Marketing Preferences" +pref,  
					objPojo.getWebActions().click(locator));
		}
	}

	public void clickUpdatePreferencesBtn()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Update')]");

		objPojo.getLogReporter().webLog(" Click on Update button",  
				objPojo.getWebActions().click(locator));

	}
	//Change Password

	public void setCurrentPassword(String currpassword)
	{
		By locator= By.xpath("//input[@id='edit-current-password']");
		objPojo.getLogReporter().webLog(" Set on Current Password  ",  
				objPojo.getWebActions().setText(locator, currpassword));

	}
	public void setNewPassword(String newPwd)
	{
		By locator= By.xpath("//input[@id='edit-new-password']");
		objPojo.getLogReporter().webLog(" Set New Password: "+newPwd,  
				objPojo.getWebActions().setText(locator, newPwd));
	}

	public void clickUpdatePasswordBTN()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons padded']/div/button[contains(text(),'Update')]");
		objPojo.getLogReporter().webLog(" Click on Update button to change password   ",  
				objPojo.getWebActions().click(locator));

	}


	public void verifyLabels(String labels )
	{
		if(labels.contains(","))
		{
			String labl[] = labels.split(",");
			for (String labl1 : labl)
			{
				By Label01 = By.xpath("//span[@class='list-detail' and contains(text(),'"+labl1+"')]");
				//System.out.println(" Label  " +labl1);
				objPojo.getLogReporter().webLog(" Verify "+labl1+" label is displayed",
						objPojo.getWebActions().checkElementDisplayed(Label01));
			}
		}
	}

	public void getLastLoginFormat()
	{
		By lastLoginDate = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]/following::span[1]");

		System.out.println("Last Login Date : "+objPojo.getWebActions().getText(lastLoginDate));
	}

	public void validateLastLoginFormatDisplayed()
	{
		By lastLoginDate = By.xpath("//span[@class='list-detail' and contains(text(),'Last login')]/following::span[1]");
		String lastLogin = objPojo.getWebActions().getText(lastLoginDate);
		lastLogin=lastLogin.replaceAll("(?<=\\d)(st|nd|rd|th)", "") ;
		
		DateFormat lastFormat = new SimpleDateFormat("dd MMM yyyy, hh:mm:ss a");
		try
		{
			Date lastlogindate = lastFormat.parse(lastLogin);
			objPojo.getLogReporter().webLog("last login date: "+lastLogin+" displayed in correct format" , true);
			
		}catch(Exception e)
		{
			objPojo.getLogReporter().webLog("last login date :"+lastLogin+" displayed in correct format" , false);
		}
			
	}

	public void verifyEditlinkDisplayed()
	{
		objPojo.getLogReporter().webLog(" Verify Edit Link is displayed",
				objPojo.getWebActions().checkElementDisplayed(EditLink));
	}

	public void clickEditLink()
	{
		objPojo.getLogReporter().webLog(" Click on Edit Link is displayed",
				objPojo.getWebActions().click(EditLink));
	}

	public void verifyNonEditableTXTDisplayed()
	{
		objPojo.getLogReporter().webLog(" Verify Non Editable TXT is displayed",
				objPojo.getWebActions().checkElementDisplayed(NonEditableTXT));
	}

	public void clickClearBtn()
	{
		By clearBtn = By.xpath("//input[@id='edit-email']/following-sibling::button[contains(text(),'Clear')]");
		objPojo.getLogReporter().webLog(" Click on Clear button displayed",
				objPojo.getWebActions().click(clearBtn));

	}
	public void EditEmail()
	{
		String newEmail = GenericUtils.getRandomAlphanumericEmailString(10, "@mailinator.com");

		By clearBtn = By.xpath("//input[@id='edit-email']/following-sibling::button[contains(text(),'Clear')]");
		objPojo.getLogReporter().webLog(" Click on Clear button displayed",
				objPojo.getWebActions().click(clearBtn));

		objPojo.getLogReporter().webLog(" Set Email ",  
				objPojo.getWebActions().setText(editEmail, newEmail));
	}

	public void EditPhoneNumber()
	{
		String newMobile ="077009" + GenericUtils.getRandomNumeric(5);

		By clearBtn = By.xpath("//input[@id='edit-phone']/following-sibling::button[contains(text(),'Clear')]");
		objPojo.getLogReporter().webLog(" Click on Clear button displayed",
				objPojo.getWebActions().click(clearBtn));

		objPojo.getLogReporter().webLog(" Set Mobile number ",  
				objPojo.getWebActions().setText(editPhone, newMobile));
	}
	public void inputPassword(String password)
	{
		By inputPassword = By.xpath("//input[@id='edit-password']");
		objPojo.getLogReporter().webLog(" Enter your password to update",  
				objPojo.getWebActions().setText(inputPassword, password));
	}
	
	public void showPasswordBTNdisplayed()
	{
		By showBTN = By.xpath("//button[contains(text(),'Show')]");
		
		objPojo.getLogReporter().webLog(" Verify Show button is displayed for Password ",
				objPojo.getWebActions().checkElementDisplayed(showBTN));
		
		objPojo.getLogReporter().webLog(" click on Show button is displayed for Password ",
				objPojo.getWebActions().click(showBTN));
		
	}

	
	public void clickUpdateBtn()
	{
		By updateButton = By.xpath("//fieldset[@class='form-buttons ']/div/button[contains(text(),'Update')]");
		objPojo.getLogReporter().webLog(" Click on Update button  ",  
				objPojo.getWebActions().click(updateButton));
	}

	public void verifyAccountUpdateSucsessMSGdisplayed(String msg)
	{
		By updateMsg = By.xpath("//div[@class='myaccount-success' and contains(text(),'"+msg+"')]");

		objPojo.getLogReporter().webLog(" Verify Account details updated is displayed",
				objPojo.getWebActions().checkElementDisplayed(updateMsg));

	}

	public void clickXbtn()
	{
		By closeXbtn = By.xpath("//a[@class='close']");
		objPojo.getLogReporter().webLog(" click on ' X' Button'displayed on Bonuse History screen ", 
				objPojo.getWebActions().click(closeXbtn));
	}
	public void clickLogoutLink()
	{
		By logoutLink = By.xpath("//a[@class='logout-link']");

		objPojo.getLogReporter().webLog(" click on logout-link displayed  ", 
				objPojo.getWebActions().click(logoutLink));
		objPojo.getWaitMethods().sleep(8);

	}




}
