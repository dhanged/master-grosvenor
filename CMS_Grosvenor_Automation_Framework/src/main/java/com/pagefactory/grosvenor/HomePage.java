package com.pagefactory.grosvenor;

import org.openqa.selenium.By;
import com.generic.Pojo;


public class HomePage {
	private Pojo objPojo;
	
	public HomePage(Pojo pojo){
		this.objPojo = pojo;
	}

	By SearchBtn = By.xpath("//button[@class='search__open-btn']");
	By inpSearchTxt = By.xpath("//input[@class='search-bar__input icon-search-white' and @placeholder='Search for a game']");
	
	By PlayBtnOnSearchResults = By.xpath("//button[@class='search-results__play-btn btn']");
	By NoResultsErrorMSG = By.xpath("//p[@class='search-no-results__message dialog-box' and contains(text(),'Sorry but no matches were found.')]");
	By browseSectionBTN = By.xpath("//button[@class='search-no-results__browse-btn' and contains(text(),'Browse sections')]");
	By GameInfoOnSearchResults = By.xpath("//a[@class='search-results__info']/i[@class='search-results__info-icon']");
	
	//Wallet balance
	By BalanceOnHomepage = By.xpath("//li[@class='text-data']/a/div/span/strong");
	By PlayableBalanceLabel = By.xpath("//span[@class='myaccount-preview__heading' and contains(text(),'Playable Balance')]");
	By PlayableBalanceAmount = By.xpath("//span[@class='myaccount-preview__amount preview-amount']");
	By balanceExpandBTN = By.xpath("//button[@class='preview-toggle reset']");
	By detailedViewBTN = By.xpath("//a[@class='balance-detailed-view-btn' and contains(text(),'Detailed view')]");
	By depositBTN = By.xpath("//button[@class='balance-deposit-btn' and contains(text(),'Deposit')]");
	
	// Wallet Balance 
	public String getBalanceOnHomepage()
	{
		String BalanceOnHomePAge =objPojo.getWebActions().getText(BalanceOnHomepage);
		
		System.out.println("Balance on Homepage : "+ BalanceOnHomePAge);
		
		return BalanceOnHomePAge;
	}
	
	public void verifyPlayableBalanceLabelDisplayed()
	{
		objPojo.getLogReporter().webLog("Verify Playable Balance Label is displayed",
				objPojo.getWebActions().checkElementDisplayed(PlayableBalanceLabel));
	}
	
	public String getPlayableBalanceAmount()
	{
		
		System.out.println("Playable Balance displayed  : "+ objPojo.getWebActions().getText(PlayableBalanceAmount));
		
		return objPojo.getWebActions().getText(PlayableBalanceAmount);
	}
	
	public void validateBalanceDisplayed(String HomepageBalance)
	{
		String PlayableBal = objPojo.getWebActions().getText(PlayableBalanceAmount);
		
		objPojo.getLogReporter().webLog(" Balance displayed on Homepage is:"+HomepageBalance +" matching the Playable Balance on My account screen " ,
					HomepageBalance.equals(PlayableBal));
	}
	public void clickExpandBalanceBTN()
	{
		objPojo.getLogReporter().webLog(" Click on Expand Balance (+) button ",
				objPojo.getWebActions().click(balanceExpandBTN));
	}
	
	public void verifyLabelsOnMyAccount(String labels)
	{
		if(labels.contains(","))
		{
			String[] arr1 = labels.split(",");
			
			for (String labels2 : arr1) 
			{
				By label01 = By.xpath("//div[@class='myaccount-balance']/ul/li/span[contains(text(),'"+labels2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify "+labels2+" is displayed on Balance >>My account screen",  
						 objPojo.getWebActions().checkElementDisplayed(label01));
			}
		}
	}
	
	public void verifyDetailedViewBTN()
	{
		objPojo.getLogReporter().webLog("Verify detailed View BTN  is displayed",
				objPojo.getWebActions().checkElementDisplayed(detailedViewBTN));
	}
	
	public void clickDetailedViewBTN()
	{
		objPojo.getLogReporter().webLog(" Click on detailed View BTN displayed",
				objPojo.getWebActions().click(detailedViewBTN));
	}
	
	public void verifyLabelsDetailedView(String labels)
	{
		if(labels.contains(","))
		{
			String[] arr1 = labels.split(",");
			
			for (String labels2 : arr1) 
			{
				By label01 = By.xpath("//span[@class='account-balance__heading' and contains(text(),'"+labels2+"')]");
				objPojo.getLogReporter().webLog(" Verify "+labels2+" is displayed on Detailed view screen",  
						 objPojo.getWebActions().checkElementDisplayed(label01));
			}
		}
	}
	
	public String getValueofLabelOnDetailedView(String label)
	{
		By value = By.xpath("//span[@class='account-balance__heading' and contains(text(),'"+label+"')]//following::span[@class='account-balance__amount preview-amount']");
		
		String val = objPojo.getWebActions().getText(value);
		return val.substring(1);
	}
	
	public String getPlayableBalanceOnDetailedView()
	{
		By PlayableBalance = By.xpath("//span[@class='account-balance__amount preview-amount']");
		System.out.println("Playable Balance displayed on detailed view   : "+ objPojo.getWebActions().getText(PlayableBalance));
		
		return objPojo.getWebActions().getText(PlayableBalance);
	}
	
	public void clickCashExpandBTN()
	{
		By cashExpandBTN = By.xpath("//span[@class='collapse-button details-content-toggle closed enabled']");
		
		objPojo.getLogReporter().webLog(" Click to expand Cash wallet",
				objPojo.getWebActions().click(cashExpandBTN));
	}
	
	public void validateCashAmountOnDetailedView(String cashDefault , String nonCredit)
	{
		String digitalCash = this.getValueofLabelOnDetailedView("Cash");
		
		float totalDigitalCash =Float.parseFloat(cashDefault) + Float.parseFloat(nonCredit) ;
		float digitalCashamt = Float.parseFloat(digitalCash);
			
		objPojo.getLogReporter().webLog(" Cash amount displayed is Sum of CashDefault and NonCredit wallet : "+totalDigitalCash ,
				String.valueOf(digitalCashamt).equals(String.valueOf(totalDigitalCash) ));
	}
	
	public void validatePlayableBalanceOnDetailedView()
	{
		String totalPlayablebalance = this.getPlayableBalanceOnDetailedView().substring(1);
		String Cashamt = this.getValueofLabelOnDetailedView("Cash");
		String RewardAmt = this.getValueofLabelOnDetailedView("Reward");
		String bonusAmt = this.getValueofLabelOnDetailedView("All games bonuses");
		
		float SumOfCashRewardsBonus = Float.parseFloat(Cashamt) +Float.parseFloat(RewardAmt)+Float.parseFloat(bonusAmt);
		float playableBalanceamt = Float.parseFloat(totalPlayablebalance);
		
		objPojo.getLogReporter().webLog(" Playable balance displayed on Detailed view is Sum of online Cash,Rewards and Bonus : "+SumOfCashRewardsBonus ,
				String.valueOf(playableBalanceamt).equals(String.valueOf(SumOfCashRewardsBonus)) );
		
	}
	
	
	public void validatePlayableOnlineCash(String cashDefault , String nonCredit)
	{
		String PlayableOnlinecash = this.getValueofLabelOnDetailedView("Playable online cash");
		
		float totalOnlineCash =Float.parseFloat(cashDefault) + Float.parseFloat(nonCredit) ;
		float PlayableOnline = Float.parseFloat(PlayableOnlinecash);
			
		objPojo.getLogReporter().webLog(" Playable Online Cash displayed is Sum of CashDefault and NonCredit wallet : "+PlayableOnlinecash ,
				String.valueOf(totalOnlineCash).equals(String.valueOf(PlayableOnline) ));
	}
	
	public void validateInClubCash(String nonCredit , String unplayable)
	{
		String PlayableinClubCash = this.getValueofLabelOnDetailedView("Playable in-club");
		
		float totalinClubCash =Float.parseFloat(nonCredit) + Float.parseFloat(unplayable) ;
		float Playableinclub = Float.parseFloat(PlayableinClubCash);
		
		objPojo.getLogReporter().webLog(" Playable in-club Cash displayed is Sum of NonCredit and Unplayable wallet : "+totalinClubCash ,
				String.valueOf(totalinClubCash).equals(String.valueOf(Playableinclub)) );
		
	}
	
	public void ValidateTotalCashBalanceDisplayed()
	{
		String totalPlayablebalance = this.getPlayableBalanceOnDetailedView().substring(1);
		String PlayableOnlinecash = this.getValueofLabelOnDetailedView("Playable online cash");
		String RewardAmt = this.getValueofLabelOnDetailedView("Reward");
		String bonusAmt = this.getValueofLabelOnDetailedView("All games bonuses");
		
		float cash = Float.parseFloat(totalPlayablebalance);
		float SumOfCashRewardsBonus = Float.parseFloat(PlayableOnlinecash) +Float.parseFloat(RewardAmt)+Float.parseFloat(bonusAmt);
		objPojo.getLogReporter().webLog(" Playable balance displayed on Detailed view is Sum of online Cash,Rewards and Bonus : "+SumOfCashRewardsBonus ,
				String.valueOf(cash).equals(String.valueOf(SumOfCashRewardsBonus)) );
	}
	
//Search Functionality	
	public void verifySearchBtnDisplayed()
	{
		objPojo.getLogReporter().webLog("Verify Search button is displayed on Homepage ",
				objPojo.getWebActions().checkElementDisplayed(SearchBtn));
	}
	public void clickSearchBtn()
	{
		objPojo.getWaitMethods().sleep(6);
		objPojo.getLogReporter().webLog(" Click on Search button  on Homepage ",
				objPojo.getWebActions().click(SearchBtn));
	}
	public void verifyinputSearchNameFieldDisplayed()
	{
		objPojo.getLogReporter().webLog("Verify Search input field is displayed on Homepage ",
				objPojo.getWebActions().checkElementDisplayed(SearchBtn));
	}
	public void setSearchNameField(String Gamename)
	{
		objPojo.getLogReporter().webLog(" input text to search on Homepage ",
				objPojo.getWebActions().setText(inpSearchTxt, Gamename));
	}
	public void verifyGameNameOnSearchResultsDisplayed(String Gamename)
	{
		By GameNameOnSearchResults = By.xpath("//div[@class='search-results__title heading']/span/strong[contains(text(),'"+Gamename+"')]");
		
		objPojo.getLogReporter().webLog(" Verify Game Name containing "+ objPojo.getWebActions().getText(GameNameOnSearchResults)+ " On Search Results displayed",
				objPojo.getWebActions().checkElementDisplayed(GameNameOnSearchResults));
	}
	
	public void verifyInfoIconOnSearchResultsDisplayed(String Gamename)
	{
		
		objPojo.getLogReporter().webLog("Verify Info Icon On Search Results displayed",
				objPojo.getWebActions().checkElementDisplayed(GameInfoOnSearchResults));
	}
	
	public void clickInfoIconOnSearchResults()
	{
		objPojo.getLogReporter().webLog(" click on Info Icon On Search Results displayed",
				objPojo.getWebActions().click(GameInfoOnSearchResults));
	}
	
	public void verifyPlayBtnOnSearchResultsDisplayed()
	{
		objPojo.getLogReporter().webLog("Verify Play Button On Search Results displayed",
				objPojo.getWebActions().checkElementDisplayed(PlayBtnOnSearchResults));
	}
	
	public void verifyTabsOnGameInfo(String tabname)
	{
		if(tabname.contains("~"))
		{
			String[] arr1 = tabname.split("~");
			
			for (String tabname2 : arr1) 
			{
				By sectionName = By.xpath("//a[contains(text(),'"+tabname2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify "+tabname2+" section is displayed on Game info page ",  
						 objPojo.getWebActions().checkElementDisplayed(sectionName));
			}
		}
	}
	public void clickCloseXonGameInfoPage()
	{
		By CloseXBTN = By.xpath("//button[@class='close']");
		objPojo.getLogReporter().webLog("click on X button on game info page",
				objPojo.getWebActions().click(CloseXBTN));
	}
	
	
	
	public void clickPlayBtnOnSearchResults()
	{
		objPojo.getLogReporter().webLog("Verify Play Button On Search Results displayed",
				objPojo.getWebActions().click(PlayBtnOnSearchResults));
	}
	
	public void verifyNoResultsErrorMSGDisplayed()
	{
		By NoResultsErrorMSG = By.xpath("//p[@class='search-no-results__message dialog-box' and contains(text(),'Sorry but no matches were found.')]");
		objPojo.getLogReporter().webLog(" Verify 'Sorry but no matches were found' Message displayed",
				objPojo.getWebActions().checkElementDisplayed(NoResultsErrorMSG));
	}
	public void verifybrowseSectionBTNDisplayed()
	{
		objPojo.getLogReporter().webLog(" Verify 'Browse Sections BTN' displayed",
				objPojo.getWebActions().checkElementDisplayed(browseSectionBTN));
	}
	public void clickbrowseSectionBTN()
	{
		objPojo.getLogReporter().webLog(" Click 'Browse Sections BTN' displayed",
				objPojo.getWebActions().click(browseSectionBTN));
	}
	
	public void verifycloseBTNDisplayed()
	{
		By closeBTN = By.xpath("//button[@class='search-no-results__close-btn info' and contains(text(),'Close')]");
		objPojo.getLogReporter().webLog(" Verify 'close BTN' on error message  displayed",
				objPojo.getWebActions().checkElementDisplayed(closeBTN));
	}
	
	public void clickCloseSearchBTN()
	{
		By CloseSearchBTN = By.xpath("//button[@class='search__close-btn']");
		
		objPojo.getLogReporter().webLog("click on X button to close the search results",
				objPojo.getWebActions().click(CloseSearchBTN));
	}
	
	public void verifyIconsDisplayed()
	{
		By SevenIcon = By.xpath("//i[@class='sitemap__icon seven']");
		By TuxedoIcon = By.xpath("//i[@class='sitemap__icon tux']");
		
		objPojo.getLogReporter().webLog(" Verify 'Seven Icon' displayed",
				objPojo.getWebActions().checkElementDisplayed(SevenIcon));
		
		objPojo.getLogReporter().webLog(" Verify 'Tuxedo Icon' displayed",
				objPojo.getWebActions().checkElementDisplayed(TuxedoIcon));
		
	}
	
	public void verifySectionsDisplayed(String sections)
	{
		if(sections.contains(","))
		{
			String[] arr1 = sections.split(",");
			
			for (String sections2 : arr1) 
			{
				By sectionName = By.xpath("//a[@class='sitemap__sub-section-title' and contains(text(),'"+sections2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify "+sections2+" is displayed when clicked on Browse section button ",  
						 objPojo.getWebActions().checkElementDisplayed(sectionName));
			}
		}
		
	}
	
	
	
	
	public void clickOnSection(String secname , String url ,String pageTitle)
	{
		By sectionName = By.xpath("//a[@class='sitemap__sub-section-title' and contains(text(),'"+secname+"')]");
		
		objPojo.getWebDriverProvider().openNewWindow(sectionName);
		objPojo.getWaitMethods().sleep(12);
		objPojo.getWebActions().switchToWindowUsingTitle(pageTitle);
		objPojo.getWaitMethods().sleep(12);
		
		String currentURL = objPojo.getWebDriverProvider().getDriver().getCurrentUrl();
		objPojo.getLogReporter().webLog(secname +" Redirected to the correct url : ", currentURL.equalsIgnoreCase(url));
		
		objPojo.getWebActions().switchToWindowUsingTitle("Online Casino | Play Online with the UK's Biggest Casino Brand");
	}
	
	
	
	
	public void clickOnSection(String secname)
	{
		By sectionName = By.xpath("//a[@class='sitemap__sub-section-title' and contains(text(),'"+secname+"')]");
		
		objPojo.getLogReporter().webLog(" click on section :  "+secname,  
				 objPojo.getWebActions().click(sectionName));
	}
	
	
	
	public void verifyPlayBtnOnGameDetails()
	{
		By RealPlayButton = By.xpath("//button[@class='load-game column-4-middle column-4-large' and contains(text(),'Play')]");
		objPojo.getLogReporter().webLog(" Verify 'RealPlay Button' displayed",
				objPojo.getWebActions().checkElementDisplayed(RealPlayButton));
	}
	
	public void clickPlayBtnOnGameDetails()
	{
		objPojo.getWaitMethods().sleep(5);
		By RealPlayButton = By.xpath("//button[@class='load-game column-4-middle column-4-large' and contains(text(),'Play')]");
		objPojo.getLogReporter().webLog(" click'RealPlay Button' displayed",
				objPojo.getWebActions().click(RealPlayButton));
		objPojo.getWaitMethods().sleep(12);
	}
	
	public void clickStartgameBTN()
	{
	
		objPojo.getWebActions().switchToFrameUsingNameOrId("game-overlay-iframe"); 
		objPojo.getWebActions().switchToFrameUsingNameOrId("playerFrame"); 
		By startgame = By.xpath("//div[@id='framePrimaryGameButtons']//following-sibling::div[@id='frameStartButtonPrimary']");
		objPojo.getWaitMethods().sleep(5);
		
		By balnce = By.xpath("//span[@id='frameStatusBarBalance']");
		System.out.println(objPojo.getWebActions().getText(balnce));
		
		objPojo.getWebActions().javascriptClick(startgame);
		
						objPojo.getWaitMethods().sleep(8);		
				System.out.println(objPojo.getWebActions().getText(balnce));
	}
	
	
}
