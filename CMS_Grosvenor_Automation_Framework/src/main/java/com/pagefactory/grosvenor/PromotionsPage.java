package com.pagefactory.grosvenor;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;

import com.generic.Pojo;

public class PromotionsPage {
	
private Pojo objPojo;
	
	
	public PromotionsPage(Pojo pojo){
		this.objPojo = pojo;
	}

	
	By moreinfoLink = By.xpath("//div[@class='overlay-cta promotions__button-group']/a[@class='more-info promotions__ghost']");
	
	
	public void navigateToPromosTAB()
	{
		By PromosprimaryTAB = By.xpath("//nav[@class='component top-navigation']/ul/li/a[@class='star']");
		
		objPojo.getLogReporter().webLog(" Verify Promotions tab from Primary Navigation displayed",
				objPojo.getWebActions().checkElementDisplayed(PromosprimaryTAB));
		
		objPojo.getLogReporter().webLog("Click on the Promotions tab from Primary Navigation",
				objPojo.getWebActions().click(PromosprimaryTAB));
		
	}
	
	public void verifySecondaryLinksDisplayed(String lnk)
	{
		if(lnk.contains(";"))
		{
			String[] arr1 = lnk.split(";");
			
			for (String lnk2 : arr1) 
			{
				By locator = By.xpath("//a[contains(text(),'"+lnk2+"')]");
				
				objPojo.getLogReporter().webLog(" Verify "+lnk2+" is displayed on secondary tab on Promotions page ",  
						 objPojo.getWebActions().checkElementDisplayed(locator));
			}
		}
	}
	public void clickSecondaryLinksDisplayed(String lnk)
	{
		if(lnk.contains(";"))
		{
			String[] arr1 = lnk.split(";");
			
			for (String lnk2 : arr1) 
			{
				By locator = By.xpath("//a[contains(text(),'"+lnk2+"')]");
				objPojo.getLogReporter().webLog(" Click on  "+lnk2+" is displayed on secondary tab on Promotions page ",  
						 objPojo.getWebActions().click(locator));
			}
		} 
		else
		{	
			By locator = By.xpath("//a[contains(text(),'"+lnk+"')]");
			objPojo.getLogReporter().webLog(" Click on  "+lnk+" is displayed on secondary tab on Promotions page ",  
				 objPojo.getWebActions().click(locator));
		}
	}
	
	
	public void clickMoreinfolink()
	{
		objPojo.getLogReporter().webLog("Click on the more info Link displayed",
				objPojo.getWebActions().click(moreinfoLink));
	}
	
	public void verifyCalendarIconDisplayed()
	{
		By calendarIcon = By.xpath("//i[@class='icon-Calendar']");
		
		objPojo.getLogReporter().webLog(" Verify calendar Icon displayed on promotion info page",
				objPojo.getWebActions().checkElementDisplayed(calendarIcon));
	}
	
	public void validateActiveExpiryDate()
	{
		By promoDate = By.xpath("//div[@class='promo-date']/p");
		String promodt = objPojo.getWebActions().getText(promoDate);
		
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		String input = objPojo.getWebActions().getText(promoDate);
		String dts [] = input.split("-");
		if (dts != null && dts.length > 1) {
			String toDateStr = dts[1].trim().replaceAll("(\\d+).* (.*) (.*)", "$1 $2 $3");
			String formDateStr = dts[0].trim().replaceAll("(\\d+).* (.*)", "$1 $2");
			System.out.println("From Date: " + formDateStr);
			System.out.println("To Date: " + toDateStr);
			try {
				Date toDate = dateFormat.parse(toDateStr);
				formDateStr += " " + (1900 + toDate.getYear());
				Date fromDate = dateFormat.parse(formDateStr);
				objPojo.getLogReporter().webLog(" Verify Active date is before the expiry date",
						fromDate.before(toDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void clickClaimBTN()
	{
		By claimBTN= By.xpath("//button[@class='opt-in-button button']");
		
		objPojo.getLogReporter().webLog("Click on the claim/OPTIN Button displayed",
				objPojo.getWebActions().click(claimBTN));
	}
	
	
	public void findPromoBTN(String btn)
	{
		By promoBTN = By.xpath("//button[contains(text(),'"+btn+"')]");
		
		objPojo.getLogReporter().webLog("Verify "+btn+" button displayed on the Promotions page ",
				objPojo.getWebActions().checkElementDisplayed(promoBTN));
	}
	
	
	public void clickPromoBTNonPromotions(String btn)
	{
		By promoBTN = By.xpath("//button[contains(text(),'"+btn+"')]");
		
		objPojo.getLogReporter().webLog(" click on "+btn+" button displayed on the Promotions page ",
				objPojo.getWebActions().click(promoBTN));
	}
	
	public void clickFollowingMoreInfoLink(String btn)
	{
		By followingMoreinfoLink = By.xpath("//button[contains(text(),'"+btn+"')]/following-sibling::a[contains(text(),'More info')]");
		
		objPojo.getLogReporter().webLog("Click on the More info link following "+btn+" Button displayed",
				objPojo.getWebActions().click(followingMoreinfoLink));
	}
	
	public void verifyTickIMGDisplayed()
	{
		By locator = By.xpath("//div[@id='opted-in']/img[@src='/assets/grosvenor/img/promo/tick.png']");
		
		objPojo.getLogReporter().webLog(" Verify Green Tick image displayed ",
				objPojo.getWebActions().checkElementDisplayed(locator));
		
	}
	
	public void verifyOptedInTXTDisplayed()
	{
		By locator = By.xpath("//div[@id='opted-in']/p[contains(text(),'Opted-in')]");
		
		objPojo.getLogReporter().webLog(" Verify Opted-in text displayed when clicked on OptIn BTN",
				objPojo.getWebActions().checkElementDisplayed(locator));
	}
	
	public void verifyOptedInTXTonPromotionsPage()
	{
		By locator = By.xpath("//div[@class='opted-in claim-success']/p[contains(text(),'Opted-in')]");
		
		objPojo.getLogReporter().webLog(" Verify Opted-in text displayed when clicked on OptIn BTN on Promotions list",
				objPojo.getWebActions().checkElementDisplayed(locator));
	}
	
	
	
	public void verifyOptOutBTNDisplayed()
	{
		By locator = By.xpath("//button[@class='opt-in-button button' and contains(text(),'Opt out')]");
		
		objPojo.getLogReporter().webLog(" Verify Opt out Button displayed when clicked on OptIn BTN",
				objPojo.getWebActions().checkElementDisplayed(locator));
		
	}
	
	
	//BiB locators
	
	By BuyInBTN =By.xpath("//button[@class='submit' and contains(text(),'Buy-in')]");
	By cancelBTN = By.xpath("//button[@class='cancel' and contains(text(),'Cancel')]");
	By availableCashLabel = By.xpath("//h3[@class='heading' and contains(text(),'Your available cash: ')]");
	By OkBTN =By.xpath("//button[@class='submit' and contains(text(),'OK')]");
	
	public void verifyavailableCashLabeldisplayed()
	{
		objPojo.getLogReporter().webLog(" Verify 'Available Cash : ' Label displayed on popup",
				objPojo.getWebActions().checkElementDisplayed(availableCashLabel));
	}
	
	public String getAvailableCashAmount()
	{
		By availableCashLocator = By.xpath("//h3[@class='heading' and contains(text(),'Your available cash: ')]/span");
		return objPojo.getWebActions().getText(availableCashLocator);
	}
	
	public String getBuyInForCashAmount()
	{
		By BuyInForCashLocator = By.xpath("//div[@class='first']/label[@class='number']");
		return objPojo.getWebActions().getText(BuyInForCashLocator);
	}
	
	public String getBuyInBonusAmount()
	{
		By BuyInBonusAmountLocator = By.xpath("//div[@class='second']/label[@class='number']");
		return objPojo.getWebActions().getText(BuyInBonusAmountLocator);
	}
	
	public String getPlayWithAmount()
	{
		By PlayWithAmountLocator = By.xpath("//div[@class='third']/label[@class='number']");
		return objPojo.getWebActions().getText(PlayWithAmountLocator);
	}
	
	
	public void verifyBuyInBTNdisplayed()
	{
		objPojo.getLogReporter().webLog(" Verify BuyIn Button displayed on popup",
				objPojo.getWebActions().checkElementDisplayed(BuyInBTN));
	}
	
	public void clickBuyInBTN()
	{
		objPojo.getLogReporter().webLog(" Click BuyIn Button displayed on popup",
				objPojo.getWebActions().click(BuyInBTN));
	}
	
	public void verifyCancelBTNdisplayed()
	{
		objPojo.getLogReporter().webLog(" Verify Cancel Button displayed on popup",
				objPojo.getWebActions().checkElementDisplayed(cancelBTN));
	}
	
	public void verifyConfirmBuyingInHDRDisplayed()
	{
		By confirmHDR = By.xpath("//h2[contains(text(),'Confirm')]");
		By buyingInHDR = By.xpath("//h2[contains(text(),'Buying-in')]");
		objPojo.getLogReporter().webLog(" Verify confirm HDR displayed on confirmation popup",
				objPojo.getWebActions().checkElementDisplayed(confirmHDR));
		objPojo.getLogReporter().webLog(" Verify buyingIn HDR displayed on confirmation popup",
				objPojo.getWebActions().checkElementDisplayed(buyingInHDR));
	}
	
	public void verifyOKBTNonPopup()
	{
		objPojo.getLogReporter().webLog(" Verify Ok Button displayed on confirmation popup",
				objPojo.getWebActions().checkElementDisplayed(OkBTN));
	}

	public void clickOKBTNonPopup()
	{
		objPojo.getLogReporter().webLog(" Click Ok Button displayed on confirmation popup",
				objPojo.getWebActions().click(OkBTN));
	}


	public void verifyCancelBTNonPopup()
	{
		By locator= By.xpath("//fieldset[@class='form-buttons ']/div/button[@class='cancel' and contains(text(),'Cancel')]");
		
		objPojo.getLogReporter().webLog(" Verify Cancel BTN displayed on confirmation popup",
				objPojo.getWebActions().checkElementDisplayed(locator));
	}
	
	
	public void verifyBuyInSuccessMsgDisplayed()
	{
		By congratulationsHDR = By.xpath("//h3[contains(text(),'Congratulations')]");
		By youHaveReceivedTXT = By.xpath("//h2[contains(text(),'You have received')]");
		objPojo.getLogReporter().webLog(" Verify congratulations text displayed on Success msg popup",
				objPojo.getWebActions().checkElementDisplayed(congratulationsHDR));
		objPojo.getLogReporter().webLog(" Verify 'You have received..' displayed on Success msg popup",
				objPojo.getWebActions().checkElementDisplayed(youHaveReceivedTXT));
	}
	
	public void validateReceivedAmount(String playwithamt )
	{
		By locator = By.xpath("//h3[text()='Congratulations']//following-sibling::h2");
		String actualPlayAmount = objPojo.getWebActions().getText(locator);
		actualPlayAmount = actualPlayAmount.substring(actualPlayAmount.indexOf("£"),actualPlayAmount.indexOf("B"));
		actualPlayAmount = actualPlayAmount.replace(" ", "");
	
		objPojo.getLogReporter().webLog(" Verify 'You have received amount : "+playwithamt +" is displayed correctly on Success msg popup",
				actualPlayAmount.equals(playwithamt));
	}
	
	public void verifyBuyInDetailsOnPopup() {
		
		By locator= By.xpath("//p[@class='buy-in-bonus_confirmation__description' and contains(text(),'Here are the details of your buy-in operation: ')]");
		
		objPojo.getLogReporter().webLog(" Verify 'Here are the details of your buy-in operation: ' displayed on Success msg popup",
				objPojo.getWebActions().checkElementDisplayed(locator));
		
	}
	
	public void validateBonusDetailOnPopup(String availableCash,String BuyInForCash,String BuyInBonusAmount)
	{
		
		By availableCashLocator = By.xpath("//p[@class='buy-in-bonus_confirmation__description']");
		String availableCashAmount = objPojo.getWebActions().getText(availableCashLocator);
		
		String [] numbers = availableCashAmount.replaceAll(("[^0-9]+"), " ").split(" ");
		
		Float popupAvailableCash = Float.parseFloat(availableCash.substring(1)) - Float.parseFloat(BuyInForCash.substring(1)) ;
			
		objPojo.getLogReporter().webLog(" Your available cash: amount displayed on popup is correct "+popupAvailableCash ,
				popupAvailableCash.equals(Float.parseFloat(numbers[3])));
		
		Float popupBonusBalance =Float.parseFloat(BuyInForCash.substring(1)) + Float.parseFloat(BuyInBonusAmount.substring(1)) ;
	
		objPojo.getLogReporter().webLog(" You bonus balance: amount displayed on popup is correct "+popupBonusBalance ,
				popupBonusBalance.equals(Float.parseFloat(numbers[6])));
		
		
	}
	
	
	
}
