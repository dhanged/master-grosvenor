package com.pagefactory.grosvenor;
import org.openqa.selenium.By;

import com.generic.Pojo;

public class CasinoDetailsPage {
	
	private Pojo objPojo;
	
	By FindACasino = By.xpath("//div[@class='sub-navigation-wrapper']/p/a[contains(text(),'Find a Casino')]");
	By FindHDR = By.xpath("//h2[@class='location' and contains(text(),'Find')]");
	By inputPostcode = By.xpath("//input[@id='casino-finder']");
	By clearXbtn = By.xpath("//button[@class='clear']");
	By viewAllCasinosLink = By.xpath("//a[@class='view-cta' and contains(text(),'View all casinos')]");
	By casinoName = By.xpath("//span[@class='name']");
	String CasinoName ;
	
	public CasinoDetailsPage(Pojo pojo){
		this.objPojo = pojo;
	}
	
	
	
	public void navigateToCasinosTAB()
	{
		By CasinosPrimaryTAB = By.xpath("//nav[@class='component top-navigation']/ul/li/a[@class='house']");
		
		objPojo.getLogReporter().webLog(" Verify Casinos tab from Primary Navigation displayed",
				objPojo.getWebActions().checkElementDisplayed(CasinosPrimaryTAB));
		
		objPojo.getLogReporter().webLog("Click on the Casinos tab from Primary Navigation",
				objPojo.getWebActions().click(CasinosPrimaryTAB));
		
	}
	
	public void verifyFindCasinoTABDisplayed()
	{
		objPojo.getLogReporter().webLog("verify Find a casino link displayed from secondary navigation",
				objPojo.getWebActions().checkElementDisplayed(FindACasino));
		
	}
	public void ClickFindCasinoTAB()
	{
		objPojo.getLogReporter().webLog(" Click on  Find a casino link displayed from secondary navigation",
				objPojo.getWebActions().click(FindACasino));
	}
	public void verifyFindHDRdisplayed()
	{
		objPojo.getLogReporter().webLog("verify Find Header is displayed ",
				objPojo.getWebActions().checkElementDisplayed(FindHDR));
	}
	public void setPostcodeToSearch(String searchTxt)
	{
		objPojo.getLogReporter().webLog("click  field Input postcode / Town to search casino ",
				objPojo.getWebActions().click(inputPostcode));
		
		objPojo.getLogReporter().webLog(" Input postcode / Town to search casino ",
				objPojo.getWebActions().setText(inputPostcode, searchTxt));
	}
	
	public void verifySearchResultsdisplayed()
	{
		objPojo.getWaitMethods().sleep(6);
		By resultsTXT = By.xpath("//div[@class='search-list-location' and contains(text(),'These are the')]");
		By milesTXT = By.xpath("//span[@class='miles']");
		By casinoName = By.xpath("//span[@class='name']");
		
		objPojo.getLogReporter().webLog(" Verify message-'These are the 5 nearest casinos... ' is displayed ",
				objPojo.getWebActions().checkElementDisplayed(resultsTXT));
		objPojo.getLogReporter().webLog(" Verify casino distance in Miles is displayed ",
				objPojo.getWebActions().checkElementDisplayed(milesTXT));
		objPojo.getLogReporter().webLog(" Verify casino name is displayed ",
				objPojo.getWebActions().checkElementDisplayed(casinoName));
			
	}
	
	
	
	public String getcasinoname()
	{
		return objPojo.getWebActions().getText(casinoName);
	}
	
	public void setTownToSearch(String searchTxt)
	{
		By inputTown = By.xpath("//input[@id='casino-finder']");

		objPojo.getLogReporter().webLog("click  field Input Town to search casino ",
				objPojo.getWebActions().click(inputPostcode));
		
		objPojo.getLogReporter().webLog(" Input Town to search casino ",
				objPojo.getWebActions().setText(inputTown, searchTxt));
	}
	
	public void clickResultDisplayed()
	{
		By casinoName = By.xpath("//span[@class='name']");
		objPojo.getLogReporter().webLog(" Click on the casino name displayed ",
				objPojo.getWebActions().click(casinoName));
	}
	
	public void verifyDirectionsDisplayed()
	{
		By directionsLnk= By.xpath("//a[contains(text(),'Directions')]");
		
		objPojo.getLogReporter().webLog(" Verify Directions link is displayed ",
				objPojo.getWebActions().checkElementDisplayed(directionsLnk));
	}
	public void clickMoreInfo()
	{
		By moreInfoLnk= By.xpath("//a[contains(text(),'More Info')]");
	
		objPojo.getLogReporter().webLog(" Click on the More info link displayed ",
				objPojo.getWebActions().click(moreInfoLnk));
	}
	
	
	public void verifyCasinoNameDisplayed(String CasName)
	{
		
		By CasinoNameHDR= By.xpath("//h1[contains(text(),'"+CasName+"')]");
		
		objPojo.getLogReporter().webLog(" Verify Casino name "+CasName+" is displayed ",
				objPojo.getWebActions().checkElementDisplayed(CasinoNameHDR));
		
	}
	
	public void verifyLocationIconDisplayed()
	{
		By LocationIcon = By.xpath("//i[@class='location']");
		
		objPojo.getLogReporter().webLog(" Verify Location Icon is displayed ",
				objPojo.getWebActions().checkElementDisplayed(LocationIcon));
	}
	public void verifyCasinoAddressDisplayed()
	{
		By CasinoAddress = By.xpath("//a[contains(text(),'3 - 4 Coventry Street, London, England, W1D 6BL')]");
		objPojo.getLogReporter().webLog(" Verify Casino Address is displayed ",
				objPojo.getWebActions().checkElementDisplayed(CasinoAddress));
	}
	public void verifyOpeningTimeIconDisplayed()
	{
		By OpeningTimeIcon =  By.xpath("//i[@class='opening-time']");
		
		objPojo.getLogReporter().webLog(" Verify Opening Time Icon is displayed ",
				objPojo.getWebActions().checkElementDisplayed(OpeningTimeIcon));
	}
	
	public void verifyTelephoneButtonDisplayed()
	{
		By TelephoneButton = By.xpath("//button[@class='btn button telephone mobile-call']");
		objPojo.getLogReporter().webLog(" Verify Telephone Button is displayed ",
				objPojo.getWebActions().checkElementDisplayed(TelephoneButton));
	}
	public void verifyEmailButtonDisplayed()
	{
		By EmailButton = By.xpath("//button[@class='hidden-large hidden-middle btn button blue']");
		
		objPojo.getLogReporter().webLog(" Verify Email Button is displayed ",
				objPojo.getWebActions().checkElementDisplayed(EmailButton));
	}
	
	public void verifyTabsDisplayed(String tabname)
	{
		
	
		if(tabname.contains(","))
		{
			String[] arr1 = tabname.split(",");
			
			for (String tabname2 : arr1) 
			{
				
				By DescriptionTab = By.xpath("//a[contains(text(),'"+tabname2+"')]");
				objPojo.getLogReporter().webLog(" Verify "+tabname2+" is displayed on casino details page ",  
						 objPojo.getWebActions().checkElementDisplayed(DescriptionTab));
			
				objPojo.getLogReporter().webLog(" click on "+tabname2+" displayed on casino details page ",  
						 objPojo.getWebActions().javascriptClick(DescriptionTab));
				}
		}
	}
	
	public void verifyFacilitiesHdrDisplayed()
	{
		By FacilitiesHdr = By.xpath("//div[@class='component links-carousel hidden-exsmall hidden-small column-12-middle column-12-large']/h2[contains(text(),'Facilities')]");
		
		objPojo.getLogReporter().webLog(" Verify Facilities Header is displayed ",
				objPojo.getWebActions().checkElementDisplayed(FacilitiesHdr));
	}
	public void verifySocialMediaHdrDisplayed()
	{
		By SocialMediaHdr = By.xpath("//div[@class='row social']/h2[contains(text(),'Social Media')]");
		
		objPojo.getLogReporter().webLog(" Verify Social Media Header is displayed ",
				objPojo.getWebActions().checkElementDisplayed(SocialMediaHdr));
	}
	
	
	public void verifyFaceBookLinkDisplayed()
	{
		By FacebookLink = By.xpath("//div[@class='row social']/ul[@class='links-carousel-row list-unstyled first']/li/a[@class='facebook' and contains(text(),'Facebook')]");
		objPojo.getLogReporter().webLog(" Verify Facebook Link displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(FacebookLink));
	}
	

	public void verifyTwitterLinkDisplayed()
	{
		By TwitterLink = By.xpath("//div[@class='row social']/ul[@class='links-carousel-row list-unstyled first']/li/a[@class='twitter' and contains(text(),'Twitter')]");
		objPojo.getLogReporter().webLog(" Verify Twitter Link displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(TwitterLink));
	}
	
	public void verifyBarMenuLinkDisplayed()
	{
		By BarMenuLink = By.xpath("//div[@class='row']/div/ul[@class='links-carousel-row list-unstyled book-table-row']/li/a[contains(text(),'Bar Menu')]");
		
		
		objPojo.getLogReporter().webLog(" Verify Bar Menu Link displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(BarMenuLink));
	}
	
	
	public void verifyFacilitiesIconsDisplayed()
	{
		By glassIcon=By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i[@class='glass']");
		By FastFoodIcon = By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i[@class='fastfood']");
		
		By DisabledIcon=By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i[@class='disabled-icon']");
		
		By SlotsIcon=By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i[@class='slots']");
		
		By footballIcon=By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i[@class='football']");
		
		By smokingIcon=By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i[@class='smoking']");
		
		objPojo.getLogReporter().webLog(" Verify Glass icon is displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(glassIcon));
		objPojo.getLogReporter().webLog(" Verify Fast Food Icon is displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(FastFoodIcon));
		objPojo.getLogReporter().webLog(" Verify Disabled Icon is displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(DisabledIcon));
		objPojo.getLogReporter().webLog(" Verify Slots Icon is displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(SlotsIcon));
		objPojo.getLogReporter().webLog(" Verify football Icon is displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(footballIcon));
		objPojo.getLogReporter().webLog(" Verify smoking Icon is displayed under Social Media Header ",
				objPojo.getWebActions().checkElementDisplayed(smokingIcon));
		
		
	}
	
	public void verifyFacilitiesLabelsDisplayed(String labelTxt)
	{
		if(labelTxt.contains(","))
		{
			String[] arr1 = labelTxt.split(",");
			
			for (String lablTxt1 : arr1) 
			{
				By LabelLocator = By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i/span[@class='links-carousel-text' and contains(text(),'"+lablTxt1+"')]");
				
				objPojo.getLogReporter().webLog(" Verify Label displayed under Facilities is : " +lablTxt1 ,  
						 objPojo.getWebActions().checkElementDisplayed(LabelLocator));
				
			}
		}
		else
		{
			By locator = By.xpath("//div[@class='column-12-exsmall column-12-small column-12-middle column-12-large']/ul/li/i/span[@class='links-carousel-text' and contains(text(),'"+labelTxt+"')]");
			objPojo.getLogReporter().webLog(" Verify Label displayed under Facilities is : " +labelTxt ,  
					objPojo.getWebActions().checkElementDisplayed(locator));
		}
		
	}
	
	
	
	

}
