package com.generic.utils;

import com.generic.Pojo;
/**
 * Navigation to URL for AUT
 */
public class WebDomains 
{
	private Pojo objPojo;

	// Constructor
	public WebDomains(Pojo pojo){
		objPojo = pojo;
	}

	public void launchURLForGCWeb(){
		String environment = objPojo.getConfiguration().getConfig("Environment");
		switch(environment.toLowerCase()) {
		case "uat":
			this.launchURL(objPojo.getConfiguration().getConfig("uat.gc.URL"));
			break;
		}
	}

	public void launchURLForNeonWeb(){
		String environment = objPojo.getConfiguration().getConfig("Environment");
		switch(environment.toLowerCase()) {
		case "uat":
			this.launchURL(objPojo.getConfiguration().getConfig("uat.neon.URL"));
			break;
		}
	}
	
	public void launchURLForMailinator(){
		this.launchURL(objPojo.getConfiguration().getConfig("mailinator.URL"));
	}
 
	public void launchURL(String URL) {
		try { 
			objPojo.getWebDriverProvider().getDriver().get(URL);
			objPojo.getLogReporter().webLog("Get URL '" + URL + "'", true);
		}catch(Exception exception){
			objPojo.getLogReporter().webLog("Get URL '" + URL + "'", false);
		}
	}
	public void launchURLForPAMWeb(){
		String environment = objPojo.getConfiguration().getConfig("Environment");
		switch(environment.toLowerCase()) {
		case "uat":
			this.launchURL(objPojo.getConfiguration().getConfig("uat.PAM.URL"));
			break;
		}
	}
	
}