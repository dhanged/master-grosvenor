package com.generic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestRunner;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.generic.appiumDriver.AppiumDriverProvider;
import com.generic.appiumDriver.AppiumManager;
import com.generic.customAnnotation.TestDataSource;
import com.generic.logger.Log4JLogger;
import com.generic.testmanagementtools.APIClient;
import com.generic.utils.Configuration;
import com.generic.utils.CustomReporter;
import com.generic.utils.DBManager;
import com.generic.utils.DataPool;
import com.generic.utils.GenericUtils;
import com.generic.utils.NetworkMonitor;
import com.generic.utils.RunTimeDataHolder;
import com.generic.utils.WaitMethods;
import com.generic.utils.WebDomains;
import com.generic.webDriver.WebDriverProvider;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**
 *  Class will load all test data, load all objects, initialize web driver, start reports. 
 *  Contains generic functionalities like open browser 
 * 	
 * */
public class BaseTest extends Pojo 
{
	private long startTime;
	/**
	 * This method webdriver for web application 
	 */
	@Step("Initialize Web Environment")
	public void initializeWebEnvironment(Hashtable<String, String> dataSet, String... browser) 
	{
		try
		{
			if(getConfiguration() == null) {
				Configuration objConfiguration = new Configuration();
				objConfiguration.loadConfigProperties();
				super.setConfiguration(objConfiguration);
			}

			loadTestData(dataSet);

			NetworkMonitor networkMonitor = new NetworkMonitor();
			super.setNetworkMonitor(networkMonitor);
			if (getConfiguration().getConfig("browsermob.proxy").equalsIgnoreCase("true"))	
				getNetworkMonitor().startNetworkMonotorProxyServer();

			WebDriverProvider objWebDriverProvider = new WebDriverProvider();
			if(browser!= null && browser.length > 0)
				objWebDriverProvider.initialize(getConfiguration(), getNetworkMonitor(), browser[0]);
			else
				objWebDriverProvider.initialize(getConfiguration(), getNetworkMonitor());
			super.setWebDriverProvider(objWebDriverProvider);

			this.loadGetterSetter();
		

			super.getLogReporter().webLog("Web environment initialize successful", true);

		}
		catch(Exception exception){
			if(getConfiguration().getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			Assert.assertTrue(false);
			super.getLogReporter().webLog("Web environment initialize successful", false);
		}
	}

	/**
	 * @Description	: quit webdriver  
	 */
	@SuppressWarnings("unchecked")
	public void tearDownWebEnvironment(ITestResult iTestResult)
	{
		try
		{
			String status = "";
			if(getConfiguration().getConfigBooleanValue("powerBIReport")) 
			{ 
				status = "";
				if(iTestResult.getStatus() == ITestResult.SUCCESS)
					status = "Pass";
				if(iTestResult.getStatus() == ITestResult.FAILURE)
					status = "Fail";
				String browser = getConfiguration().getConfig("web.browser").trim().toLowerCase();
				String moduleName = getConfiguration().getConfig("ModuleName").trim();
				String testDescription = super.getTestCaseDescription();

				long endTime = System.currentTimeMillis();
				long executionTime = endTime - startTime;
				long exeutiontime = TimeUnit.MILLISECONDS.toMinutes(executionTime);
				String timeStamp = super.getTestExeuctionTimeStamp();
				String environment = getConfiguration().getConfig("Environment").trim();
				String build = getConfiguration().getConfig("Build").trim();

				Connection con = DBManager.getDatabaseConnection(getConfiguration().getConfig("powerBI.DataBaseDriver").trim(), 
						getConfiguration().getConfig("powerBI.DataBaseURL").trim(), 
						getConfiguration().getConfig("powerBI.DataBaseUserName").trim(), 
						getConfiguration().getConfig("powerBI.DataBasePassword").trim());

				con.createStatement();
				String SQL = "INSERT INTO Report(BrowserName,ModuleName ,TestDesription ,Status, ExecutionTime,ExecutionDateTime,ENV,Build)" + 
						" VALUES('" + browser + "','" + moduleName + "','" + testDescription +"','" + status + "','" + exeutiontime + "','" + timeStamp + "','" + environment + "','" + build + "')";
				PreparedStatement pstmt = con.prepareStatement(SQL); 
				pstmt.executeUpdate();

			}
			super.getWebDriverProvider().tearDown();
			super.getLogReporter().webLog("Web environment teardown successful", true);

			if(getConfiguration().getConfigBooleanValue("testRailResultUpdate")) 
			{
				APIClient client = new APIClient("https://expleostepin.testrail.io");
				client.setUser(getConfiguration().getConfig("testRail.UserName").trim());
				client.setPassword(getConfiguration().getConfig("testRail.Password").trim());

				@SuppressWarnings("rawtypes")
				Map data = new HashMap();
				if(status.equalsIgnoreCase("Pass")) {
					Integer pass = Integer.valueOf(1);
					data.put("status_id", pass);
				}
				if(status.equalsIgnoreCase("Fail")) {
					Integer fail = Integer.valueOf(5);
					data.put("status_id", fail);
				}
				data.put("comment", "Test get executed successfully!");
				@SuppressWarnings("unused")
				JSONObject jSONObject = (JSONObject) client.sendPost("add_result_for_case/" + getTestCaseID() , data);
			}
		}
		catch(Exception exception){
			if(getConfiguration().getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			super.getLogReporter().webLog("Web environment teardown successful", false);

		}
		finally {
			if (getConfiguration().getConfig("browsermob.proxy").equalsIgnoreCase("true"))	
				getNetworkMonitor().stopNetworkMonotorProxyServer();
			if (getConfiguration().getConfig("test.custom.reporter").equalsIgnoreCase("true")) 
				super.getCustomReporter().endReport();
		}
	}

	/**
	 * This method appium driver for mobile application 
	 */
	public void initializeMobileEnvironment(Hashtable<String, String> dataSet)
	{
		try
		{ 
			if(getConfiguration() == null) {
				Configuration objConfiguration = new Configuration();
				objConfiguration.loadConfigProperties();
				super.setConfiguration(objConfiguration);
			}

			loadTestData(dataSet);

			AppiumDriverProvider objAppiumDriverProvider = new AppiumDriverProvider();
			if(System.getProperty("os.name").trim().toLowerCase().contains("windows"))
			{
				if(getConfiguration().getConfigBooleanValue("desktop.appium")) {
					objAppiumDriverProvider.initializeDesktopAppium("android", getConfiguration());
					this.setAppiumDriverProvider(objAppiumDriverProvider);
				}
				else {
					AppiumManager objAppiumManager = new AppiumManager();
					this.setAppiumManager(objAppiumManager);
					objAppiumManager.startAppium();

					objAppiumDriverProvider.initializeConsoleBaseAppium("android", getConfiguration(), objAppiumManager.getCurrentAppiumIpAddress(), objAppiumManager.getCurrentAppiumPort());
					this.setAppiumDriverProvider(objAppiumDriverProvider);
				}
			}
			else if(System.getProperty("os.name").trim().toLowerCase().contains("mac"))
			{
				objAppiumDriverProvider.initializeDesktopAppium("ios", getConfiguration());
				this.setAppiumDriverProvider(objAppiumDriverProvider);
			}

			this.loadGetterSetter();

			super.getLogReporter().mobileLog("Mobile environment initialize successful", true);
		}
		catch(Exception exception){
			if(getConfiguration().getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			Assert.assertTrue(false);
			super.getLogReporter().mobileLog("Mobile environment initialize successful", false);
		}
	}

	private void loadGetterSetter() 
	{
		Log4JLogger objLog4JLogger = new Log4JLogger();
		super.setLog4JLogger(objLog4JLogger);
		super.getLog4JLogger().initializeLogger();

		WaitMethods objWaitMethods = new WaitMethods();
		super.setWaitMethods(objWaitMethods);

		LogReporter objLogReporter = new LogReporter(this);
		super.setLogReporter(objLogReporter);

		WebActions objWebActions = new WebActions(this);
		super.setWebActions(objWebActions);

		MobileActions objMobileActions = new MobileActions(this);
		super.setMobileActions(objMobileActions);

		RunTimeDataHolder objRunTimeDataHolder = new RunTimeDataHolder();
		super.setRunTimeDataHolder(objRunTimeDataHolder);

		WebDomains objWebDomains = new WebDomains(this);
		super.setWebDomains(objWebDomains);

		if(getCustomReporter() == null) {
			if (getConfiguration().getConfig("test.custom.reporter").equalsIgnoreCase("true")){
				CustomReporter objCustomReporter = new CustomReporter();
				objCustomReporter.startReport(super.getTestCaseID(), getSuiteName());
				super.setCustomReporter(objCustomReporter);
			}
		}
	}

	/**
	 * @Description	: quit webdriver  
	 */
	public void tearDownMobileEnvironment()
	{
		try
		{
			getAppiumDriverProvider().tearDown();
			super.getLogReporter().mobileLog("Mobile environment teardown successful", true);
		}
		catch(Exception exception){
			if(getConfiguration().getConfigBooleanValue("printStackTraceForActions"))
				exception.printStackTrace();
			super.getLogReporter().mobileLog("Mobile environment teardown successful", false);

		}
		finally {
			if(getConfiguration().getConfig("appium.log").equalsIgnoreCase("true"))
				GenericUtils.writeAppiumLogs(getAppiumDriverProvider().getAppumLogEntries());
			if(System.getProperty("os.name").trim().toLowerCase().contains("windows")){
				if(!getConfiguration().getConfigBooleanValue("desktop.appium"))
					getAppiumManager().stopServer();
			}
		}
	}

	/** 
	 * return runtime object of page or view
	 * 
	 */
	public <T> T PageObjectManager(Class<T> cls) {
		try {
			Constructor<T> constructor = cls.getConstructor(new Class[] { Pojo.class });
			return (T) constructor.newInstance(new Object[] { this });
		} catch (  InstantiationException | IllegalAccessException | InvocationTargetException | IllegalArgumentException
				| SecurityException | NoSuchMethodException exception) {
			exception.printStackTrace();
		}
		return null;
	}

	/** 
	 * testng data provider
	 * 
	 */
	@DataProvider(name = "TestDataProvider")
	public Object[][] getDataProvider(Method method, ITestContext context) 
	{
		if(getConfiguration() == null) {
			Configuration objConfiguration = new Configuration();
			objConfiguration.loadConfigProperties();
			super.setConfiguration(objConfiguration);
		}
		startTime = System.currentTimeMillis();
		String textExecutionDate = GenericUtils.getRequiredDay("0", "YYYY-MM-dd", null);
		String textExecutionTime = GenericUtils.getRequiredDay("0", "hh:mm:ss", null);
		String textExecutionTimeStamp = textExecutionDate + "T" + textExecutionTime;
		super.setTestExeuctionTimeStamp(textExecutionTimeStamp);

		Object[][] testData = null;
		//String testCaseID = ((TestDataSource) method.getAnnotation(TestDataSource.class)).testCaseID();
		//super.setTestCaseID(testCaseID);

		String dataSource = ((TestDataSource) method.getAnnotation(TestDataSource.class)).dataSource();
		String testDataFilePath = System.getProperty("user.dir") + "/src/test/testData/" +
				((dataSource.substring(0,1).equals("/") || dataSource.substring(0,1).equals("\\")) 
						? dataSource.substring(1) : dataSource) 
				+ ".xlsx";
		String pathUptoFolder = testDataFilePath.substring(0, testDataFilePath.lastIndexOf("/"));
		String pathAfterFolder = testDataFilePath.substring(testDataFilePath.lastIndexOf("/"));
	 	String environment = getConfiguration().getConfig("Environment");
		switch(environment.toLowerCase()) {
		case "uat":
			testDataFilePath = pathUptoFolder + "/uat" + pathAfterFolder;
			break;
		case "live":
			testDataFilePath = pathUptoFolder + "/live" + pathAfterFolder;
			break;
		}
		super.setTestDataFilePath(testDataFilePath);

		String testCaseTitle = ((TestCaseId) method.getAnnotation(TestCaseId.class)).value();
		super.setTestCaseTitle(testCaseTitle);
		String testCaseID = testCaseTitle;
		super.setTestCaseID(testCaseID);
		String testCaseDescription = ((Description) method.getAnnotation(Description.class)).value();
		super.setTestCaseDescription(testCaseDescription);

		System.out.println("********* Executing test case - " + testCaseID + " - " + testCaseTitle + " - " + testCaseDescription );
		System.out.println("********* Loading test data for test case");
		if (!testDataFilePath.equals("") && !testCaseID.equals("")) 
			testData = new DataPool().loadTestData(testCaseID, testDataFilePath);

		if(testData == null) {
			System.out.println("********* Unable to load test data for test case please check missing filed \n following may be chances \n - TestCase id mismatch to excel test data row \n - forgot to add test data row");
			return null;
		}
		else
			return testData;
	}

	/**
	 * @Method : loadTestData
	 * @param : dataSet - test data hash table
	 * @Description : Load data from excel for the running testCase and return
	 *              as Object array
	 */
	public void loadTestData(Hashtable<String, String> dataSet) {
		super.setDataPoolHashTable(dataSet);
	}

	@BeforeSuite(alwaysRun = true)
	public void setup(ITestContext ctx) {
		System.out.println("*** Setting test output directory for testNG");
		TestRunner runner = (TestRunner) ctx;
		runner.setOutputDirectory(System.getProperty("user.dir") + "/target/test-output");
	}

	@BeforeMethod(alwaysRun = true)
	public void setsuitename(ITestContext ctx) {
		super.setSuiteName(ctx.getSuite().getName());
	}
}