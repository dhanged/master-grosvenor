package com.generic.webDriver;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.generic.interfaces._WebDriverCreation;
import com.generic.utils.Configuration;
import com.generic.utils.NetworkMonitor;

import net.lightbody.bmp.client.ClientUtil;
/**
 * used to initiate selenium webdriver for automation  
 */
public class WebDriverFactory implements _WebDriverCreation
{
	public WebDriver setWebDriver(Configuration objConfiguration, NetworkMonitor networkMonitor) throws Exception
	{
		WebDriver webDriver;
		String browser = objConfiguration.getConfig("web.browser").trim().toLowerCase();
		boolean executionDocker = objConfiguration.getConfigBooleanValue("execution.docker");

		if(executionDocker) {
			switch (browser)
			{ 

			case "chrome":   
				DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
				desiredCapabilities.setBrowserName("chrome");
				String dockeURL = objConfiguration.getConfig("docker.URL").trim().toLowerCase();
				webDriver = new RemoteWebDriver(new URL(dockeURL), desiredCapabilities);
				break;

			default:
				DesiredCapabilities desiredCapabilitiesDefault = DesiredCapabilities.chrome();
				desiredCapabilitiesDefault.setBrowserName("chrome");
				String dockeURLDefault = objConfiguration.getConfig("docker.URL").trim().toLowerCase();
				webDriver = new RemoteWebDriver(new URL(dockeURLDefault), desiredCapabilitiesDefault);
				break;
			}
		}
		else 
		{
			switch (browser)
			{ 
			case "ie":
				webDriver = new InternetExplorerDriver(this.internetExplorerOptions(objConfiguration));
				break;

			case "chrome":   
			/*	DesiredCapabilities cap = DesiredCapabilities.chrome();
			 	LoggingPreferences logPrefs = new LoggingPreferences();
			 	logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
			 	cap.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			 	cap.setCapability(ChromeOptions.CAPABILITY, this.chromeOptions(objConfiguration, networkMonitor));
			 	*/
				webDriver = new ChromeDriver(this.chromeOptions(objConfiguration, networkMonitor));
				break;

			default:
				webDriver = new ChromeDriver(this.chromeOptions(objConfiguration, networkMonitor));
			}
		}
		return webDriver;
	} 

	public WebDriver setWebDriver(Configuration objConfiguration, NetworkMonitor networkMonitor, String browser) throws Exception
	{
		WebDriver webDriver;
		switch (browser)
		{ 
		case "ie":
			webDriver = new InternetExplorerDriver(this.internetExplorerOptions(objConfiguration));
			break;

		case "chrome":   
			webDriver = new ChromeDriver(this.chromeOptions(objConfiguration, networkMonitor));
			break;

		default:
			webDriver = new ChromeDriver(this.chromeOptions(objConfiguration, networkMonitor));
			
		}

		return webDriver;
	} 

	private InternetExplorerOptions internetExplorerOptions(Configuration objConfiguration) throws IOException
	{
		System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "/src/main/resources/drivers/IEDriverServer/IEDriverServer.exe");

		InternetExplorerOptions options = new InternetExplorerOptions();
		options.setCapability( "ignoreZoomSetting", true ); //$NON-NLS-1$
		options.setCapability( InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true );
		options.setCapability( "requireWindowFocus", false ); //$NON-NLS-1$
		options.setCapability( "ie.usePerProcessProxy", true ); //$NON-NLS-1$
		options.setCapability( "ie.ensureCleanSession", true ); //$NON-NLS-1$
		return options;
	}

	private ChromeOptions chromeOptions(Configuration objConfiguration, NetworkMonitor networkMonitor) throws IOException
	{
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/drivers/chromedriver_win32/chromedriver.exe");		
		ChromeOptions options = new ChromeOptions(); 
		options.setAcceptInsecureCerts(true);

		if (objConfiguration.getConfig("browsermob.proxy").equalsIgnoreCase("true")) {
			Proxy seleniumProxy = ClientUtil.createSeleniumProxy(networkMonitor.getNetworkMonitorProxy());
			options.setProxy(seleniumProxy);	
			//options.setAcceptInsecureCerts(true); 
		}
		options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); 
		Map<String, Object> prefs = new HashMap<>();
		options.addArguments( "--test-type" ); //$NON-NLS-1$
		options.addArguments( "--disable-popup-blocking" ); //$NON-NLS-1$
		options.addArguments( "--disable-extensions" ); //$NON-NLS-1$
		options.addArguments( "--disable-infobars" ); //$NON-NLS-1$
		options.setExperimentalOption( "prefs", prefs ); //$NON-NLS-1$
		options.setCapability( "chrome.switches", //$NON-NLS-1$
				Arrays.asList( "--ignore-certificate-errors", "--disable-translate", "-no-proxy-server=" ) ); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		options.setBinary(System.getProperty("user.dir") + "/src/main/resources/browsers/Chrome/Application/chrome.exe");
		
		/*
		 * Map<String, Object> perfLogPrefs = new HashMap<String, Object>();
		 * perfLogPrefs.put("traceCategories", "browser,devtools.timeline,devtools");
		 * perfLogPrefs.put("enableNetwork", true);
		 * options.setExperimentalOption("perfLoggingPrefs", perfLogPrefs);
		 */
	 	 	
 		return options;
	}
}