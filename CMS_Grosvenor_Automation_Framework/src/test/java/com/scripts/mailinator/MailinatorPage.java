package com.scripts.mailinator;

import org.openqa.selenium.By;

import com.generic.Pojo;
public class MailinatorPage {

	// Local variables
	private Pojo objPojo;

	private By inpInboxName = By.xpath("//input[@aria-label='Enter Inbox Name']");
	private By btnGO = By.xpath("//button[@aria-label='Go to public']");	
	private By logoMailinator = By.xpath("//div[@class='nav-title' and normalize-space()='Mailinator']");
	private By hdrInbox = By.xpath("//div[@class='ng-binding' and contains(.,'inbox')]");
  	 	
	public MailinatorPage(Pojo pojo){
		this.objPojo = pojo;
	}

	public void verifyMailinatorLogoDisplayed(){
		objPojo.getLogReporter().webLog("Verify Mailinator logo displayed.", 
				objPojo.getWebActions().checkElementDisplayed(logoMailinator));
 	}
	
	public void setInboxName(String emailAddress) {
 		objPojo.getLogReporter().webLog("Enter Public Mailinator Inbox ", emailAddress,
				objPojo.getWebActions().setText(inpInboxName, emailAddress));
	}

	public void clickGO() {
 		objPojo.getLogReporter().webLog("Click 'GO' button", 
				objPojo.getWebActions().click(btnGO));
	}
	
	public void verifyInboxDisplayed(){
		objPojo.getLogReporter().webLog("Verify Inbox displayed.", 
				objPojo.getWebActions().checkElementDisplayed(hdrInbox));
 	}

	public void verifyForgotUserNameMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Did you forget your Grosvenor Casinos username?']");
		objPojo.getLogReporter().webLog("Verify forgot username mail received.", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	} 
	
	public void verifyForgotPasswordMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Did you forget your Grosvenor Casinos password?']");
		objPojo.getLogReporter().webLog("Verify forgot password mail received.", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	} 
	
 	public void openForgotPasswordMail() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Did you forget your Grosvenor Casinos password?']");
		objPojo.getLogReporter().webLog("Open forgot password email.", 
				objPojo.getWebActions().click(locator));
	} 
	
	public void clickResetPasswordButtonFromMail() {
		By locator = By.xpath("//a[text()='Reset Password']");
		objPojo.getWebActions().javascriptScrollIntoView(locator);
	 	objPojo.getLogReporter().webLog("Click 'Reset Password' button.", 
				objPojo.getWebActions().click(locator));
	}
	
	public void verifyHereAreYourMembershipCardDetailsMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='Here are your membership card details']");
		objPojo.getLogReporter().webLog("Verify forgot username mail received.", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	} 
	
	public void verifyYouRequestedPINResetMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='You requested a PIN reset for GrosvenorCasinos.com']");
		objPojo.getLogReporter().webLog("Verify forgot password mail received.", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	} 
	
	public void openPINResetMail() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[normalize-space()='You requested a PIN reset for GrosvenorCasinos.com']");
		objPojo.getLogReporter().webLog("Open PIN Reset email.", 
				objPojo.getWebActions().click(locator));
	} 
	
	public void clickChangeYourPINLinkFromMail() {
		By locator = By.xpath("//font[contains(.,'to change your PIN')]/a[contains(.,'click')]");
		objPojo.getWebActions().javascriptScrollIntoView(locator);
	 	objPojo.getLogReporter().webLog("Click 'Click here to change your PIN' link.", 
				objPojo.getWebActions().click(locator));
	}
	
	public void verifyRequestedLoginCodeMailReceived() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[contains(text(),'requested a Login Code to access GrosvenorCasinos.com')]");
		objPojo.getLogReporter().webLog("Verify 'You've requested a Login Code to access GrosvenorCasinos.com' mail received.", 
				objPojo.getWebActions().checkElementDisplayed(locator));
	} 
	
	public void openRequestedLoginCodeMail() {
		By locator = By.xpath("//table[@class='table table-striped jambo_table']/tbody/tr[1]/td[4]/a[contains(text(),'requested a Login Code to access GrosvenorCasinos.com')]");
		objPojo.getLogReporter().webLog("Open requested a Login Code mail.", 
				objPojo.getWebActions().click(locator));
	} 
	
	public String getLoginCode() {
		By locator = By.xpath("//p[contains(.,'Login') and contains(.,'code')]");
		return objPojo.getWebActions().getText(locator).split(":")[1].trim();
	}
	 
	
}