package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 To check SAW user login
 */
public class Test5_LoginFlogMembershipCardNumber_SAWCustomer extends BaseTest 
{	 
	@TestCaseId("Test5")
	@Description("Login - FLOG - Digital Only Customer - FLOG Membership Number_Check system send username "
			+ "on registrated Email Address when user selects Forgotten membership Number Option form Login Page.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void test5_LoginFlogUserName_SAWCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
 		PageObjectManager(LoginLogoutView.class).verifyForgottenUsernameFuntionalityForSAWCustomer(
				dataPool("EmailAddress"));
 	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}