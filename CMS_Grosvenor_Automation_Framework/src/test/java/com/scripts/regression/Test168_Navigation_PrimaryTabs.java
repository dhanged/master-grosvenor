
package com.scripts.regression;


import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.Footer;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.PageValidation;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test168_Navigation_PrimaryTabs extends BaseTest {
	
	@TestCaseId("Test168")
	@Description("Check Primary Navigation ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc168_Navigation_PrimaryTabs(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(PageValidation.class).verifyPrimaryTabs("Live Casino ,Slots & Games,Jackpots,Table Games,Sport,Poker,Promos,Grosvenor One,Local Casinos");	
		PageObjectManager(PageValidation.class).verifySerachFinderInHeader();
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();
		
		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Most Popular~seven:Slots & Games~tux:Live Casino~money:Jackpots~roulette:Electronic Roulette~grosvenor-one-goals:Virtuals~chip-solid:Table Games~seven:Originals~star:Classic Slots");
		PageObjectManager(PageValidation.class).verifyGamesSection("star:Most Popular~seven:Slots & Games~tux:Live Casino~money:Jackpots~roulette:Electronic Roulette~grosvenor-one-goals:Virtuals~chip-solid:Table Games~seven:Originals~star:Classic Slots");
		PageObjectManager(PageValidation.class).verifySectionLinks("Most Popular:View all~Slots & Games:More Slots~Live Casino:More Tables~Jackpots:More Jackpots~Electronic Roulette:View more~Virtuals:More games~Table Games:More games~Originals:More Originals~Classic Slots:All Classic Slots");
		
	}

	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

}
