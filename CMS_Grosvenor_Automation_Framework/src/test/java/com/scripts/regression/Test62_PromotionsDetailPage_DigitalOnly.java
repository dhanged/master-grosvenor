package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.PromotionsPage;
import com.sun.tools.xjc.Driver;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test62_PromotionsDetailPage_DigitalOnly extends BaseTest {
	
	
	@TestCaseId("Test62")
	@Description("check that system displays promotions details page with all the required details." 
				+ "Check that system displays the Active and Expiry date for the specific promotion on details page."
				+ "Check that system allows to claim/Opt-In any promotions from promotion details page")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	
	public void test62_PromotionsDetailPage_DigitalOnly(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("Password"));
		PageObjectManager(MainPage.class).clickCloseX();
		PageObjectManager(PromotionsPage.class).navigateToPromosTAB();
		PageObjectManager(PromotionsPage.class).verifySecondaryLinksDisplayed("All;My Promotions");
		
		PageObjectManager(PromotionsPage.class).findPromoBTN("Opt-in");
		PageObjectManager(PromotionsPage.class).clickFollowingMoreInfoLink("Opt-in");
		PageObjectManager(PromotionsPage.class).validateActiveExpiryDate();
		PageObjectManager(PromotionsPage.class).clickClaimBTN();
		PageObjectManager(PromotionsPage.class).verifyOptedInTXTDisplayed();
		PageObjectManager(PromotionsPage.class).verifyOptOutBTNDisplayed();

		/*PageObjectManager(PromotionsPage.class).findPromoBTN("Claim");
		PageObjectManager(PromotionsPage.class).clickFollowingMoreInfoLink("Claim");
		PageObjectManager(PromotionsPage.class).validateActiveExpiryDate();
		PageObjectManager(PromotionsPage.class).clickClaimBTN();
		PageObjectManager(PromotionsPage.class).verifyavailableCashLabeldisplayed();
		String availableCash = PageObjectManager(PromotionsPage.class).getAvailableCashAmount() ;
		
		String BuyInForCash = PageObjectManager(PromotionsPage.class).getBuyInForCashAmount() ;
		String BuyInBonusAmount = PageObjectManager(PromotionsPage.class).getBuyInBonusAmount() ;
		String PlayWithAmount =  PageObjectManager(PromotionsPage.class).getPlayWithAmount() ;
		System.out.println(" availableCash : " + availableCash + " BuyInForCash : " + BuyInForCash+" BuyInBonusAmount: "+BuyInBonusAmount + "PlayWithAmount : " +PlayWithAmount);
		
		
		PageObjectManager(PromotionsPage.class).verifyBuyInBTNdisplayed();
		PageObjectManager(PromotionsPage.class).verifyCancelBTNdisplayed();
		PageObjectManager(PromotionsPage.class).clickBuyInBTN();
		
		PageObjectManager(PromotionsPage.class).verifyConfirmBuyingInHDRDisplayed();
		PageObjectManager(PromotionsPage.class).verifyOKBTNonPopup();
		PageObjectManager(PromotionsPage.class).verifyCancelBTNonPopup();
		PageObjectManager(PromotionsPage.class).clickOKBTNonPopup();
		
		PageObjectManager(PromotionsPage.class).verifyBuyInSuccessMsgDisplayed();
		PageObjectManager(PromotionsPage.class).validateReceivedAmount(PlayWithAmount );
		
		PageObjectManager(PromotionsPage.class).verifyBuyInDetailsOnPopup();
		
		PageObjectManager(PromotionsPage.class).validateBonusDetailOnPopup(availableCash,BuyInForCash,BuyInBonusAmount);
*/
		
		
	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	//	tearDownWebEnvironment(iTestResult); 
	}
	
	

}
