package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.MainPage;
import com.view.RegistrationView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 To check digital registration
 */
public class Test7_Registration_DigitalOnlyCustomer extends BaseTest 
{	 
	@TestCaseId("Test7")
	@Description("Registration - Digital Only Customer - Digital Registration_Check whether user gets successfully "
			+ "registered to site when user enters correct data in all fields.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc7_Registration_DigitalOnlyCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		String userName = "opst" + GenericUtils.getRandomAlphanumericString(8);
		String password = "Test" + GenericUtils.getRandomNumeric(4);
		PageObjectManager(RegistrationView.class).newUserRegistration(GenericUtils.getRandomAlphanumericEmailString(10, "@mailinator.com"), 
				userName, password, GenericUtils.getRandomTitleForUser(), 
				GenericUtils.getRandomAlphabeticString(8), GenericUtils.getRandomAlphabeticString(8), 
				GenericUtils.getRequiredDateWithCustomYear("-20", "DD_MM_YYYY", ""), dataPool("Country"), 
				GenericUtils.getRandomAlphabeticString(8), GenericUtils.getRandomAlphabeticString(8), GenericUtils.getRandomAlphabeticString(8), 
				dataPool("Postcode"), ("077009" + GenericUtils.getRandomNumeric(5)), dataPool("AppNotificationPreferences"));
		updateTestData("UserName", userName);
		updateTestData("Password", password);
  	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}