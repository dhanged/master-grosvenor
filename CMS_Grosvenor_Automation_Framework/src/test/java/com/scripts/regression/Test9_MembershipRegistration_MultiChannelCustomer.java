package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.neonretail.MainPage;
import com.view.NeonView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 	To check retail registration
 */
public class Test9_MembershipRegistration_MultiChannelCustomer extends BaseTest 
{	 
	@TestCaseId("Test9")
	@Description("Membership Registration -  MultiChannel Customer - Retail Registration_Check whether system displays correct "
			+ "details for entered membership number and DOB on registration page.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc9_Registration_DigitalOnlyCustomer_Deposit(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet, "ie");
		getWebDomains().launchURLForNeonWeb();
		PageObjectManager(NeonView.class).loginToNeon(dataPool("NeonUserName"), dataPool("NeonPassword"));
		PageObjectManager(MainPage.class).navigateThroughMenu("Customers", "Registration");
	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}