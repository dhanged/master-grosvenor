package com.scripts.regression;
import java.util.Hashtable;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.AccountDetailsView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test104_AccountDetails_UpdateMarketingPreferences extends BaseTest {
	
	@TestCaseId("Test104")
	@Description("Account Details_Check whether user able to Update Marketing Preferences without any issues")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc104_AccountDetails_UpdateMarketingPreferences(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(AccountDetailsView.class).AccountDetails_UpdateMarketingPreferences_Digital(dataPool("UserName"), dataPool("Password"),dataPool("MarketingPreferences"));
  	}
	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	tearDownWebEnvironment(iTestResult);
	}
	
	
	

}
