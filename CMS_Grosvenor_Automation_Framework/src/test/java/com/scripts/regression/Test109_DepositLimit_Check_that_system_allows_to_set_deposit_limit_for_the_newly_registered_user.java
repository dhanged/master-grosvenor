package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.MainPage;
import com.view.RegistrationView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test109_DepositLimit_Check_that_system_allows_to_set_deposit_limit_for_the_newly_registered_user extends BaseTest {


	@TestCaseId("Test109")
	@Description("Deposit Limit_Check that system allows to set deposit limit for the newly registered user.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web","Compliance"})
	public void tc109_RegisterUserWithDepositLimit(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet);
		getWebDomains().launchURLForGCWeb();

		getWaitMethods().sleep(9);
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		String userName = "GC_Aut" + GenericUtils.getRandomAlphanumericString(4);
		//	String password = "RankGroup" + GenericUtils.getRandomNumeric(2);
		String password = "RankGroup01";

		PageObjectManager(RegistrationView.class).newUserRegisterWithDepositLimit(GenericUtils.getRandomAlphanumericEmailString(10, "@mailinator.com"), 
				userName, dataPool("Password"), GenericUtils.getRandomTitleForUser(), 
				GenericUtils.getRandomAlphabeticString(8), GenericUtils.getRandomAlphabeticString(8), 
				GenericUtils.getRequiredDateWithCustomYear("-20","dd_MM_YYYY", ""), dataPool("Country"), 
				GenericUtils.getRandomAlphabeticString(8), GenericUtils.getRandomAlphabeticString(8), GenericUtils.getRandomAlphabeticString(8), 
				dataPool("Postcode"), ("077009" + GenericUtils.getRandomNumeric(5)), dataPool("DailyDepositLimit"), dataPool("WeeklyDepositLimit"), dataPool("MonthlyDepositLimit"));
		updateTestData("UserName", userName);


	}


	@AfterMethod(groups = {"Web","Compliance"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}
