
package com.scripts.regression;

import com.generic.BaseTest;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.generic.customAnnotation.TestDataSource;

import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.PageValidation;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test177_Navigation_SecondaryTabs extends BaseTest {

	@TestCaseId("Test177")
	@Description("Check Secondary Navigation ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"},priority=1)
	public void tc177_Navigation_SecondaryTabs(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();

		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		
		//Live Casino
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Live Casino");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Host Offers,Live Roulette,Live Blackjack,Live Baccarat,Live Casino Poker,Game Shows,Grosvenor Exclusive");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Live Casino");
		
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();
		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Top Picks~tux:Most Popular~roulette:Live Roulette~hearts:Live Blackjack~chip-solid:Game Show~token:Live Baccarat~tux:More Games~medal:VIP");
		PageObjectManager(PageValidation.class).verifySectionLinks("Live Roulette:View More~Live Blackjack:View More~Live Baccarat:View All");


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Host Offers");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Host Offers");
		
		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Roulette");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Roulette");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Blackjack");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Blackjack");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Baccarat");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Baccarat");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Casino Poker");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Casino Poker");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Game Shows");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Game Shows");
		PageObjectManager(PageValidation.class).verifyGamesOnGameshowsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Grosvenor Exclusive");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Grosvenor Exclusive");
		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("tux:Exclusive Studio Tables~tux:Exclusively from Grosvenor Victoria Casino");
		PageObjectManager(PageValidation.class).verifyGamesOnGameshowsPage();
			}

	@Test(priority=2,groups ={"Web"})
	public void verifySlotsandGamesPageNavigation()
	{
		//Slots & Games
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Slots & Games");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Slots & Games");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("All Slots,Featured,Originals,New,Top 20,in Club,Classic Slots");
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();


		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:New & Exclusive~seven:Top Games~medal:Jackpots~seven:Originals~star:Classic Slots");
		PageObjectManager(PageValidation.class).verifySectionLinks("New & Exclusive:More New Games~Top Games:More Top Games~Jackpots:More Jackpots~Originals:More Originals~Classic Slots:All Classic Slots");


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("All Slots");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("All Slots");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Featured");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Featured");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Originals");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Originals");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("New");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("New");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Top 20");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Top 20");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("in Club");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("in Club");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Classic Slots");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Classic Slots");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

	}


	@Test(priority=3,groups ={"Web"})
	public void verifyJackpotsPageNavigation()
	{
		//Jackpots
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Jackpots");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Jackpots");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Daily Jackpots,Jackpot King,Mega Jackpots,All Jackpots,Jackpots at Grosvenor");
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();


		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Hot Jackpots~trophy-star:Daily Jackpots~trophy-star:Jackpot King~trophy-star:Mega Jackpots");
		PageObjectManager(PageValidation.class).verifySectionLinks("Hot Jackpots:View All~Daily Jackpots:View All~Jackpot King:View All~Mega Jackpots:View All");
		
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("h1;Jackpot Slots~h2;Daily Jackpots~h2;Must Go jackpots~h2;King Jackpots~h2;Jackpot King~h2;Mega Jackpots");


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Daily Jackpots");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Daily Jackpots");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Jackpot King");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Jackpot King");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Mega Jackpots");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Mega Jackpots");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("All Jackpots");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("All Jackpots");
		PageObjectManager(PageValidation.class).verifySortByText();
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Jackpots at Grosvenor");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Jackpots at Grosvenor");
		//PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		
	}

	@Test(priority=4,groups ={"Web"})
	public void verifyTableGamesPageNavigation()
	{
		//Table Games
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Table Games");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Table Games");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Top Picks,Roulette,Blackjack,Baccarat,Video Poker,All");
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();


		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Top Picks~roulette:Roulette~hearts:Blackjack~tux:Live Tables~chip-solid:Other Games");
		PageObjectManager(PageValidation.class).verifySectionLinks("Top Picks:View All~Roulette:View All~Blackjack:View More~Live Tables:View More~Other Games:View All");
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("h1;Play Your Favourite Table Games with Grosvenor");


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Top Picks");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Top Picks");
		//PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Roulette");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Roulette");
		//PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Blackjack");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Blackjack");
		//PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Baccarat");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Baccarat");
		//PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Video Poker");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Video Poker");
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("h1;Video Poker Games");

		//PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("All");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("All");
		//PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

	}

	
	@Test(priority=5,groups ={"Web"})
	public void verifyPokerPageNavigation()
	{
		//Poker
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Poker");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Poker");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Instant Play,Promotions,Sponsored Players,Loyalty Scheme,Live Events,Poker Blog,Live Stream,National League");
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("h1;Welcome to Grosvenor Poker~h2;Play Live & Online Poker with Grosvenor~h2;Grosvenor Poker Online~h2;Online Poker Bonuses~h2;Learn to Play Poker from the Pros~h2;Join our Poker Tournaments~h2;Poker Professionals & Poker Loyalty Scheme");
		//PageObjectManager(LiveCasinosPage.class).verifyHomepageSectionHeadersdisplayed("hearts:Featured Poker Promotions");


		//PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Instant Play");
		//PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Instant Play");
		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Promotions");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Promotions");
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("h2;Grosvenor Poker Exclusive Promotions & Offers");

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Sponsored Players");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Sponsored Players");
		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Loyalty Scheme");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Loyalty Scheme");
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("h1;Grosvenor Casinos Poker - Loyalty Scheme~h2;General Terms and Conditions");

		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Events");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Events");
		
		//PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Poker Blog");
		//PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Poker Blog");
		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Stream");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Stream");
		
		//PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("National League");
		//PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("National League");
		
	}

	
/*	@Test(priority=6,groups = {"Web"})
	public void verifyPromosPageNavigation()
	{
		//promos
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Promos");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("All");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Promos");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("All");
		PageObjectManager(PageValidation.class).verifyEnterBonusCodeFieldOnPromotionPage();
		PageObjectManager(PageValidation.class).verifyBonusHistoryLinkOnPromotionPage();
		PageObjectManager(PageValidation.class).verifyPromoetionsONPromosPage();
	}
*/
 
	
	@Test(priority=7,groups ={"Web"})
	public void verifyGrosvenorOnePageNavigation()
	{
		//Grosvenor One
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Grosvenor One");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Grosvenor One");
		//PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Grosvenor One,Rewards,Welcome Offers");
		PageObjectManager(PageValidation.class).verifySectionsOnG1("Grosvenor One,Rewards,Welcome Offers");
		PageObjectManager(PageValidation.class).verifyGrosvenorOneCasinoscardiMG();
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("div;Why join Grosvenor One?~div;Single wallet you can use in-casino and online~div; Top up your online account in-casino~div;Deposit and withdraw cash in any casino~div;Use at the cash desk, on slots and electronic roulette");


		//PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Top Picks~roulette:Roulette~hearts:Blackjack~tux:Live Tables~chip-solid:Other Games");
		//PageObjectManager(PageValidation.class).verifySectionLinks("Top Picks:View All~Roulette:View All~Blackjack:View More~Live Tables:View More~Other Games:View All");


		
		PageObjectManager(PageValidation.class).clickOnSectionsOnG1("Rewards");
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("strong;Casino Rewards~div;Your key to a world of benefits from Grosvenor Casinos. All in one.");

		PageObjectManager(PageValidation.class).clickOnSectionsOnG1("Welcome Offers");
		PageObjectManager(PageValidation.class).verifyArticleBlockHeaders("strong;Online Welcome Offers");
		
		//PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Welcome Offers");
		//PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Welcome Offers");
		
		
	}

	@Test(priority=8,groups ={"Web"})
	public void verifyLocalCasinosPageNavigation()
	{
		//Local Casinos
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Local Casinos");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Local Casinos");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Find a Casino,All our Casinos");
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();


		//PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Top Picks~roulette:Roulette~hearts:Blackjack~tux:Live Tables~chip-solid:Other Games");
		//PageObjectManager(PageValidation.class).verifySectionLinks("Top Picks:View All~Roulette:View All~Blackjack:View More~Live Tables:View More~Other Games:View All");


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Find a Casino");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Find a Casino");
		PageObjectManager(PageValidation.class).verifyEntertownorpostcodefield();
		PageObjectManager(PageValidation.class).verifyViewAllCasionsLink();
		PageObjectManager(PageValidation.class).verifycasinoFinderClearButton();
		PageObjectManager(PageValidation.class).verifyfindnearestButton();

		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("All our Casinos");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("All our Casinos");
		PageObjectManager(PageValidation.class).verifyMapIcon();
		PageObjectManager(PageValidation.class).verifyTelephoneIocn();
		PageObjectManager(PageValidation.class).verifyMoreInfoIocn();

		
		
	}

	

	
	@Test(priority=4,groups = {"Web"})
	public void verifyPromosPageNavigation()
	{
		//promos
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Promos");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("All");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Promos");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("All");
		PageObjectManager(PageValidation.class).verifyEnterBonusCodeFieldOnPromotionPage();
		PageObjectManager(PageValidation.class).verifyBonusHistoryLinkOnPromotionPage();
		PageObjectManager(PageValidation.class).verifyPromoetionsONPromosPage();
	}

	@Test(priority=3,groups = {"Web"})
	public void verifyCasinoPageNavigation()
	{
		//casino
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Casino");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Table and Card,Live");
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Casino");
		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Top Picks~roulette:Roulette~hearts:Blackjack~chip-solid:Other Games");
		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Table and Card");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Table and Card");
		PageObjectManager(PageValidation.class).verifyGamesOnCasinoPage();
		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live");
		PageObjectManager(PageValidation.class).verifyGamesOnCasinoPage();	
	}

	@AfterClass(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

}
