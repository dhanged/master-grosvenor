package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 Perform deposit using credit cards
 */
public class Test15_MyAccountCashier_Deposit_Card extends BaseTest 
{	 
	@TestCaseId("Test15")
	@Description("My Account > Cashier - Deposit_Check whether user able to deposit "
			+ "amount using My Account > Cashier > Deposit functionality > Card ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc15_MyAccountCashier_Deposit_Card(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("password"));
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Cashier", "Deposit");
		PageObjectManager(MyAccountPage.class).deposit_UsingCard(dataPool("CardNumber"),
				GenericUtils.getRequiredDateWithCustomYear("0", "MM", "") + 
				GenericUtils.getRequiredDateWithCustomYear("2", "YY", ""),
				GenericUtils.getRandomNumeric(3), GenericUtils.getRandomNumericBetweenTwo(10, 100));
	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}