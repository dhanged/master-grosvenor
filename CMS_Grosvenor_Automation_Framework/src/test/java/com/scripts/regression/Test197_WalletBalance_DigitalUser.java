package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.ResponsibleGamblingView;
import com.view.WalletBalanceView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test197_WalletBalance_DigitalUser extends BaseTest {
	@TestCaseId("Test197")
	@Description("Check that system displays correct wallet details for Digital user")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
		
	public void tc197_WalletBalance_DigitalUser(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet);
		getWaitMethods().sleep(9);
		PageObjectManager(ResponsibleGamblingView.class).PAMlogin(dataPool("PAMUsername"), dataPool("PAMPassword"));
		PageObjectManager(ResponsibleGamblingView.class).PAMSearchCustomer(dataPool("UserName"), dataPool("accStatusPAM"));
		
		String newCashDefaultBalance = PageObjectManager(WalletBalanceView.class).PAM_AddBalanceToWallet("Cash Default",dataPool("Cash Default"));
		String newCashNonCreditBalance = PageObjectManager(WalletBalanceView.class).PAM_AddBalanceToWallet("Cash Non-Credit",dataPool("Cash Non-Credit"));
		String newCasinoBonusBalance = PageObjectManager(WalletBalanceView.class).PAM_AddBalanceToWallet("Casino Bonus",dataPool("Casino Bonus"));
	
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).verifyLoginPopupDisplayed();
		PageObjectManager(MainPage.class).setUserName(dataPool("UserName"));
		PageObjectManager(MainPage.class).setPassword(dataPool("Password"));
		PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(WalletBalanceView.class).DigitalOnly_ValidateBalanceMyAccount(newCashDefaultBalance ,newCashNonCreditBalance ,newCasinoBonusBalance);
		
	}
	
	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	tearDownWebEnvironment(iTestResult);
	}
}
