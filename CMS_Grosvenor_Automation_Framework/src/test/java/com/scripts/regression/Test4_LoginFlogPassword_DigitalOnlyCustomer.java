package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.MainPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 * 	To validate forgotten password
 */
public class Test4_LoginFlogPassword_DigitalOnlyCustomer extends BaseTest 
{	 
	@TestCaseId("Test4")
	@Description("Login - FLOG - Digital Only Customer - FLOG Password_Check system send reset Password link on registrated"
			+ "Email Address when user selects Forgotten Password Option form Login Page.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc4_LoginFlogPassword_DigitalOnlyCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		String password = GenericUtils.getRandomAlphanumericString(6);
	 	PageObjectManager(LoginLogoutView.class).verifyForgottenPasswordFuntionalityForDigitalOnlyCustomer(
 				dataPool("UserName"),dataPool("EmailAddress"), password);
 		updateTestData("Test1", "Password", password);
 	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}