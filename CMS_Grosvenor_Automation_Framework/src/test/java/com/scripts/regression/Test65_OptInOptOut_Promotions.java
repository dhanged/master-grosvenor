package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.PromotionsPage;
import com.pagefactory.grosvenor.MyAccount_BonusPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test65_OptInOptOut_Promotions extends BaseTest {

	
	@TestCaseId("Test65")
	@Description("Check that customer can Opt In for a promotion from the Promotions details page")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"} ,priority = 1)
	
	public void test65_PromotionsDetailPage_DigitalOnly(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("Password"));
		PageObjectManager(MainPage.class).clickCloseX();
		PageObjectManager(PromotionsPage.class).navigateToPromosTAB();
		PageObjectManager(PromotionsPage.class).verifySecondaryLinksDisplayed("All;My Promotions");
		PageObjectManager(PromotionsPage.class).findPromoBTN("Opt-in");
		PageObjectManager(PromotionsPage.class).clickFollowingMoreInfoLink("Opt-in");
		PageObjectManager(PromotionsPage.class).validateActiveExpiryDate();
		PageObjectManager(PromotionsPage.class).clickClaimBTN();
//		PageObjectManager(PromotionsPage.class).verifyTickIMGDisplayed();
		PageObjectManager(PromotionsPage.class).verifyOptedInTXTDisplayed();
		PageObjectManager(PromotionsPage.class).verifyOptOutBTNDisplayed();
		
		
	}
	
	@Description("Check that customer can Opt out from the Opted In promotion from Bonus history")
	@Test(groups = {"Web"},priority=2)
	
	public void test65_OptOutFromPromotion()
	{
		PageObjectManager(MainPage.class).clickMyAccount();
		
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Bonuses","Bonus History");
		PageObjectManager(MyAccount_BonusPage.class).clickBonusType("All types");
		PageObjectManager(MyAccount_BonusPage.class).selectBonusType("Opt In");
		PageObjectManager(MyAccount_BonusPage.class).verifyTypeHDRDisplayed("Opt In");
		PageObjectManager(MyAccount_BonusPage.class).clickExpandButton();
		//PageObjectManager(MyAccount_BonusPage.class).verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
		PageObjectManager(MyAccount_BonusPage.class).verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Ends In,Activation,Bonus status,Expiry");
	 	
 		PageObjectManager(MyAccount_BonusPage.class).verifyOptOutLinkDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyViewTCsLinkDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).clickOptOutLink();
 		PageObjectManager(MyAccount_BonusPage.class).verifyAreUSureTXTonPopupDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyOptOutTXTonPopupDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyYesBTNonPopupDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyNoBTNonPopupDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).clickYesBTNonPopup();
 		PageObjectManager(MyAccount_BonusPage.class).validateBonusStatus("OptedOut");
 	}
	

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	//	tearDownWebEnvironment(iTestResult); 
	}
	
}
