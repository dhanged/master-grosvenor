package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.ResponsibleGamblingView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test117_TakeaBreakFuctionality extends BaseTest {
		
	@TestCaseId("Test117")
	@Description("Take a Break Functionality- Digital Only Customer - Check whether User gets Logged out from Site and suspended when selects Take a Break option")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc117_TakeABreakFunctionality_DigitalCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(ResponsibleGamblingView.class).TakeABreakFunctionality_Digital(dataPool("UserName"), dataPool("Password") ,dataPool("BreakPeriod"));
		PageObjectManager(ResponsibleGamblingView.class).PAMlogin(dataPool("PAMUsername"), dataPool("PAMPassword"));
		PageObjectManager(ResponsibleGamblingView.class).PAMSearchCustomer(dataPool("UserName"), dataPool("accStatusPAM"));
		
		
	
	}

	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

}
