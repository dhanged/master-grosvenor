package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.CasinoDetailView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test126_Casino_Finder_Check_that_system_allows_to_search_the_casinos_Clubs_using_country_and_location extends BaseTest {

	@TestCaseId("Test126")
	@Description("Check that system allows to search the casinos/Clubs using country/location")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc126_RealityChecks_DigitalCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		getWaitMethods().sleep(7);
	
		PageObjectManager(CasinoDetailView.class).CasinoFinder_UsingTown(dataPool("Country"));
  	}
	
	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

	
	
	
	
}
