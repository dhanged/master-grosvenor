package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.ResponsibleGamblingView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test107_RealityChecks extends BaseTest {
	

	@TestCaseId("Test107")
	@Description("Reality checks-Check that system allows to set the reality check for the user.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc107_RealityChecks_DigitalCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(ResponsibleGamblingView.class).RealityChecksFunctionality_Digital(dataPool("UserName"), dataPool("Password") ,dataPool("ReminderPeriod"));
 	}
	
	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	tearDownWebEnvironment(iTestResult);
	}
	
}
