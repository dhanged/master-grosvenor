package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.pagefactory.grosvenor.SignUpPage;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 	To check digital registration with first deposit
 */
public class Test8_Registration_DigitalOnlyCustomer_Deposit extends BaseTest 
{	 
	@TestCaseId("Test8")
	@Description("Registration - Digital Only Customer - Digital Registration_Check whether "
			+ "user able to deposit after successful registration.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc8_Registration_DigitalOnlyCustomer_Deposit(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();

		String userName = "opst" + GenericUtils.getRandomAlphanumericString(8);
		String password = "Test" + GenericUtils.getRandomNumeric(4);
	 	PageObjectManager(MainPage.class).clickJoinNow();
		PageObjectManager(SignUpPage.class).verifySignUpHeaderDisplayed();
		PageObjectManager(SignUpPage.class).selectNewUserYes();
		PageObjectManager(SignUpPage.class).setEmailAddress(GenericUtils.getRandomAlphanumericEmailString(10, "@mailinator.com"));
		PageObjectManager(SignUpPage.class).selectJoinnowAgreeCheckbox();
		PageObjectManager(SignUpPage.class).clickNext();
		PageObjectManager(SignUpPage.class).setUserName(userName);
		PageObjectManager(SignUpPage.class).setPassword(password);
		PageObjectManager(SignUpPage.class).selectTitle(GenericUtils.getRandomTitleForUser());
		PageObjectManager(SignUpPage.class).setFirstName(GenericUtils.getRandomAlphabeticString(8));
		PageObjectManager(SignUpPage.class).setSurName(GenericUtils.getRandomAlphabeticString(8));
		PageObjectManager(SignUpPage.class).selectDateOfBirth(GenericUtils.getRequiredDateWithCustomYear("-20", "DD_MM_YYYY", ""));
		PageObjectManager(SignUpPage.class).selectCountry(dataPool("Country"));
		PageObjectManager(SignUpPage.class).setmanualAddress(GenericUtils.getRandomAlphabeticString(8), GenericUtils.getRandomAlphabeticString(8),
				GenericUtils.getRandomAlphabeticString(8), dataPool("Country"), dataPool("Postcode"));
		PageObjectManager(SignUpPage.class).setMobileNumber(("077009" + GenericUtils.getRandomNumeric(5)));
		PageObjectManager(SignUpPage.class).setMarketingPreferences(dataPool("AppNotificationPreferences"));
		PageObjectManager(SignUpPage.class).selectDepositLimitNo();
		PageObjectManager(SignUpPage.class).clickRegister();
		updateTestData("UserName", userName);
		updateTestData("Password", password);
		PageObjectManager(MyAccountPage.class).deposit_UsingCard(dataPool("CardNumber"), 
				GenericUtils.getRequiredDateWithCustomYear("0", "MM", "") + GenericUtils.getRequiredDateWithCustomYear("2", "YY", ""),
				GenericUtils.getRandomNumeric(3), GenericUtils.getRandomNumericBetweenTwo(10, 100));
		PageObjectManager(MyAccountPage.class).verifyDepositSuccessPopUpDisplayed();
	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		//tearDownWebEnvironment(iTestResult); 
	}
}