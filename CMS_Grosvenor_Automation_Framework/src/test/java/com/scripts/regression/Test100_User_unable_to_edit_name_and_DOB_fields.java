
package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.AccountDetailsView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test100_User_unable_to_edit_name_and_DOB_fields extends BaseTest {
	

	@TestCaseId("Test100")
	@Description("Account Details_Check whether user unable to edit Name and DOB.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc100_UnableToEditAccountDetails(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(AccountDetailsView.class).AccountDetails_UpdateDetails_Digital(dataPool("UserName"), dataPool("Password"));
  	}
	
	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	tearDownWebEnvironment(iTestResult);
	}

}
