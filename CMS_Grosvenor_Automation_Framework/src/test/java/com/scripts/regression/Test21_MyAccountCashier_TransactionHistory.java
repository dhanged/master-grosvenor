package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 	Validate transaction history 
 */
public class Test21_MyAccountCashier_TransactionHistory extends BaseTest 
{	 
	@TestCaseId("Test21")
	@Description("My Account > Cashier - Transaction History_Check whether user able to view Transactions on My Account > Transaction Page")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc21_MyAccountCashier_Withdraw_FastBankTransfer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("password")); 
	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}