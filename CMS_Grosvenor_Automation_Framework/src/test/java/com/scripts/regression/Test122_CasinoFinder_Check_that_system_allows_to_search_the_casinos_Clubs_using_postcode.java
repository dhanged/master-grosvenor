package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.CasinoDetailView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test122_CasinoFinder_Check_that_system_allows_to_search_the_casinos_Clubs_using_postcode extends BaseTest {
	
	@TestCaseId("Test122")
	@Description("Casino Finder_Check that system allows to search the casinos/Clubs using postcode")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc122_RealityChecks_DigitalCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		getWaitMethods().sleep(5);
		//PageObjectManager(CasinoDetailView.class).CasinoFinder_UsingPostcode(dataPool("Postcode"));
		PageObjectManager(CasinoDetailView.class).CasinoFinder_UsingPostcode(dataPool("Postcode"));
  	}
	
	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}


}
