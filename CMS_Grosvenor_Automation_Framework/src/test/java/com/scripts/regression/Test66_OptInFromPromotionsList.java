package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.PromotionsPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test66_OptInFromPromotionsList extends BaseTest {
	
	@TestCaseId("Test66")
	@Description("Check that customer can Opt In for a promotion from the Promotions list")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	
	public void tc66_OptInFromPromotionsList(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("Password"));
		PageObjectManager(MainPage.class).clickCloseX();
		PageObjectManager(PromotionsPage.class).navigateToPromosTAB();
		PageObjectManager(PromotionsPage.class).verifySecondaryLinksDisplayed("All;My Promotions");

		PageObjectManager(PromotionsPage.class).findPromoBTN("Opt-in");
		PageObjectManager(PromotionsPage.class).clickPromoBTNonPromotions("Opt-in");
		PageObjectManager(PromotionsPage.class).verifyOptedInTXTonPromotionsPage();
	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	//	tearDownWebEnvironment(iTestResult); 
	}
}
