package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.CasinoDetailView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test131_Casino_Details_Page_Check_that_system_displays_Casinos_details_page_with_all_the_required_details extends BaseTest {

	@TestCaseId("Test131")
	@Description("Casino Details Page_Check that system displays Casinos details page with all the required details")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc131_CasinoDetailsPage(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		getWaitMethods().sleep(5);
		
		PageObjectManager(CasinoDetailView.class).CasinoFinder_CasinoDetails("London");
  	}
	
	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

	
}
