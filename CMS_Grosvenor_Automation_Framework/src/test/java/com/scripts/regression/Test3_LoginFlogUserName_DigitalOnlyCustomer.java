package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 To validate forgotten username
 */
public class Test3_LoginFlogUserName_DigitalOnlyCustomer extends BaseTest 
{	 
	@TestCaseId("Test3")
	@Description("Login - FLOG - Digital Only Customer - FLOG Username_Check system send username on "
			+ "registrated Email Address when user selects Forgotten username Option form Login Page.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc3_LoginFlogUserName_DigitalOnlyCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
 		PageObjectManager(LoginLogoutView.class).verifyForgottenUsernameFuntionalityForDigitalOnlyCustomer(
				dataPool("EmailAddress"));
 	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}