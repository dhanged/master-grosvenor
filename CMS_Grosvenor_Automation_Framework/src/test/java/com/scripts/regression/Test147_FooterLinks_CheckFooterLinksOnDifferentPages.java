package com.scripts.regression;
import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.Footer;
import com.pagefactory.grosvenor.MainPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;


public class Test147_FooterLinks_CheckFooterLinksOnDifferentPages extends BaseTest {
	
	@TestCaseId("Test147")
	@Description("Check Footer links are available on different pages ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc147_FooterLinks_CheckFooterLinksOnDifferentPages(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clicklivecasinoTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();	
		
		PageObjectManager(MainPage.class).clickSlotsandGamesTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clickJackpotsTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clickTableGamesTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clickSportTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clickPokerTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clickPromosTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clickGrosvenorOneTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
		PageObjectManager(MainPage.class).clickLocalCasinosTab();
		PageObjectManager(Footer.class).verifyUsefulLinksDisplayed();
		PageObjectManager(Footer.class).verifyHelpLinksDisplayed();
		
			
	}

	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

}
