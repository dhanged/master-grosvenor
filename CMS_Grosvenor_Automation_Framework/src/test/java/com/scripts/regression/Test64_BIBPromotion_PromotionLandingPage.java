package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.PromotionsPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test64_BIBPromotion_PromotionLandingPage extends BaseTest{
	
	@TestCaseId("Test64")
	@Description("Check that customer can claim for a BIB promotion from the Promotions landing page and allows to redeem the Claimed amount for the user.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	
	public void test64_BIBPromotion_PromotionLandingPage(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("Password"));
		
		PageObjectManager(MainPage.class).clickCloseX();
		PageObjectManager(PromotionsPage.class).navigateToPromosTAB();
			
		PageObjectManager(PromotionsPage.class).findPromoBTN("Claim");
		PageObjectManager(PromotionsPage.class).clickPromoBTNonPromotions("Claim");
		PageObjectManager(PromotionsPage.class).verifyavailableCashLabeldisplayed();
		String availableCash = PageObjectManager(PromotionsPage.class).getAvailableCashAmount() ;
		
		String BuyInForCash = PageObjectManager(PromotionsPage.class).getBuyInForCashAmount() ;
		String BuyInBonusAmount = PageObjectManager(PromotionsPage.class).getBuyInBonusAmount() ;
		String PlayWithAmount =  PageObjectManager(PromotionsPage.class).getPlayWithAmount() ;
		System.out.println(" availableCash : " + availableCash + " BuyInForCash : " + BuyInForCash+" BuyInBonusAmount: "+BuyInBonusAmount + " PlayWithAmount : " +PlayWithAmount);
		
		PageObjectManager(PromotionsPage.class).verifyBuyInBTNdisplayed();
		PageObjectManager(PromotionsPage.class).verifyCancelBTNdisplayed();
		PageObjectManager(PromotionsPage.class).clickBuyInBTN();
		
		PageObjectManager(PromotionsPage.class).verifyConfirmBuyingInHDRDisplayed();
		PageObjectManager(PromotionsPage.class).verifyOKBTNonPopup();
		PageObjectManager(PromotionsPage.class).verifyCancelBTNonPopup();
		PageObjectManager(PromotionsPage.class).clickOKBTNonPopup();
		
		PageObjectManager(PromotionsPage.class).verifyBuyInSuccessMsgDisplayed();
		PageObjectManager(PromotionsPage.class).validateReceivedAmount(PlayWithAmount );
		
		PageObjectManager(PromotionsPage.class).verifyBuyInDetailsOnPopup();
		PageObjectManager(PromotionsPage.class).validateBonusDetailOnPopup(availableCash,BuyInForCash,BuyInBonusAmount);
	
	}

	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
	
	
	

}
