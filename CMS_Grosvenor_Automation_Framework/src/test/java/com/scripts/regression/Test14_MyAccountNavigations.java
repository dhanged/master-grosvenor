package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 My Account navigation
 */
public class Test14_MyAccountNavigations extends BaseTest 
{	 
	@TestCaseId("Test14")
	@Description("My Account Navigations -My Account Navigation_Check that system redirects to appropriate window on clicking MyAccount menus..")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc14_myAccountNavigations(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("password"));
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Cashier");
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Bonuses");
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Messages");
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Account Details");
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling");
  	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}