package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

/**	
 Perform withdraw using credit cards
 */
public class Test18_MyAccountCashier_Withdraw_Card extends BaseTest 
{	 
	@TestCaseId("Test18")
	@Description("My Account > Cashier - Withdraw_Check whether user able to Withdraw amount using My Account > Cashier > Withdraw functionality > Using Card")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc18_MyAccountCashier_Withdraw_Card(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("password"));
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Cashier", "Withdrawal");
		int availableAmount = PageObjectManager(MyAccountPage.class).getAvailableAmountToWithdraw();
		String withdrawAmount = String.valueOf(((availableAmount > 20) ? (availableAmount - 10) : availableAmount)); 
		PageObjectManager(MyAccountPage.class).withdraw_UsingCard(withdrawAmount, dataPool("password"));
	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}