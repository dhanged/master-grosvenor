package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.DepositLimitPage;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.view.ResponsibleGamblingView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test112_DepositLimit_Check_whether_system_allow_to_set_deposit_limit_for_existing_user extends BaseTest 
{	 
	@TestCaseId("Test112")
	@Description("Deposit Limit_Check whether system allow to set deposit limit for existing user")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Mobile"},priority=1)
	public void tc112_VerifyDepositLimitErrorMessage(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();

		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(dataPool("UserName"), dataPool("password"));
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Deposit Limits");

		PageObjectManager(DepositLimitPage.class).verifyWerecommendthatyousetdepositlimitstohelpyoumanageyourspendingInfoText();

		PageObjectManager(DepositLimitPage.class).selectDepositLimit("Daily");
		PageObjectManager(DepositLimitPage.class).clickOnResetLimit();

		int dailyLimit = Integer.valueOf(dataPool("DailyDepositLimit")) - 1;
		System.out.println(" dailyLimit :  "+dailyLimit);
		PageObjectManager(DepositLimitPage.class).setDepositLimit(String.valueOf(dailyLimit));
		PageObjectManager(DepositLimitPage.class).clickOnSetLimit();
		PageObjectManager(DepositLimitPage.class).verifyDepositLimitPendingRequestReceivedMessage();
		PageObjectManager(DepositLimitPage.class).clickOnClose();

		PageObjectManager(MainPage.class).clickMyAccount();
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Deposit Limits");
		getWaitMethods().sleep(5);
		PageObjectManager(DepositLimitPage.class).verifyDepositLimitIsSetSuccessfullyOrNot("Daily",String.valueOf(dailyLimit));

		updateTestData("DailyDepositLimit",String.valueOf(dailyLimit));

		
		
		PageObjectManager(DepositLimitPage.class).selectDepositLimit("Weekly");
		PageObjectManager(DepositLimitPage.class).clickOnResetLimit();
		
		int weeklyLmt = Integer.valueOf(dataPool("WeeklyDepositLimit")) +1;
		System.out.println(" weeklyLmt :  "+weeklyLmt);
		
		PageObjectManager(DepositLimitPage.class).setDepositLimit(String.valueOf(weeklyLmt));
		PageObjectManager(DepositLimitPage.class).clickOnSetLimit();
		PageObjectManager(DepositLimitPage.class).verifyDepositLimitPendingRequestReceivedMessage();
		PageObjectManager(DepositLimitPage.class).clickOnClose();

		PageObjectManager(MainPage.class).clickMyAccount();
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Deposit Limits");
		getWaitMethods().sleep(5);
		PageObjectManager(DepositLimitPage.class).verifyPendingLimitIsSetSuccessfully("Weekly",String.valueOf(weeklyLmt));
		PageObjectManager(DepositLimitPage.class).verifyActivateLimitButton();
		PageObjectManager(DepositLimitPage.class).verifyCancelLink();
		PageObjectManager(DepositLimitPage.class).clickOnCancelLink();
		
		//PageObjectManager(DepositLimitPage.class).verifyDepositLimitIsSetSuccessfullyOrNot("Weekly",String.valueOf(weeklyLmt));
		updateTestData("WeeklyDepositLimit\r\n" + 
				"",String.valueOf(weeklyLmt));

		
		/*need to run on
		PageObjectManager(DepositLimitPage.class).verifyPendingLimitIsSetSuccessfully("Weekly",String.valueOf(weeklyLmt));
		PageObjectManager(DepositLimitPage.class).verifyActivateLimitButton();
		PageObjectManager(DepositLimitPage.class).verifyCancelLink();
        */
			}

	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}
