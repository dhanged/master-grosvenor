package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.ResponsibleGamblingView;
import com.view.WalletBalanceView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test72_WalletBalance_SAWUser extends BaseTest {
	
@TestCaseId("Test72")
@Description("Check that system displays correct wallet details for SAW user")
@TestDataSource(dataSource = "/Regression/Regression")
@Test(dataProvider = "TestDataProvider", groups = {"Web"},priority = 1)
	
public void tc72_WalletBalance_SAWUser(Hashtable<String, String> dataSet){
	
	initializeWebEnvironment(dataSet);
	getWaitMethods().sleep(9);
	PageObjectManager(ResponsibleGamblingView.class).PAMlogin(dataPool("PAMUsername"), dataPool("PAMPassword"));
	PageObjectManager(ResponsibleGamblingView.class).PAMSearchCustomer(dataPool("UserName"), dataPool("accStatusPAM"));
	
	PageObjectManager(WalletBalanceView.class).PAM_AddBalanceToWallet("Cash Default",dataPool("Cash Default"));
	PageObjectManager(WalletBalanceView.class).PAM_AddBalanceToWallet("Cash Non-Credit",dataPool("Cash Non-Credit"));
	PageObjectManager(WalletBalanceView.class).PAM_AddBalanceToWallet("Casino Bonus",dataPool("Casino Bonus"));
	PageObjectManager(WalletBalanceView.class).PAM_AddBalanceToWallet("Cash Unplayable",dataPool("CashUnplayable"));
	
	initializeWebEnvironment(dataSet); 
	getWebDomains().launchURLForGCWeb();
	PageObjectManager(MainPage.class).clickLogin();
	PageObjectManager(MainPage.class).verifyLoginPopupDisplayed();
	PageObjectManager(MainPage.class).setUserName(dataPool("UserName"));
	PageObjectManager(MainPage.class).setPassword(dataPool("Password"));
	PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
	PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
	
	PageObjectManager(WalletBalanceView.class).SAW_ValidateBlanceMyAccount(dataPool("Cash Default") ,dataPool("Cash Non-Credit") , dataPool("CashUnplayable"));
}

@AfterMethod(groups = {"Web"})
public void tearDown(ITestResult iTestResult){
	//tearDownWebEnvironment(iTestResult);
   
}

}
