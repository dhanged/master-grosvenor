package com.scripts.regression;

import java.util.Hashtable;
import java.util.Properties;

import org.openqa.selenium.By;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.Footer;
import com.pagefactory.grosvenor.MainPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test154_FooterLinks_ClickOnFooterLinks extends BaseTest {
	
	
	
	@TestCaseId("Test154")
	@Description("Check Footer links are clickable ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc154_FooterLinks_ClickOnFooterLinks(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
       	PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(Footer.class).verifyFooterLinks("USEFUL LINKS","Slots & Games~Online Poker~Live Casino~Roulette Games~Jackpots~Blackjack~Sports Betting~Grosvenor Blog~Grosvenor One");
		PageObjectManager(Footer.class).verifyFooterLinks("HELP AND ADVICE","Live Help~Complaints Process~Responsible Gambling~Deposit Limits~Work for Grosvenor~Terms & Conditions~Privacy Policy~Affiliates~Carers Trust");
		
		String url = getConfiguration().getConfig("uat.gc.URL");
		PageObjectManager(Footer.class).clickOnFooterLinks("HELP AND ADVICE","Complaints Process,"+url+"static/contactus"+"~Privacy Policy,"+url+"static/privacy-policy"+"~Carers Trust,"+url+"grosvenor-and-carers-trust");
	    PageObjectManager(Footer.class).clickOnFooterLinks("USEFUL LINKS","Slots & Games,"+url+"slots-and-games"+"~Online Poker,"+url+"poker"+"~Live Casino,"+url+"live-casino"+"~Roulette Games,"+url+"table-games/roulette"+"~Jackpots,"+url+"jackpots"+"~Blackjack,"+url+"live-casino/live-blackjack"+"~Sports Betting,"+url+"sport#home"+"~Grosvenor One,"+url+"grosvenor-one");
	    
	    
	    /*Following Four tabs are remaining
	    PageObjectManager(Footer.class).clickOnFooterLinks("HELP AND ADVICE","Deposit Limits"+url+"#login");
	    PageObjectManager(Footer.class).clickOnFooterLinks("USEFUL LINKS","Grosvenor Blog");
	    PageObjectManager(Footer.class).clickOnFooterLinks("USEFUL LINKS","This month's promotions");
		PageObjectManager(Footer.class).clickOnFooterLinks("HELP AND ADVICE","Terms & Conditions");
		*/
		
		}

	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

}
