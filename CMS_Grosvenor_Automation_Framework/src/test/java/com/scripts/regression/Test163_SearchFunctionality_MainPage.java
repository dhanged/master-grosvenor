package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.HomePage;
import com.pagefactory.grosvenor.MainPage;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test163_SearchFunctionality_MainPage extends BaseTest {
	
	@TestCaseId("Test163")
	@TestDataSource(dataSource = "/Regression/Regression")
	
	@Description("Check that system displays the appropriate options(Game name) on entering text in the search box with Game image, game name and Play button on search window "+ "Check that system displays Error message if no search is found for the entered text "
			+ "system allows to browse the sections on clicking on error messages observed for wrong search")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"},priority = 1)
	public void tc163_SearchUsingGameName(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet);
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(MainPage.class).clickLogin();
		getWaitMethods().sleep(6);
		PageObjectManager(MainPage.class).verifyLoginPopupDisplayed();
		PageObjectManager(MainPage.class).setUserName(dataPool("UserName"));
		PageObjectManager(MainPage.class).setPassword(dataPool("Password"));
		PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
		PageObjectManager(HomePage.class).verifySearchBtnDisplayed();
		PageObjectManager(HomePage.class).clickSearchBtn();
		PageObjectManager(HomePage.class).verifyinputSearchNameFieldDisplayed();
		PageObjectManager(HomePage.class).setSearchNameField("cle");
		PageObjectManager(HomePage.class).verifyGameNameOnSearchResultsDisplayed("Cle");
		PageObjectManager(HomePage.class).verifyInfoIconOnSearchResultsDisplayed("Cle");
		PageObjectManager(HomePage.class).verifyPlayBtnOnSearchResultsDisplayed();
		PageObjectManager(HomePage.class).clickInfoIconOnSearchResults();
		PageObjectManager(HomePage.class).verifyTabsOnGameInfo("Description~How to play~Terms & Conditions");
		PageObjectManager(HomePage.class).clickCloseXonGameInfoPage();	
		
		getWaitMethods().sleep(5);
		
		PageObjectManager(HomePage.class).verifySearchBtnDisplayed();
		PageObjectManager(HomePage.class).clickSearchBtn();
		PageObjectManager(HomePage.class).verifyinputSearchNameFieldDisplayed();
		PageObjectManager(HomePage.class).setSearchNameField("xcd");
		PageObjectManager(HomePage.class).verifyNoResultsErrorMSGDisplayed();
		PageObjectManager(HomePage.class).verifybrowseSectionBTNDisplayed();
		PageObjectManager(HomePage.class).verifycloseBTNDisplayed();
		PageObjectManager(HomePage.class).clickbrowseSectionBTN();
		PageObjectManager(HomePage.class).verifyIconsDisplayed();
		PageObjectManager(HomePage.class).verifySectionsDisplayed("All Slots,Top Slots,Jackpots,New Slots,Live Roulette,Live Blackjack");
		
		
        String url = getConfiguration().getConfig("uat.gc.URL");
		
        PageObjectManager(HomePage.class).clickOnSection("All Slots",url+"slots-and-games/all" ,"All Slots and Games | Grosvenor Casinos");
        PageObjectManager(HomePage.class).clickOnSection("Top Slots",url+"slots-and-games/featured" ,"Featured Slots and Games | Grosvenor Casinos");
        PageObjectManager(HomePage.class).clickOnSection("Jackpots",url+"jackpots" , "Win Each Day With Our Daily Jackpots | Grosvenor Casinos");
        PageObjectManager(HomePage.class).clickOnSection("New Slots",url+"slots-and-games/new" ,"New Slots and Games | Grosvenor Casinos");
        PageObjectManager(HomePage.class).clickOnSection("Live Roulette",url+"live-casino/live-roulette" ,"Play Live Roulette | Grosvenor Casinos");
		PageObjectManager(HomePage.class).clickOnSection("Live Blackjack",url+"live-casino/live-blackjack" ,"Live Blackjack | Grosvenor Casinos");
		
		   
	
	}
	
	
	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}

}
