package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.pagefactory.grosvenor.MyAccount_BonusPage;
import com.view.ResponsibleGamblingView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test194_Bonus_History_Check_that_the_customer_can_filter_the_Bonus_history_based_on_bonus_type_and_date_range extends BaseTest {
	
	@TestCaseId("Test194")
	@Description("Bonus History _Check that the customer can filter the Bonus history based on bonus type , "
			+ " Check that the customer can filter the Bonus history based on date range " )
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"},priority = 1)
		
	public void tc194_BonusHistoryByBonusType(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(dataPool("UserName"), dataPool("Password"));
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Bonuses","Bonus History");
		
		PageObjectManager(MyAccount_BonusPage.class).verifyFilterByBonusTypeLabelDisplayed();
		
		PageObjectManager(MyAccount_BonusPage.class).clickBonusType("All types");
		PageObjectManager(MyAccount_BonusPage.class).verifyBonusTypesDisplayed("All types,First Deposit,Deposit,Instant,Player Registration,Card Registration,Deposit Over Period,Cashback On Total Stake," + 
				"Cashback On Net Losses,Manual Adjustment,Opt In,Content,Buy In");
		PageObjectManager(MyAccount_BonusPage.class).selectBonusType("Instant");
		//PageObjectManager(MyAccount_BonusPage.class).clickFilterButton();
		PageObjectManager(MyAccount_BonusPage.class).verifyTypeHDRDisplayed("Instant");
		PageObjectManager(MyAccount_BonusPage.class).clickBonusType("Instant");
		
		//PageObjectManager(MyAccount_BonusPage.class).selectBonusType("First Deposit");
		//PageObjectManager(MyAccount_BonusPage.class).verifyTypeHDRDisplayed("First Deposit");
		//PageObjectManager(MyAccount_BonusPage.class).clickBonusType("First Deposit");
	    
		PageObjectManager(MyAccount_BonusPage.class).selectBonusType("Player Registration");
		PageObjectManager(MyAccount_BonusPage.class).verifyTypeHDRDisplayed("Player Registration");
		PageObjectManager(MyAccount_BonusPage.class).clickBonusType("Player Registration");
	    
		PageObjectManager(MyAccount_BonusPage.class).selectBonusType("Opt In");
		PageObjectManager(MyAccount_BonusPage.class).verifyTypeHDRDisplayed("Opt In");
		PageObjectManager(MyAccount_BonusPage.class).clickBonusType("Opt In");
	    
		
		PageObjectManager(MyAccount_BonusPage.class).selectBonusType("Buy In");
		PageObjectManager(MyAccount_BonusPage.class).verifyTypeHDRDisplayed("Buy In");
		PageObjectManager(MyAccount_BonusPage.class).clickXbtn();
	  	
	
	
	}
	
		@Description(" Check that the customer can filter the Bonus history based on date range " )
		@Test(groups = {"Web"},priority = 2)
		
		public void tc194_BonusHistoryByDateRange()
		{
			String fromDate = GenericUtils.getRequiredDay("-1", "dd/MM/yyyy", "");
			String toDate = GenericUtils.getCurrentDate("dd/MM/YYYY");
			
			PageObjectManager(MainPage.class).clickMyAccount();
	 		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Bonuses","Bonus History");
	 		PageObjectManager(MyAccount_BonusPage.class).verifyFilterByDateRangeLabelDisplayed();
			PageObjectManager(MyAccount_BonusPage.class).verifyFilterButtonDisplayed();
			PageObjectManager(MyAccount_BonusPage.class).verifyResetLinkdisplayed();
			PageObjectManager(MyAccount_BonusPage.class).setFromDate(fromDate);
	 		PageObjectManager(MyAccount_BonusPage.class).verifyToDate(toDate);
	 		PageObjectManager(MyAccount_BonusPage.class).clickFilterButton();
	 		PageObjectManager(MyAccount_BonusPage.class).verifyExpandButtonDisplayed();
	 		
	 		PageObjectManager(MyAccount_BonusPage.class).verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
	 		//PageObjectManager(MyAccount_BonusPage.class).verifyOptOutLinkDisplayed();
	 		PageObjectManager(MyAccount_BonusPage.class).verifyViewTCsLinkDisplayed();
	 		PageObjectManager(MyAccount_BonusPage.class).clickXbtn();
	 	}
		
		@AfterClass(groups = {"Web"})
		public void tearDown(ITestResult iTestResult){
			tearDownWebEnvironment(iTestResult); 
		}
	}
