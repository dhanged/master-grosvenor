package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.DepositLimitPage;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.view.CashierView;
import com.view.LoginLogoutView;


import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test116_Deposit_Limit_Check_that_system_displays_error_message_if_user_tries_to_deposit_amount_greater_than_the_limit_set extends BaseTest {


	@TestCaseId("Test116")
	@Description("Deposit Limit_Check that system displays error message if user tries to deposit amount greater than the limit set. ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web","Compliance"})
	public void tc116_Checkthatsystemdisplayserrormessageifusertriestodepositamountgreaterthanthelimitset(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet);
		getWebDomains().launchURLForGCWeb();

		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(LoginLogoutView.class).loginToGrosvenorCasino(dataPool("UserName"), dataPool("password"));
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Cashier", "Deposit");
//      
		//getWaitMethods().sleep(10);
		//PageObjectManager(MyAccountPage.class).clickShowMore();
		
		PageObjectManager(MyAccountPage.class).deposit_UsingCard(dataPool("CardNumber"), 
				GenericUtils.getRequiredDateWithCustomYear("0", "MM", "") + GenericUtils.getRequiredDateWithCustomYear("2", "YY", ""),
				GenericUtils.getRandomNumeric(3), GenericUtils.getRandomNumericBetweenTwo(10, 100));
		PageObjectManager(MyAccountPage.class).verifyDepositSuccessPopUpDisplayed();
		PageObjectManager(MyAccountPage.class).Closedafterdeposit();
		
	
		
		//
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Cashier", "Deposit");
		int dailyLimit = Integer.valueOf(dataPool("DailyDepositLimit")) + 1;
		System.out.println(" dailyLimit :  "+dailyLimit);
		
		PageObjectManager(MyAccountPage.class).setDepositAmt(String.valueOf(dailyLimit));
		PageObjectManager(MyAccountPage.class).clickNext();
		PageObjectManager(DepositLimitPage.class).verifyDepositLimitErrorMesageOnDepositPage("Daily deposit limit reached");
	}


	@AfterMethod(groups = {"Web","Compliance"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}
