package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.pagefactory.grosvenor.MyAccount_BonusPage;
import com.view.ResponsibleGamblingView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test185_Bonus_Claim_Check_that_system_allows_to_claim_the_bonus_using_Bonus_code_for_the_user extends BaseTest {
	
 static String BonusName = null;
	@TestCaseId("Test185")
	@Description("Bonus Claim_Check that system allows to claim the bonus using Bonus code for the user ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"},priority = 1)
	public void tc185_BonusClaimUsingBonusCode(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();		
		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(dataPool("UserName"), dataPool("Password"));
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Bonuses","Enter bonus code");
 		PageObjectManager(MyAccount_BonusPage.class).verifyEnterBonusCodeTXTDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).setPromoCode(dataPool("BonusCode"));
 		PageObjectManager(MyAccount_BonusPage.class).verifyClearBTN();
 		PageObjectManager(MyAccount_BonusPage.class).clickSubmitcode();
 		PageObjectManager(MyAccount_BonusPage.class).verifyTermsnConditionsLinkDisplayed();
 		BonusName= "epiInstantBonus";
 				/*PageObjectManager(MyAccount_BonusPage.class).getBonusName();
 		System.out.println("Bonus Name : " +BonusName);*/
 		
 		PageObjectManager(MyAccount_BonusPage.class).verifyAcceptCheckboxDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyAcceptTnCsLabelDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).SelectAcceptCheckboxDisplayed();
		PageObjectManager(MyAccount_BonusPage.class).verifyClaimPromotionBTN();
		PageObjectManager(MyAccount_BonusPage.class).verifyCancelLinkDisplayed();
		PageObjectManager(MyAccount_BonusPage.class).clickClaimPromotionBTN();
 		PageObjectManager(MyAccount_BonusPage.class).verifySuccessHeaderDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyClaimedBonusNameDisplayed(BonusName);
	
 		PageObjectManager(MyAccount_BonusPage.class).verifyCloseBTNDisplayed();
		PageObjectManager(MyAccount_BonusPage.class).verifyActivePromotionsBTNDisplayed();
		PageObjectManager(MyAccount_BonusPage.class).ClickCloseBTNDisplayed();
 		
  	}
	
	@Description("check that system displays the claimed bonus details in bonus history tab")
	@Test( groups = {"Web"},priority = 2)
	public void tc185_ClaimedBonusUnderBonusHistory()
	{
		PageObjectManager(MainPage.class).clickMyAccount();
 		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Bonuses","Bonus History");
		System.out.println("Bonus Name : " + BonusName);
		PageObjectManager(MyAccount_BonusPage.class).verifyFilterByBonusTypeLabelDisplayed();
		PageObjectManager(MyAccount_BonusPage.class).verifyFilterByDateRangeLabelDisplayed();
		PageObjectManager(MyAccount_BonusPage.class).verifyFilterButtonDisplayed();
		PageObjectManager(MyAccount_BonusPage.class).clickFilterButton();		
		PageObjectManager(MyAccount_BonusPage.class).verifyBonusNameOnBonusHistoryDisplayed(BonusName);
		PageObjectManager(MyAccount_BonusPage.class).verifyExpandButtonDisplayed();
 		
 		PageObjectManager(MyAccount_BonusPage.class).verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
 		//PageObjectManager(MyAccount_BonusPage.class).verifyOptOutLinkDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyViewTCsLinkDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).clickCollapseButton();
 		PageObjectManager(MyAccount_BonusPage.class).clickXbtn();
	}
	
	@Description("Active Bonuses_Check that system allows to expand & Collapsed each active bonus for the user")
	@Test( groups = {"Web"},priority = 3)
	public void tc185_ActiveBonuses() 
 	{
		PageObjectManager(MainPage.class).clickMyAccount();
 		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Bonuses","Active Bonuses");
 		
 		PageObjectManager(MyAccount_BonusPage.class).verifyBonusNameOnBonusHistoryDisplayed(BonusName);
 		PageObjectManager(MyAccount_BonusPage.class).verifyExpandButtonDisplayed();
 		
 		PageObjectManager(MyAccount_BonusPage.class).verifyActiveBonusesLabelsDisplayed("Type,Bonus status,Amount,Wagering target,Progress,Activation,Bonus status,Expiry");
 		PageObjectManager(MyAccount_BonusPage.class).verifyOptOutLinkDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).verifyViewTCsLinkDisplayed();
 		PageObjectManager(MyAccount_BonusPage.class).clickCollapseButton();
 	}
	
	
	@AfterClass(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}
