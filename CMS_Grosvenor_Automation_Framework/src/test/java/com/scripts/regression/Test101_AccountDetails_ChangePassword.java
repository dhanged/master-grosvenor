
package com.scripts.regression;

import java.util.Hashtable;

import org.apache.commons.lang.RandomStringUtils;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.AccountDetailsView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test101_AccountDetails_ChangePassword extends BaseTest{
	
	
	@TestCaseId("Test101")
	@Description("Account Details_Check whether user unable to edit name, DOB, Email and address.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc101_UnableToEditAccountDetails(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		String NewPassword= "RankGroup"+RandomStringUtils.randomNumeric(2);
		 
		PageObjectManager(AccountDetailsView.class).AccountDetails_ChangePassword_Digital(dataPool("UserName"), dataPool("Password"),NewPassword);
		
		updateTestData("NewPassword",NewPassword);
		
  	}
	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
	tearDownWebEnvironment(iTestResult);
	}

}
