package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.HomePage;
import com.pagefactory.grosvenor.LiveCasinosPage;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.PageValidation;
import com.pagefactory.grosvenor.SearchPage;
import com.view.CasinoDetailView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test138_LiveCasino extends BaseTest {

	
	@TestCaseId("Test138")
	@Description("Test138_Live Casino_Check that system displays Live casinos games on Live casino landing page"+"TC4609_Live Casino_Check that system displays Live roulette, Blackjack, Baccarat type on Live casinos landing page"+"TC4610_Live Casino_Check that system allows to access Live casinos games details page on clicking 'I' button from game.")
	@TestDataSource(dataSource = "/Regression/Regression")

	@Test(dataProvider = "TestDataProvider", groups = {"Web"} )
	
	public void tc138_LiveCasinoLandingPage(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		getWaitMethods().sleep(5);
		
		//PageObjectManager(LiveCasinosPage.class).navigateToCasinosTAB();
		
		PageObjectManager(PageValidation.class).navigateToPrimaryTabs("Live Casino");
		PageObjectManager(PageValidation.class).verifySecondaryTABdisplayed("Host Offers,Live Roulette,Live Blackjack,Live Baccarat,Live Casino Poker,Game Shows,Grosvenor Exclusive");
		PageObjectManager(PageValidation.class).verifyPrimayTabDisplayedAsSelectedOrNot("Live Casino");
		
		PageObjectManager(PageValidation.class).verifyCarouselDisplayedOnPage();
		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("star:Top Picks~tux:Most Popular~roulette:Live Roulette~hearts:Live Blackjack~chip-solid:Game Show~token:Live Baccarat~tux:More Games~medal:VIP");
		PageObjectManager(PageValidation.class).verifySectionLinks("Live Roulette:View More~Live Blackjack:View More~Live Baccarat:View All");


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Host Offers");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Host Offers");
		
		
		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Roulette");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Roulette");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();


		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Blackjack");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Blackjack");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Baccarat");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Baccarat");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Live Casino Poker");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Live Casino Poker");
		PageObjectManager(PageValidation.class).verifyGamesOnSlotsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Game Shows");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Game Shows");
		PageObjectManager(PageValidation.class).verifyGamesOnGameshowsPage();

		PageObjectManager(PageValidation.class).clickSecondaryTABdisplayed("Grosvenor Exclusive");
		PageObjectManager(PageValidation.class).verifySecondaryTabDisplayedAsSelectedOrNot("Grosvenor Exclusive");
		PageObjectManager(PageValidation.class).verifyHomepageSectionHeadersdisplayed("tux:Exclusive Studio Tables~tux:Exclusively from Grosvenor Victoria Casino");
		PageObjectManager(PageValidation.class).verifyGamesOnGameshowsPage();

		PageObjectManager(LiveCasinosPage.class).clickSecondaryTABdisplayed("Live Blackjack");
		PageObjectManager(LiveCasinosPage.class).clickInfoIcon();
		
		PageObjectManager(LiveCasinosPage.class).verifyNameOnInfoPage("Blackjack");
		PageObjectManager(LiveCasinosPage.class).clickJoinBTNOnInfoPage();
		
		PageObjectManager(SearchPage.class).verifyLoginPopupDisplayed();
        PageObjectManager(MainPage.class).setUserName(dataPool("UserName"));
		PageObjectManager(MainPage.class).setPassword(dataPool("Password"));
		PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
		
		PageObjectManager(LiveCasinosPage.class).verifyFirstTableonliveBlackjackDisplayed();
	
	}
	
	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
	 
}
	
	
	
	
	

