package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.view.MailinatorView;
import com.view.ResponsibleGamblingView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test32_SelfExcludeFromBrand_SAWCustomer extends BaseTest {
	
	@TestCaseId("Test32")
	@Description("Self-Exclusion Functionality- SAW Customer - Check whether SAW user able to Online Self exclude from site")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc32_SelfExcludeFromBrand_SAWCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
		getWebDomains().launchURLForGCWeb();
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		PageObjectManager(ResponsibleGamblingView.class).SelfExclusionBrandLevel_SAWCustomer(dataPool("UserName"), dataPool("Password") ,dataPool("ExclusionPeriod"));
		
		getWaitMethods().sleep(9);
		PageObjectManager(ResponsibleGamblingView.class).PAMlogin(dataPool("PAMUsername"), dataPool("PAMPassword"));
		PageObjectManager(ResponsibleGamblingView.class).PAMSearchCustomer(dataPool("UserName"), dataPool("accStatusPAM"));
		PageObjectManager(MailinatorView.class).VerifySelfExclusionEMailMailinatorAccount(dataPool("MailinatorLoginEmail"),dataPool("MailinatorLoginPassword"),dataPool("EmailAddress"));
	
  	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		//tearDownWebEnvironment(iTestResult); 
	}
}
