package com.scripts.regression;

import java.util.Hashtable;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.generic.utils.GenericUtils;
import com.pagefactory.grosvenor.MainPage;
import com.view.LoginLogoutView;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test6_LoginFlogPIN_SAWCustomer extends BaseTest 
{	 
	@TestCaseId("Test6")
	@Description("Login - FLOG - Digital Only Customer - FLOG PIN_Check system send reset PIN link on registrated "
			+ "Email Address when user selects Forgotten PIN Option form.")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc6_LoginFlogPassword_SAWCustomer(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet);
		getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		String pin = GenericUtils.getRandomNumericBetweenTwo(1000, 2000);
		pin = (pin.equals("1234") ? GenericUtils.getRandomNumericBetweenTwo(1000, 2000) : pin);
 	 	PageObjectManager(LoginLogoutView.class).verifyForgottenPINFuntionalityForSAWCustomer(
	 			 dataPool("UserName"), dataPool("EmailAddress"), dataPool("CardNumber"), pin);
 	}

	@AfterMethod(groups = {"Web"})
	private void tearDown(ITestResult iTestResult){
		tearDownWebEnvironment(iTestResult); 
	}
}