package com.scripts.regression;
import java.util.Hashtable;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.generic.BaseTest;
import com.generic.customAnnotation.TestDataSource;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.SearchPage;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;

public class Test121_GamePlay_Check_that_system_redirects_to_Login_window_when_clicked_on_free_play_or_Real_play_mode_in_log_out_mode extends BaseTest 
{
	@TestCaseId("Test121")
	@Description("Game Play_Check that system redirects to Login window when clicked on free play or Real play mode in log out mode ")
	@TestDataSource(dataSource = "/Regression/Regression")
	@Test(dataProvider = "TestDataProvider", groups = {"Web"})
	public void tc121_CheckthatsystemredirectstoLoginwindowwhenclickedonfreeplayorRealplaymodeinlogoutmode(Hashtable<String, String> dataSet)
	{
		initializeWebEnvironment(dataSet); 
        getWebDomains().launchURLForGCWeb();
		
		
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(SearchPage.class).verifySearchButtonDisplayed();
		PageObjectManager(SearchPage.class).clickSearchButton();
		PageObjectManager(SearchPage.class).searchGame("genie jackpots");
		PageObjectManager(SearchPage.class).verifySearchResultsDisplayed();
		
		PageObjectManager(SearchPage.class).clickOnInfoIconInSearchResult();
		PageObjectManager(SearchPage.class).verifyPlayButtonDisplayed();
		PageObjectManager(SearchPage.class).verifyButton("Demo Play");
		PageObjectManager(SearchPage.class).clickPlayButton();
		PageObjectManager(SearchPage.class).verifyLoginPopupDisplayed();
	}

	@AfterMethod(groups = {"Web"})
	public void tearDown(ITestResult iTestResult){
 		tearDownWebEnvironment(iTestResult); 
	}

}
