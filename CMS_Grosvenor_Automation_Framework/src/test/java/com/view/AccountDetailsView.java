package com.view;
import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.grosvenor.AccountDetailsPage;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;


import ru.yandex.qatools.allure.annotations.Step;

public class AccountDetailsView extends BaseView {
	
	private Pojo pojo;
	
	public AccountDetailsView(Pojo objPojo) {
		super(objPojo);
		
	}
	
	@Step("Account details page: user is unable to edit Name and DOB , able to edit Email and Mobile number ")
 	public void AccountDetails_ValidateLastLoginTime(String CustUserName, String CustPassword) 
 	{
 		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
 		PageObjectManager(AccountDetailsPage.class).clickAccountDetail();
 		PageObjectManager(AccountDetailsPage.class).getLastLoginFormat();
 		PageObjectManager(AccountDetailsPage.class).validateLastLoginFormatDisplayed();
 	}
	
	@Step("Account details page: user is unable to edit Name and DOB , able to edit Email and Mobile number ")
 	public void AccountDetails_UpdateDetails_Digital(String CustUserName, String CustPassword) 
 	{
 		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
 		PageObjectManager(AccountDetailsPage.class).clickAccountDetail();
 		PageObjectManager(AccountDetailsPage.class).getLastLoginFormat();
 		
 		PageObjectManager(AccountDetailsPage.class).verifyLabels("Last login,Name,Email,Date of birth,Phone,Postcode");
 		PageObjectManager(AccountDetailsPage.class).verifyEditlinkDisplayed();
 		PageObjectManager(AccountDetailsPage.class).clickEditLink();
 		PageObjectManager(AccountDetailsPage.class).verifyLabels("Name,Date of birth");
 		PageObjectManager(AccountDetailsPage.class).verifyNonEditableTXTDisplayed();
 		PageObjectManager(AccountDetailsPage.class).EditEmail();
 		PageObjectManager(AccountDetailsPage.class).EditPhoneNumber();
 		PageObjectManager(AccountDetailsPage.class).inputPassword(CustPassword);
 		PageObjectManager(AccountDetailsPage.class).showPasswordBTNdisplayed();
 		PageObjectManager(AccountDetailsPage.class).clickUpdateBtn();
 		PageObjectManager(AccountDetailsPage.class).verifyAccountUpdateSucsessMSGdisplayed("Account details updated");
 		
 	}
	
	

	@Step("Account details page: user able to Change Password without any issues ")
 	public void AccountDetails_ChangePassword_Digital(String CustUserName, String CustPassword , String newpwd) 
 	{
		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
 		PageObjectManager(AccountDetailsPage.class).clickAccountDetail();
		PageObjectManager(AccountDetailsPage.class).clickChangePassword();
		PageObjectManager(AccountDetailsPage.class).setCurrentPassword(CustPassword);
		PageObjectManager(AccountDetailsPage.class).showPasswordBTNdisplayed();
		PageObjectManager(AccountDetailsPage.class).setNewPassword(newpwd);
		PageObjectManager(AccountDetailsPage.class).clickUpdatePasswordBTN();
		PageObjectManager(AccountDetailsPage.class).verifyAccountUpdateSucsessMSGdisplayed("Password updated");
		PageObjectManager(AccountDetailsPage.class).clickLogoutLink();
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).verifyLoginPopupDisplayed();
		PageObjectManager(MainPage.class).setUserName(CustUserName);
		PageObjectManager(MainPage.class).setPassword(newpwd);
		PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		
		
 	}
	
	@Step("Account details page: user able to Update Marketing Preferences without any issues ")
 	public void AccountDetails_UpdateMarketingPreferences_Digital(String CustUserName, String CustPassword , String mktpref) 
 	{
		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
 		PageObjectManager(AccountDetailsPage.class).clickAccountDetail();
		PageObjectManager(AccountDetailsPage.class).clickMarketingPreferences();
		PageObjectManager(AccountDetailsPage.class).verifyMarketingInfoText();
		PageObjectManager(AccountDetailsPage.class).updateMarketingPreferences(mktpref);
		PageObjectManager(AccountDetailsPage.class).clickUpdatePreferencesBtn();
		PageObjectManager(AccountDetailsPage.class).verifyAccountUpdateSucsessMSGdisplayed("Account details updated");
 	}
	
	
	
	
	

}
