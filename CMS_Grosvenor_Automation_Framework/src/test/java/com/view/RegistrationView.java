package com.view;

import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.grosvenor.DepositLimitPage;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.SignUpPage;

import ru.yandex.qatools.allure.annotations.Step;

public class RegistrationView extends BaseView
{ 
 	public RegistrationView(Pojo pojo){
		super(pojo); 
 	 }
    
 
	@Step("Registration of new user")
	public void newUserRegistration(String emailAddress, String userName, String password,
			String title, String firstName, String surName, String dateOfBirth, String country,
			String addressLine1, String addressLine2, String townCity, String postCode, 
			String mobileNumber, String marketingPreferences) 
	{
 		PageObjectManager(MainPage.class).clickJoinNow();
 		PageObjectManager(SignUpPage.class).verifySignUpHeaderDisplayed();
 		PageObjectManager(SignUpPage.class).selectNewUserYes();
 	 	PageObjectManager(SignUpPage.class).setEmailAddress(emailAddress);
 		PageObjectManager(SignUpPage.class).selectJoinnowAgreeCheckbox();
 		PageObjectManager(SignUpPage.class).clickNext();
 		PageObjectManager(SignUpPage.class).setUserName(userName);
 		PageObjectManager(SignUpPage.class).setPassword(password);
 		PageObjectManager(SignUpPage.class).selectTitle(title);
 		PageObjectManager(SignUpPage.class).setFirstName(firstName);
 		PageObjectManager(SignUpPage.class).setSurName(surName);
 		PageObjectManager(SignUpPage.class).selectDateOfBirth(dateOfBirth);
 		PageObjectManager(SignUpPage.class).selectCountry(country);
 		PageObjectManager(SignUpPage.class).setmanualAddress(addressLine1, addressLine2,
 				townCity, country, postCode);
 		PageObjectManager(SignUpPage.class).setMobileNumber(mobileNumber);
 		PageObjectManager(SignUpPage.class).setMarketingPreferences(marketingPreferences);
 		PageObjectManager(SignUpPage.class).selectDepositLimitNo();
 		PageObjectManager(SignUpPage.class).clickRegister();
 		PageObjectManager(SignUpPage.class).clickCloseSignUp();
 		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
 		PageObjectManager(MainPage.class).clickMyAccount();
   		PageObjectManager(MainPage.class).verifyUserLoggedInSuccessfully(userName);
	} 
	@Step("	Register user with deposit limit")
	public void newUserRegisterWithDepositLimit(String emailAddress, String userName, String password,
			String title, String firstName, String surName, String dateOfBirth, String country,
			String addressLine1, String addressLine2, String townCity, String postCode, 
			String mobileNumber,String dailyLmt,String weeklyLmt,String monthlyLmt) 
	{
 		PageObjectManager(MainPage.class).clickJoinNow();
 		PageObjectManager(SignUpPage.class).verifySignUpHeaderDisplayed();
 		//PageObjectManager(SignUpPage.class).selectNewUserYes();
 	 	PageObjectManager(SignUpPage.class).setEmailAddress(emailAddress);
 		//PageObjectManager(SignUpPage.class).selectJoinnowAgreeCheckbox();
 		PageObjectManager(SignUpPage.class).clickNext();
 		PageObjectManager(SignUpPage.class).setUserName(userName);
 		PageObjectManager(SignUpPage.class).setPassword(password);
 		PageObjectManager(SignUpPage.class).selectTitle(title);
 		PageObjectManager(SignUpPage.class).setFirstName(firstName);
 		PageObjectManager(SignUpPage.class).setSurName(surName);
 		PageObjectManager(SignUpPage.class).selectDateOfBirth(dateOfBirth);
 		PageObjectManager(SignUpPage.class).selectCountry(country);
 		PageObjectManager(SignUpPage.class).setMobileNumber(mobileNumber);
 		
 		PageObjectManager(SignUpPage.class).setmanualAddress(addressLine1, addressLine2,
 				townCity, country, postCode);
 		//PageObjectManager(SignUpPage.class).setMarketingPreferences(marketingPreferences);
 		
 		PageObjectManager(SignUpPage.class).selectDepositLimitYes();
 		//PageObjectManager(SignUpPage.class).setDepositLimit("Daily", "9");
 		//PageObjectManager(SignUpPage.class).setDepositLimit("Weekly", "3");
 		//PageObjectManager(SignUpPage.class).setDepositLimit("Monthly", "2");
 		//PageObjectManager(SignUpPage.class).clickRegister();
 		//PageObjectManager(SignUpPage.class).verifyErrorMessage("Your weekly limit must be greater than your daily limit~Your monthly limit must be greater than your weekly limit~Your monthly limit must be greater than your daily limit~Your weekly limit must be greater than £5");
 		//PageObjectManager(SignUpPage.class).verifyErrorMessage("The daily limit must be less than the weekly limit~The weekly limit must be less than the monthly limit~The daily limit must be less than the monthly limit~The deposit value must be greater than £5");
 		
 		PageObjectManager(SignUpPage.class).setDepositLimit("Daily", dailyLmt);
 		PageObjectManager(SignUpPage.class).setDepositLimit("Weekly", weeklyLmt);
 		PageObjectManager(SignUpPage.class).setDepositLimit("Monthly",monthlyLmt);
 		PageObjectManager(SignUpPage.class).clickRegister();
 		
 		PageObjectManager(SignUpPage.class).verifySignUpHeaderDisplayed();
 		}

}
