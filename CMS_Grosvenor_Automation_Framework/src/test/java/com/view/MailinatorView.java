package com.view;

import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.mailinator.MailinatorPage;
import com.pagefactory.grosvenor.MainPage;

import ru.yandex.qatools.allure.annotations.Step;

public class MailinatorView extends BaseView{
	
	private Pojo pojo;
	public MailinatorView(Pojo pojo){
		super(pojo); 
		this.pojo = pojo;
	}
	
	
	@Step("Verify Login to Mailinator inbox")
	public void VerifyLoginToMailinatorAccount(String loginEmail , String loginPassword ,String emailAddress) 
	{
	pojo.getWebDomains().launchURLForMailinator();
	PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
	PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
	PageObjectManager(MailinatorPage.class).clickGO();
	PageObjectManager(MailinatorPage.class).verifyLoginlnkDisplayed();
	PageObjectManager(MailinatorPage.class).clickLoginlnk();
	PageObjectManager(MailinatorPage.class).setLoginEmail(loginEmail);
	PageObjectManager(MailinatorPage.class).setPassword(loginPassword);
	PageObjectManager(MailinatorPage.class).clickLoginBtn();
	
	}

	@Step("Verify Forgotten Username Email is received in Mailinator inbox")
	public void VerifyEmailOnMailinatorAccount(String emailAddress , String username) 
	{
		pojo.getWebDomains().launchURLForMailinator();
		PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
		PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
		PageObjectManager(MailinatorPage.class).clickGO();
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).verifyForgotUserNameMailReceived();
		PageObjectManager(MailinatorPage.class).openForgotUsernameMail();
		PageObjectManager(MailinatorPage.class).validateUsernameinEmail(username);
		
	} 
	
	@Step("Verify Forgotten Password Email is received in Mailinator inbox")
	public void VerifyPasswordResetLinkOnMailinatorAccount(String loginEmail , String loginPassword ,String emailAddress ) 
	{
		PageObjectManager(MailinatorView.class).VerifyLoginToMailinatorAccount(loginEmail, loginPassword, emailAddress);		
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).verifyForgotPasswordMailReceived();
		PageObjectManager(MailinatorPage.class).openForgotPasswordMail();
		pojo.getWebActions().switchToFrameUsingNameOrId("msg_body");
		
	}
	
	@Step("Verify Forgotten Membership Number Email is received in Mailinator inbox")
	public void VerifyForgottenCardNumberMailMailinatorAccount(String emailAddress ,String cardnumber ) 
	{
		pojo.getWebDomains().launchURLForMailinator();
		PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
		PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
		PageObjectManager(MailinatorPage.class).clickGO();
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).verifyForgotCardNumberMailReceived();
		PageObjectManager(MailinatorPage.class).openForgotCardNumberMail();
		PageObjectManager(MailinatorPage.class).validateCardNumberinEmail(cardnumber);
		
	}
	
	@Step("Verify Forgotten PIN Email is received in Mailinator inbox")
	public void VerifyForgottenPINMailMailinatorAccount(String loginEmail , String loginPassword ,String emailAddress ) 
	{
		PageObjectManager(MailinatorView.class).VerifyLoginToMailinatorAccount(loginEmail, loginPassword, emailAddress);
		
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).openForgotPINMail();
		pojo.getWebActions().switchToFrameUsingNameOrId("msg_body");
	}
	
	@Step("Verify Self Exclusion Email is received in Mailinator inbox")
	public void VerifySelfExclusionEMailMailinatorAccount(String loginEmail , String loginPassword ,String cust_emailAddress ) 
	{
		PageObjectManager(MailinatorView.class).VerifyLoginToMailinatorAccount(loginEmail, loginPassword, cust_emailAddress);
		
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).openSelfExclusionMail();
		pojo.getWebActions().switchToFrameUsingNameOrId("msg_body");
		
		PageObjectManager(MailinatorPage.class).verifyExclusionONDigitalInfoDisplayed();
	
	}


}
