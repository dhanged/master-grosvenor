package com.view;

import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.bedePAM.Bede_LoginPage;
import com.pagefactory.bedePAM.Bede_MainPage;
import com.pagefactory.bedePAM.Bede_PlayerManagementPage;

import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.grosvenor.MyAccountPage;
import com.pagefactory.grosvenor.ResponsibleGamblingPage;

import ru.yandex.qatools.allure.annotations.Step;

public class ResponsibleGamblingView extends BaseView {

	private Pojo pojo;	
	public ResponsibleGamblingView(Pojo pojo){
		super(pojo); 
		this.pojo = pojo;
	}
	
	@Step("PAM User Login")
	public void PAMlogin(String userName, String password) 
	{
		pojo.getWebDomains().launchURLForPAMWeb();
		PageObjectManager(Bede_LoginPage.class).setUserName(userName);
		PageObjectManager(Bede_LoginPage.class).setPassword(password);
		PageObjectManager(Bede_LoginPage.class).clickSignIn();
	} 
 	
 	

	@Step("PAM Search customer ")
	
 	public void PAMSearchCustomer(String userName , String accStatus ) 
	{
 		PageObjectManager(Bede_MainPage.class).clickPlayerManagementLink();
 		PageObjectManager(Bede_PlayerManagementPage.class).setPlayerSearch(userName);
		PageObjectManager(Bede_PlayerManagementPage.class).clickSearch();
		PageObjectManager(Bede_PlayerManagementPage.class).verifyAccountStatus(accStatus);
		PageObjectManager(Bede_PlayerManagementPage.class).ChangeAccountStatus(accStatus);
		
		
		
		
	}
 	
	@Step("Navigate to MyAccount section  ")
	
 	public void NavigateMyAccount(String CustUserName , String CustPassword ) 
	{
	PageObjectManager(MainPage.class).clickLogin();
	PageObjectManager(MainPage.class).verifyLoginPopupDisplayed();
	PageObjectManager(MainPage.class).setUserName(CustUserName);
	PageObjectManager(MainPage.class).setPassword(CustPassword);
	PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
	PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
	PageObjectManager(MainPage.class).clickMyAccount();
	}
 	
 	@Step("Reality Checks Functionality ")
 	public void RealityChecksFunctionality_Digital(String CustUserName, String CustPassword ,String ReminderPeriod) 
 	{
 		PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
		PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Reality Check");
		PageObjectManager(ResponsibleGamblingView.class).VerifyInfoMessagesRealityChecks();
		PageObjectManager(ResponsibleGamblingPage.class).verifyTellMeMorelinkDisplayed();
		PageObjectManager(ResponsibleGamblingPage.class).verifyTellMeMoreExpandBtnDisplayed();
		
		PageObjectManager(ResponsibleGamblingPage.class).clickTellMeMoreExpandBtn();
		PageObjectManager(ResponsibleGamblingPage.class).clickTellMeMoreCollapseBtn();
		PageObjectManager(ResponsibleGamblingPage.class).verifyReminderOptionsDispalyed("No~30 mins~1 hour~2 hours");
		
		PageObjectManager(ResponsibleGamblingPage.class).SelectReminderperiod(ReminderPeriod);
		PageObjectManager(ResponsibleGamblingPage.class).verifySaveChangesBTNDisplayed();
		PageObjectManager(ResponsibleGamblingPage.class).clickSaveChangesBTN();
		PageObjectManager(ResponsibleGamblingPage.class).verifySavedBtnDisplayed();

 		
 	}
 	
 	@Step("Verify information texts displayed on Reality Checks screen")
	
	public void VerifyInfoMessagesRealityChecks() 
	{
	PageObjectManager(ResponsibleGamblingPage.class).verifyRealityChecksInfoMessages("Reality Checks are a helpful way of keeping track of the time you have spent playing our games.");
	PageObjectManager(ResponsibleGamblingPage.class).verifyRealityChecksInfoMessages("You can choose how often you would like to receive a Reality Check below. You will see a reminder each time you reach your chosen Reality Check time interval");
	PageObjectManager(ResponsibleGamblingPage.class).verifyRealityChecksInfoMessages("Your gaming session will begin when you place your first real money bet and will end when you log out.");
	
	}
	
	
	@Step("Take a Break Functionality E2E")
	public void TakeABreakFunctionality_Digital(String CustUserName, String CustPassword ,String brkPeriod) 
	{
	PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
	PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Take a break");
	PageObjectManager(ResponsibleGamblingPage.class).verifyInfoMsgTakeBreakDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhyTakeABreaklnkDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyHowLongWouldTakeBreakMsgDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).selectTakeABreakPeriod(brkPeriod);
	PageObjectManager(ResponsibleGamblingView.class).VerifyInfoMessagesTakeaBreak();
	
	PageObjectManager(ResponsibleGamblingPage.class).verifyPasswordFieldDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).inputPassword(CustPassword);
	PageObjectManager(ResponsibleGamblingPage.class).ClickTakeBreakBtnDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyConfirmationPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyTakeBreakAndLogoutBtnOnPopup();
	PageObjectManager(ResponsibleGamblingPage.class).verifyCancelBtnOnPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickTakeBreakAndLogoutBtn();
	PageObjectManager(MainPage.class).verifyUserLoggedOutSuccessfuly();
	PageObjectManager(MainPage.class).clickLogin();
	PageObjectManager(MainPage.class).setUserName(CustUserName);
	PageObjectManager(MainPage.class).setPassword(CustPassword);
	PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
	PageObjectManager(ResponsibleGamblingPage.class).verifyErrorMessageDisplayedOnLogin("The user is on a break");
	
	}
	
	@Step("Verify information texts displayed on Take a Break screen")
	
	public void VerifyInfoMessagesTakeaBreak() 
	{
	PageObjectManager(ResponsibleGamblingPage.class).verifyInfoMessages("• The break will take effect immediately.");
	PageObjectManager(ResponsibleGamblingPage.class).verifyInfoMessages("• You will not be able to access your account.");
	PageObjectManager(ResponsibleGamblingPage.class).verifyInfoMessages("• Marketing emails will stop within 24 hours.");
	PageObjectManager(ResponsibleGamblingPage.class).verifyInfoMessages("• You can request any other length of period by contacting our customer service team");
	PageObjectManager(ResponsibleGamblingPage.class).verifyInfoMessages("• Your break will end automatically once the selected time period has passed");
	}

	
	@Step("Self Exclude Functionality E2E")
	public void SelfExclusionFunctionality_Digital(String CustUserName, String CustPassword ,String exclPeriod) 
	{
	PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
	PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Self Exclude");
		
	PageObjectManager(ResponsibleGamblingPage.class).verifyDoUFeelProblemWithGamblingTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnYesSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnNoSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickYesbtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyYouCanBlockURSelfTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhySelfExcludeTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifyHowLongDoUWantToLockACTxt();
	PageObjectManager(ResponsibleGamblingPage.class).selectSelfExcludePeriod(exclPeriod);
	PageObjectManager(ResponsibleGamblingView.class).VerifyInfoMessagesSelfExclude();
	PageObjectManager(ResponsibleGamblingPage.class).validateExclusionPeriod(exclPeriod);
	
	PageObjectManager(ResponsibleGamblingPage.class).inputPassword(CustPassword);
	PageObjectManager(ResponsibleGamblingPage.class).verifyWantToSelfExcludeBtnDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickWantToSelfExcludeBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyExclusionConfirmationPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyCancelBtnOnPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExcludeNLogOutBtnOnPopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickSelfExcludeNLogOutBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclusionCompletePopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickOKonPopup();
	PageObjectManager(MainPage.class).verifyUserLoggedOutSuccessfuly();
	PageObjectManager(MainPage.class).clickLogin();
	PageObjectManager(MainPage.class).setUserName(CustUserName);
	PageObjectManager(MainPage.class).setPassword(CustPassword);
	PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
	PageObjectManager(ResponsibleGamblingPage.class).verifyErrorMessageDisplayedOnLogin("The user is self excluded.");
	
	}
	@Step("Verify information texts displayed on Self Exclude screen")

	public void VerifyInfoMessagesSelfExclude() 
	{
		PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclInfoMessages(" All of your accounts on our other existing websites will also be locked immediately");
		PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclInfoMessages(" It will not be possible to unlock your account prior to the time period ");
		PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclInfoMessages(" Once the selected period has passed, your account will remain deactivated");
		PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclInfoMessages(" Marketing communications will stop within 24 hours");
		PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclInfoMessages(" We recommend that you self-exclude from any other gambling operators’ websites that you may use");
		PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclInfoMessages(" You accept that if you open a new account whilst self-excluded, you are personally responsible for any losses you incur and not entitled to any winnings");
		
	}
	

	@Step("Self Exclude From Online-Digital channel for SAW customer")
	public void SelfExclusionFunctionality_SAWCustomer(String CustUserName, String CustPassword ,String exclPeriod) 
	{
	
	PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
	PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Self Exclude");
		
	PageObjectManager(ResponsibleGamblingPage.class).verifyDoUFeelProblemWithGamblingTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnYesSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnNoSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickYesbtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyYouCanBlockURSelfTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhySelfExcludeTXT();
	
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhichChannelToSelectTXTDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnOnlineChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelOnlineChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnRetailChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelInClubChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnBothChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelBothChannelDisplayed();
	
	PageObjectManager(ResponsibleGamblingPage.class).SelectradioBtnDigitalChannel();
	PageObjectManager(ResponsibleGamblingPage.class).verifyTXTdisplayedOnlineSelectedDisplayed();
	
	PageObjectManager(ResponsibleGamblingPage.class).verifyHowLongDoUWantToLockACTxt();
	PageObjectManager(ResponsibleGamblingPage.class).selectSelfExcludePeriod(exclPeriod);
	PageObjectManager(ResponsibleGamblingView.class).VerifyInfoMessagesSelfExclude();
	PageObjectManager(ResponsibleGamblingPage.class).validateExclusionPeriod(exclPeriod);
	
	PageObjectManager(ResponsibleGamblingPage.class).inputPassword(CustPassword);
	PageObjectManager(ResponsibleGamblingPage.class).verifyWantToSelfExcludeBtnDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickWantToSelfExcludeBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyExclusionConfirmationPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyCancelBtnOnPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExcludeNLogOutBtnOnPopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickSelfExcludeNLogOutBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclusionCompletePopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickOKonPopup();
	PageObjectManager(MainPage.class).verifyUserLoggedOutSuccessfuly();
	PageObjectManager(MainPage.class).clickLogin();
	PageObjectManager(MainPage.class).setUserName(CustUserName);
	PageObjectManager(MainPage.class).setPassword(CustPassword);
	PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
	PageObjectManager(ResponsibleGamblingPage.class).verifyErrorMessageDisplayedOnLogin("The user is self excluded.");
	
	}
	
	@Step("Self Exclude From Cross/Brand level channel for SAW customer")
	public void SelfExclusionBrandLevel_SAWCustomer(String CustUserName, String CustPassword ,String exclPeriod) 
	{
	
	PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
	PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Self Exclude");
		
	PageObjectManager(ResponsibleGamblingPage.class).verifyDoUFeelProblemWithGamblingTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnYesSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnNoSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickYesbtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyYouCanBlockURSelfTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhySelfExcludeTXT();
	
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhichChannelToSelectTXTDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnOnlineChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelOnlineChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnRetailChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelInClubChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnBothChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelBothChannelDisplayed();
	
	PageObjectManager(ResponsibleGamblingPage.class).SelectradioBtnBothChannel();
	PageObjectManager(ResponsibleGamblingPage.class).verifyTXTdisplayedBOTHSelectedDisplayed();
	
	PageObjectManager(ResponsibleGamblingPage.class).verifyHowLongDoUWantToLockACTxt();
	PageObjectManager(ResponsibleGamblingPage.class).selectSelfExcludePeriod(exclPeriod);
	PageObjectManager(ResponsibleGamblingView.class).VerifyInfoMessagesSelfExclude();
	PageObjectManager(ResponsibleGamblingPage.class).validateExclusionPeriod(exclPeriod);
	
	PageObjectManager(ResponsibleGamblingPage.class).inputPassword(CustPassword);
	PageObjectManager(ResponsibleGamblingPage.class).verifyWantToSelfExcludeBtnDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickWantToSelfExcludeBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyAreUSurePopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyCancelBtnOnPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExcludeNLogOutBtnOnPopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickSelfExcludeNLogOutBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclusionCompletePopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickOKonPopup();
	PageObjectManager(MainPage.class).verifyUserLoggedOutSuccessfuly();
	PageObjectManager(MainPage.class).clickLogin();
	PageObjectManager(MainPage.class).setUserName(CustUserName);
	PageObjectManager(MainPage.class).setPassword(CustPassword);
	PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
	PageObjectManager(ResponsibleGamblingPage.class).verifyErrorMessageDisplayedOnLogin("The user is self excluded.");
	
	}
	
	@Step("Self Exclude From Retail channel for SAW customer")
	public void SelfExclusionRetailLevel_SAWCustomer(String CustUserName, String CustPassword ,String exclPeriod) 
	{
	
	PageObjectManager(ResponsibleGamblingView.class).NavigateMyAccount(CustUserName, CustPassword);
	PageObjectManager(MyAccountPage.class).selectMyAccountMenu("Responsible Gambling","Self Exclude");
		
	PageObjectManager(ResponsibleGamblingPage.class).verifyDoUFeelProblemWithGamblingTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnYesSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifybtnNoSelfExcludeDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickYesbtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyYouCanBlockURSelfTXT();
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhySelfExcludeTXT();
	
	PageObjectManager(ResponsibleGamblingPage.class).verifyWhichChannelToSelectTXTDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnOnlineChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelOnlineChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnRetailChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelInClubChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyradioBtnBothChannelDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyLabelBothChannelDisplayed();
	
	PageObjectManager(ResponsibleGamblingPage.class).SelectradioBtnRetailChannel();
	PageObjectManager(ResponsibleGamblingPage.class).verifyTXTdisplayedRetailSelectedDisplayed();
	
	PageObjectManager(ResponsibleGamblingPage.class).verifyHowLongDoUWantToLockACTxt();
	PageObjectManager(ResponsibleGamblingPage.class).selectSelfExcludePeriod(exclPeriod);
	PageObjectManager(ResponsibleGamblingView.class).VerifyInfoMessagesSelfExclude();
	PageObjectManager(ResponsibleGamblingPage.class).validateExclusionPeriod(exclPeriod);
	
	PageObjectManager(ResponsibleGamblingPage.class).inputPassword(CustPassword);
	PageObjectManager(ResponsibleGamblingPage.class).verifyWantToSelfExcludeBtnDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).clickWantToSelfExcludeBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifyExcludeRetail_AreUSurePopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifyCancelBtnOnPopupDisplayed();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExcludeBtnOnPopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickSelfExcludeBtn();
	PageObjectManager(ResponsibleGamblingPage.class).verifySelfExclusionCompletePopup();
	PageObjectManager(ResponsibleGamblingPage.class).clickOKonPopup();
	PageObjectManager(MainPage.class).clickMyAccount();
	
	PageObjectManager(MainPage.class).verifyUserLoggedInSuccessfully(CustUserName);
	}
	
}
