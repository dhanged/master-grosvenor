package com.view;

import org.openqa.selenium.By;

import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.grosvenor.MyAccountPage;

import ru.yandex.qatools.allure.annotations.Step;

public class CashierView extends BaseView {

	private Pojo pojo;	
	public CashierView(Pojo pojo){
		super(pojo); 
		this.pojo = pojo;
	}

	@Step("Cashier - First Deposit using > Card")
	public void depositFunctionality_FirstDepositUsingCard(String cardNumber, String expMon,String expYr, String securityCodeCVV, String amount)
	{
		PageObjectManager(MyAccountPage.class).verifysetDepLimitsLnkDisplayed();
		PageObjectManager(MyAccountPage.class).verifyliveHelpLnkDisplayed();
		PageObjectManager(MyAccountPage.class).verifywelcomeBonusHDRDisplayed();
		PageObjectManager(MyAccountPage.class).setDepositAmt(amount);
		PageObjectManager(MyAccountPage.class).verifyminMaxDepAmtTXT();
		PageObjectManager(MyAccountPage.class).firstDepositPoPFAgree();
		pojo.getWebActions().switchToFrameUsingNameOrId("payment-process");
		PageObjectManager(MyAccountPage.class).setCardNumber(cardNumber);
		PageObjectManager(MyAccountPage.class).setExpMonth(expMon);
		PageObjectManager(MyAccountPage.class).setExpYear(expYr);
		PageObjectManager(MyAccountPage.class).setCVVcode(securityCodeCVV);
		PageObjectManager(MyAccountPage.class).clickDeposit();
		PageObjectManager(MyAccountPage.class).validateSuccessMessage();

	}

	@Step("Cashier - Deposit Using > Card")
	public void depositFunctionality_UsingCard(String cardNumber, String expMon,String expYr, String securityCodeCVV, String amount)
	{
		PageObjectManager(MyAccountPage.class).verifyliveHelpLnkDisplayed();
		PageObjectManager(MyAccountPage.class).verifyDepositNowHDRDisplayed();
		PageObjectManager(MyAccountPage.class).setDepositAmt(amount);
		PageObjectManager(MyAccountPage.class).verifyminMaxDepAmtTXT();
		PageObjectManager(MyAccountPage.class).firstDepositPoPFAgree();
		pojo.getWebActions().switchToFrameUsingNameOrId("payment-process");
		PageObjectManager(MyAccountPage.class).setCardNumber(cardNumber);
		PageObjectManager(MyAccountPage.class).setExpMonth(expMon);
		PageObjectManager(MyAccountPage.class).setExpYear(expYr);
		PageObjectManager(MyAccountPage.class).setCVVcode(securityCodeCVV);
		PageObjectManager(MyAccountPage.class).clickDeposit();
		PageObjectManager(MyAccountPage.class).validateSuccessMessage();

	}

	@Step("Cashier -Withdraw functionality > card")
	public void WithdrawFunctionality_UsingCard(String amount, String password)
	{
		//PageObjectManager(MyAccountPage.class).verifywithdrawalAmount();
		//PageObjectManager(MyAccountPage.class).verifyMinimumof£10unlesyourbalanceislessthan£10();
		//PageObjectManager(MyAccountPage.class).verifyWithdrwalInfotext();
		PageObjectManager(MyAccountPage.class).setWithdrawAmount(amount);
		PageObjectManager(MyAccountPage.class).setPasswordToWithdraw(password);
		PageObjectManager(MyAccountPage.class).clickNext();
		//if(PageObjectManager(MyAccountPage.class).verifyAreYouSureToWithdrawHeadeDisplayed())
		{PageObjectManager(MyAccountPage.class).clickContinue();}
		pojo.getWebActions().switchToFrameUsingNameOrId("withdrawal-process");
		PageObjectManager(MyAccountPage.class).selectWithdrawMethod("card");
		//PageObjectManager(MyAccountPage.class).clickOnNext();
		//PageObjectManager(MyAccountPage.class).verifyWithdrawalSuccessMessage(amount);
	}
	
	
	
}

