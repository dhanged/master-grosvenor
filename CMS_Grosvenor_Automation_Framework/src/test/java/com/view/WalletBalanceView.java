package com.view;

import com.generic.BaseView;
import com.generic.Pojo;

import com.pagefactory.bedePAM.Bede_UpdateBalance;
import com.pagefactory.grosvenor.HomePage;
import com.pagefactory.grosvenor.MainPage;

import ru.yandex.qatools.allure.annotations.Step;

public class WalletBalanceView extends BaseView {
	
	private Pojo pojo;	
	public WalletBalanceView(Pojo pojo){
		super(pojo); 
		this.pojo = pojo;
	}

	@Step("PAM Manage Balance: Add money to the specified wallet")
	public String PAM_AddBalanceToWallet(String WalletType ,String amount) 
	{
		PageObjectManager(Bede_UpdateBalance.class).verifyManageBalanceTABdisplayed();
		PageObjectManager(Bede_UpdateBalance.class).clickManageBalanceTAB();
		PageObjectManager(Bede_UpdateBalance.class).verifyLabels("Wallet,Amount,Reason");
		PageObjectManager(Bede_UpdateBalance.class).clickWalletType();
		PageObjectManager(Bede_UpdateBalance.class).selectWalletType(WalletType);
		PageObjectManager(Bede_UpdateBalance.class).setAmount(amount);
		PageObjectManager(Bede_UpdateBalance.class).clickReasonType();
		PageObjectManager(Bede_UpdateBalance.class).selectReasonType("CC - Goodwill Gesture");
		PageObjectManager(Bede_UpdateBalance.class).verifyNoteLabelDisplayed();
		PageObjectManager(Bede_UpdateBalance.class).setNote("Automation Test - Add balance");
		PageObjectManager(Bede_UpdateBalance.class).clickApplyBTN();
		//PageObjectManager(PAM_ManageBalance.class).verifyLabelsOnManageBalancePopup("Wallet name,Current Wallet Balance,Adjustment Amount,New Wallet Balance");
		String NewBalance = PageObjectManager(Bede_UpdateBalance.class).validateBalanceDisplayed();
		PageObjectManager(Bede_UpdateBalance.class).clickconfirmBTN();
		return NewBalance;
		
	} 
	
	@Step("Digital Wallet Balance for Digital Customer ")
	public void DigitalOnly_ValidateBalanceMyAccount(String cashDefault , String nonCredit,String bonusAmt) 
	{
		String BalanceHomepage = PageObjectManager(HomePage.class).getBalanceOnHomepage();
		PageObjectManager(MainPage.class).clickMyAccount();
		PageObjectManager(HomePage.class).verifyPlayableBalanceLabelDisplayed();
		PageObjectManager(HomePage.class).validateBalanceDisplayed(BalanceHomepage);
		PageObjectManager(HomePage.class).clickExpandBalanceBTN();
		PageObjectManager(HomePage.class).verifyLabelsOnMyAccount("Cash,Bonuses");
		PageObjectManager(HomePage.class).verifyDetailedViewBTN();
		PageObjectManager(HomePage.class).clickDetailedViewBTN();
		PageObjectManager(HomePage.class).verifyLabelsDetailedView("Cash,Reward,All games bonuses");
		PageObjectManager(HomePage.class).validateCashAmountOnDetailedView(cashDefault, nonCredit);
		
		PageObjectManager(HomePage.class).validatePlayableBalanceOnDetailedView();
				
	} 
	
	@Step("Digital Wallet Balance for SAW Customer ")
	public void SAW_ValidateBlanceMyAccount(String cashDefault , String nonCredit , String unplayable) 
	{
		String BalanceHomepage = PageObjectManager(HomePage.class).getBalanceOnHomepage();
		PageObjectManager(MainPage.class).clickMyAccount();
		PageObjectManager(HomePage.class).verifyPlayableBalanceLabelDisplayed();
		PageObjectManager(HomePage.class).validateBalanceDisplayed(BalanceHomepage);
		PageObjectManager(HomePage.class).clickExpandBalanceBTN();
		PageObjectManager(HomePage.class).verifyLabelsOnMyAccount("Cash,Bonuses");
		PageObjectManager(HomePage.class).verifyDetailedViewBTN();
		PageObjectManager(HomePage.class).clickDetailedViewBTN();
		PageObjectManager(HomePage.class).clickCashExpandBTN();
		PageObjectManager(HomePage.class).verifyLabelsDetailedView("Cash,Playable online cash,Playable in-club cash,Reward,All games bonuses");
		
		PageObjectManager(HomePage.class).validatePlayableOnlineCash(cashDefault, nonCredit);
		PageObjectManager(HomePage.class).validateInClubCash(nonCredit, unplayable);
		PageObjectManager(HomePage.class).ValidateTotalCashBalanceDisplayed();
				
	} 
	
}
