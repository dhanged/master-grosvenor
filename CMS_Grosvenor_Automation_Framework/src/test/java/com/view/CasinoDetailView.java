package com.view;
import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.grosvenor.AccountDetailsPage;
import com.pagefactory.grosvenor.CasinoDetailsPage;
import com.pagefactory.grosvenor.PageValidation;

import ru.yandex.qatools.allure.annotations.Step;

public class CasinoDetailView extends BaseView {
	
	private Pojo pojo;
	
	public CasinoDetailView(Pojo pojo){
		super(pojo);
	 }
	
	@Step("system allows to search the casinos/Clubs using postcode ")
 	public void CasinoFinder_UsingPostcode(String searchTxt) 
 	{
 		
		PageObjectManager(CasinoDetailsPage.class).navigateToCasinosTAB();
		PageObjectManager(CasinoDetailsPage.class).verifyFindCasinoTABDisplayed();
		PageObjectManager(CasinoDetailsPage.class).ClickFindCasinoTAB();
		PageObjectManager(CasinoDetailsPage.class).verifyFindHDRdisplayed();
		PageObjectManager(CasinoDetailsPage.class).setPostcodeToSearch(searchTxt);
		PageObjectManager(CasinoDetailsPage.class).verifySearchResultsdisplayed();
	}
	
	@Step("system allows to search the casinos/Clubs using Town ")
 	public void CasinoFinder_UsingTown(String searchTxt) 
 	{
 		
		PageObjectManager(CasinoDetailsPage.class).navigateToCasinosTAB();
		PageObjectManager(CasinoDetailsPage.class).verifyFindCasinoTABDisplayed();
		PageObjectManager(CasinoDetailsPage.class).ClickFindCasinoTAB();
		PageObjectManager(CasinoDetailsPage.class).verifyFindHDRdisplayed();
		PageObjectManager(CasinoDetailsPage.class).setTownToSearch(searchTxt);
		PageObjectManager(CasinoDetailsPage.class).verifySearchResultsdisplayed();
		
		
	}
	
	@Step(" System displays Casinos details page with all the required details ")
 	public void CasinoFinder_CasinoDetails(String searchTxt) 
 	{
 		PageObjectManager(CasinoDetailView.class).CasinoFinder_UsingTown(searchTxt);
		
		PageObjectManager(CasinoDetailsPage.class).clickResultDisplayed();
		String CasName =PageObjectManager(CasinoDetailsPage.class).getcasinoname();
		
		PageObjectManager(CasinoDetailsPage.class).verifyDirectionsDisplayed();
		PageObjectManager(CasinoDetailsPage.class).clickMoreInfo();
		PageObjectManager(CasinoDetailsPage.class).verifyCasinoNameDisplayed(CasName);
		PageObjectManager(CasinoDetailsPage.class).verifyLocationIconDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyCasinoAddressDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyOpeningTimeIconDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyTelephoneButtonDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyEmailButtonDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyTabsDisplayed("Description,Restaurant Booking,Poker schedule and games");
		
		PageObjectManager(CasinoDetailsPage.class).verifyFacilitiesHdrDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyFacilitiesIconsDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifySocialMediaHdrDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyFaceBookLinkDisplayed();
		PageObjectManager(CasinoDetailsPage.class).verifyTwitterLinkDisplayed();
		
		
	}

	
	
	
	
	
	
	

}



