package com.view;

import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.neonretail.LoginPage;
import com.pagefactory.neonretail.MainPage;

import ru.yandex.qatools.allure.annotations.Step;

public class NeonView extends BaseView
{ 
	private Pojo pojo;
	
	public NeonView(Pojo pojo){
		super(pojo); 
		this.pojo = pojo;
	}


	@Step("Login to Neon")
	public void loginToNeon(String userName, String password) 
	{
		PageObjectManager(LoginPage.class).setUserName(userName);
		PageObjectManager(LoginPage.class).setPassword(password);
		PageObjectManager(LoginPage.class).clickLogin();
		PageObjectManager(MainPage.class).clickOKButton();
		PageObjectManager(MainPage.class).verifyUSerLoggedInSuccessfully(userName);
	} 
	
	 
}
