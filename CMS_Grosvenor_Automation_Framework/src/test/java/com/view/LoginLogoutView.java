package com.view;

import com.generic.BaseView;
import com.generic.Pojo;
import com.pagefactory.grosvenor.MainPage;
import com.pagefactory.mailinator.MailinatorPage;

import ru.yandex.qatools.allure.annotations.Step;

public class LoginLogoutView extends BaseView
{ 
	private Pojo pojo;
	public LoginLogoutView(Pojo pojo){
		super(pojo); 
		this.pojo = pojo;
	}

	
	@Step("Login to Grosvenor Casino")
	public void loginToGrosvenorCasino(String userName, String password) 
	{
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).verifyLoginPopupDisplayed();
		PageObjectManager(MainPage.class).setUserName(userName);
		PageObjectManager(MainPage.class).setPassword(password);
		PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
		PageObjectManager(MainPage.class).clickMyAccount();
		PageObjectManager(MainPage.class).verifyUserLoggedInSuccessfully(userName);
	} 
	
	
	@Step("Login to Grosvenor Casino")
	public void loginToGrosvenorCasinoWithCard(String userName, String emailAddress, String cardNumber, String PIN) 
	{
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).verifyLoginPopupDisplayed();
		PageObjectManager(MainPage.class).setUserName(cardNumber);
		PageObjectManager(MainPage.class).setPIN(PIN);
		PageObjectManager(MainPage.class).clickLoginOnLoginPopup();
		PageObjectManager(MainPage.class).verifyCodeHasBeenSentToYouHeaderDisplayed();
		String mainWindowHandle = pojo.getWebActions().getCurrentWindowHandle();
		pojo.getWebActions().openNewTab();
		pojo.getWebActions().switchToChildWindow();
		pojo.getWebDomains().launchURLForMailinator();
		PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
		PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
		PageObjectManager(MailinatorPage.class).clickGO();
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).verifyRequestedLoginCodeMailReceived();
		PageObjectManager(MailinatorPage.class).openRequestedLoginCodeMail();
		pojo.getWebActions().switchToFrameUsingNameOrId("msg_body");
		String loginCode = PageObjectManager(MailinatorPage.class).getLoginCode();
	 	pojo.getWebActions().switchToWindowUsingHandle(mainWindowHandle);
		PageObjectManager(MainPage.class).setCodeForMembershipCardLogin(loginCode);
		PageObjectManager(MainPage.class).clickVerifyOnLoginPopup();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed();
 		PageObjectManager(MainPage.class).clickMyAccount();
		PageObjectManager(MainPage.class).verifyUserLoggedInSuccessfully(userName);
	}

	
	@Step("Verify forgotten username funtionality for digital only customer")
	public void verifyForgottenUsernameFuntionalityForDigitalOnlyCustomer(String emailAddress) 
	{
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).clickForgottenUserNameLink();
		PageObjectManager(MainPage.class).verifyUsernameReminderHeaderDisplayed();
		PageObjectManager(MainPage.class).setUsernameReminderEmailAddress(emailAddress);
		PageObjectManager(MainPage.class).clickSendUsernameReminder();
		PageObjectManager(MainPage.class).verifyYouHaveMailHeaderDisplayed();
		pojo.getWebDomains().launchURLForMailinator();
		PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
		PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
		PageObjectManager(MailinatorPage.class).clickGO();
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).verifyForgotUserNameMailReceived();
	}

	
	@Step("Verify forgotten password funtionality for digital only customer")
	public void verifyForgottenPasswordFuntionalityForDigitalOnlyCustomer(String userName, String emailAddress, String password) 
	{
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).clickForgottenPasswordLink();
		PageObjectManager(MainPage.class).verifyResetPasswordHeaderDisplayed();
		PageObjectManager(MainPage.class).setResetPasswordUserName(userName);
		PageObjectManager(MainPage.class).clickSubmitForResetPassword();
		PageObjectManager(MainPage.class).verifyPasswordResetMailSend();
		pojo.getWebDomains().launchURLForMailinator();
		PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
		PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
		PageObjectManager(MailinatorPage.class).clickGO();
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).verifyForgotPasswordMailReceived();
		PageObjectManager(MailinatorPage.class).openForgotPasswordMail();
		pojo.getWebActions().switchToFrameUsingNameOrId("msg_body");
		PageObjectManager(MailinatorPage.class).clickResetPasswordButtonFromMail();
		pojo.getWebActions().switchToChildWindow(); 
		PageObjectManager(MainPage.class).setNewPasswordForPasswordReset(password);
		PageObjectManager(MainPage.class).setConfirmPasswordForPasswordReset(password);
		PageObjectManager(MainPage.class).clickResetPasswordForPasswordReset();
		PageObjectManager(MainPage.class).verifyPasswordResetSuccessMessageDisplayed(); 
		pojo.getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		this.loginToGrosvenorCasino(userName, password);
	}

	
	@Step("Verify forgotten username funtionality for digital only customer")
	public void verifyForgottenUsernameFuntionalityForSAWCustomer(String emailAddress) 
	{
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).clickForgottenMembershipNumberLink();
		PageObjectManager(MainPage.class).verifyMembershipNumberReminderHeaderDisplayed();
		PageObjectManager(MainPage.class).setUsernameReminderEmailAddress(emailAddress);
		PageObjectManager(MainPage.class).clickSubmitForMembershipNumberReminder();
	 	PageObjectManager(MainPage.class).verifyYouHaveMailHeaderDisplayed();
		pojo.getWebDomains().launchURLForMailinator();
		PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
		PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
		PageObjectManager(MailinatorPage.class).clickGO();
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
		PageObjectManager(MailinatorPage.class).verifyHereAreYourMembershipCardDetailsMailReceived();
	}

	
	@Step("Verify forgotten password funtionality for SAW customer")
	public void verifyForgottenPINFuntionalityForSAWCustomer(String userName, String emailAddress, String cardNumber, String pin) 
	{
		PageObjectManager(MainPage.class).clickLogin();
		PageObjectManager(MainPage.class).clickForgottenPINLink();
		PageObjectManager(MainPage.class).verifyResetPINHeaderDisplayed();
		PageObjectManager(MainPage.class).setResetPINEmailAddress(emailAddress);
		PageObjectManager(MainPage.class).clickRequestANewPIN();
	 	PageObjectManager(MainPage.class).verifyYouHaveMailHeaderDisplayed();
		pojo.getWebDomains().launchURLForMailinator();
		PageObjectManager(MailinatorPage.class).verifyMailinatorLogoDisplayed();
		PageObjectManager(MailinatorPage.class).setInboxName(emailAddress);
		PageObjectManager(MailinatorPage.class).clickGO();
		PageObjectManager(MailinatorPage.class).verifyInboxDisplayed();
	 	PageObjectManager(MailinatorPage.class).verifyYouRequestedPINResetMailReceived();
		PageObjectManager(MailinatorPage.class).openPINResetMail();
		pojo.getWebActions().switchToFrameUsingNameOrId("msg_body");
		PageObjectManager(MailinatorPage.class).clickChangeYourPINLinkFromMail();
		pojo.getWebActions().switchToChildWindow(); 
		PageObjectManager(MainPage.class).verifyResetPINForUseInGrosvenorCasinosHeaderDisplayed();
		PageObjectManager(MainPage.class).setNewPINForPasswordPIN(pin);
		PageObjectManager(MainPage.class).setConfirmPINForPasswordPIN(pin);
		PageObjectManager(MainPage.class).clickSubmitForPINReset();
		PageObjectManager(MainPage.class).verifyPINResetSuccessMessageDisplayed(); 
		pojo.getWebDomains().launchURLForGCWeb();
		PageObjectManager(MainPage.class).verifyGrosvenorCasinosLogoDisplayed(); 
		this.loginToGrosvenorCasinoWithCard(userName, emailAddress, cardNumber, pin);
	}
}
